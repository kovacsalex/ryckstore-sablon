var header_height = undefined;
var header_y = undefined;
var scroll_position = 0;
var eventer = undefined;
var ajax_timer = undefined;
var search_ajax_interval = 200;
var search_text = '';
var search_term_type = 'product_cat';
var search_term_id = -1;

// Oldalbetöltést követő funkciók
jQuery(document).ready(function() {
    header_height = jQuery('#header_container').outerHeight();
    header_y = jQuery('#header_container').offset().top;
    jQuery(document).on('scroll', windowScrolled);
    jQuery(window).on('resize', windowResized);

    refreshMenuItemsSelector();

    jQuery('#wrapper').css('display', 'initial');

    windowScrolled();

    checkProductScrollWidgets();

    let all_cookies = document.cookie;
    if (all_cookies.includes('ryck_age_confirmed'))
        setObjectClass('body', 'age-confirmed', true);

    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	eventer = window[eventMethod];
	var messageEvent = eventMethod === "attachEvent" ? "onmessage" : "message";

	eventer(messageEvent, messageGot);

    jQuery('.ryck-ajax-search > form > .ryck-ajax-dropdown > term').click(searchTermClick);
    jQuery('.ryck-ajax-search > form > input.ajax-input-field').on("change paste keyup", searchStringChanged);
    refreshSelectedTermClass();

    jQuery('body').on('added_to_cart', function () {
        document.getElementById('MINI-CART').contentDocument.location.reload(true);
        console.log('kosár frissítése')
    });
});

function messageGot(e)
{
    if (e.data === "close")
        closeCart();
    else if (e.data === "goCart")
    {
        closeCart();
        window.location.href = cart_url;
    }
    else if (e.data === "goCheckout")
    {
        closeCart();
        window.location.href = checkout_url;
    }
    else if (e.data === 'refreshCart')
        updateFragments();
}

function updateFragments()
{
    jQuery(document.body).trigger('wc_fragment_refresh');
}

function refreshMenuItemsSelector()
{
    if (main_menu_items_selector !== undefined && main_menu_items_selector != '')
    {
        let main_menu_items = jQuery(main_menu_items_selector);
        main_menu_items.each(refreshMenuItem);

        refreshMenuItemLogic();
    }
}
function refreshMenuItem()
{
    let this_item = this;
    let menu_item_classes = jQuery.grep(this_item.className.split(" "), function(v, i){
        return v.indexOf('menu-item-') === 0;
    });
    let menu_item_index = undefined;
    for (let i=0; i<menu_item_classes.length; i++)
    {
        let end_of_class = menu_item_classes[i].replace('menu-item-', '');
        if (jQuery.isNumeric(end_of_class))
            menu_item_index = parseInt(end_of_class);
    }
    if (menu_item_index !== undefined)
    {
        //console.log('menu id: ' + menu_item_index);
        let jq_item = jQuery(this_item);
        jq_item.attr('complex-menu-id', menu_item_index);
        setObjectClass(jq_item, 'ryck-custom-complex-main-menu-item', true);
    }
}
var any_item_hover = false; var menu_container_hover = false; var update_timer_handler = undefined;
function refreshMenuItemLogic()
{
    let menu_container = jQuery('.complex_main_menu')
    menu_container.mouseleave(function() {
        menu_container_hover = false;
        resetMouseStateTimer();
    });
    menu_container.mouseenter(function() {
        menu_container_hover = true;
        resetMouseStateTimer();
    });

    let main_menu_items = jQuery('.ryck-custom-complex-main-menu-item');
    main_menu_items.mouseleave(function() {
        let main_menu_item = jQuery(this);
        let main_menu_item_index = main_menu_item.attr('complex-menu-id');
        if (main_menu_item_index !== undefined)
            any_item_hover = false;
            resetMouseStateTimer();
    });
    main_menu_items.mouseenter(function() {
        let main_menu_item = jQuery(this);
        let main_menu_item_index = main_menu_item.attr('complex-menu-id');
        if (main_menu_item_index !== undefined)
        {
            let main_menu_submenu = jQuery('ul.complex_menu_submenu[mm-id="'+main_menu_item_index+'"]');

            if (main_menu_submenu.length)
            {
                setObjectClass('.complex_main_menu', 'hidden', false);
                setObjectClass('body', 'complex-menu-any-item-hover', true);
                any_item_hover = true;
            }
            menu_container.attr('selected-menu-id', main_menu_item_index);
            resetMouseStateTimer();
        }
    });
}
function resetMouseStateTimer()
{
    if (update_timer_handler !== undefined)
        clearTimeout(update_timer_handler);
    update_timer_handler = setTimeout(updateMenuHoverState, 200);
}
function updateMenuHoverState()
{
    setObjectClass('body', 'complex-menu-any-item-hover', any_item_hover || menu_container_hover);
    setTimeout(updateMenuHoverClass, 200);

}
function updateMenuHoverClass()
{
    let menu_container = jQuery('.complex_main_menu');
    let is_hidden = !jQuery('body').hasClass('complex-menu-any-item-hover');
    setObjectClass(menu_container, 'hidden', is_hidden);
    if (is_hidden)
        menu_container.attr('selected-menu-id', -1);
}

// Elem osztályának ki/be kapcsolása
function setObjectClass(object, className, state)
{
    let element = jQuery(object);

    if (element === undefined)
        return;

    if (state && !element.hasClass(className))
        element.addClass(className);
    if (!state && element.hasClass(className))
        element.removeClass(className);
}

// Sütik eltárolása
function storeCookie(name, value)
{
    let cookie_store_uri = ajax_folder_uri + 'store-cookies.php';
    jQuery.ajax({ url: cookie_store_uri,
             data: {
                cookie: name,
                value: value
             },
             type: 'get',
             success: function(output) {
                            if (output !== 'success')
                                console.log('Nem sikerült a sütit elmenteni');
                      }
    });
}

// Oldalgörgetés
function windowScrolled()
{
    scroll_position = jQuery(document).scrollTop();
    setObjectClass('body', 'scroll-on-top', scroll_position == 0);
    updateStickyHeader();
}

// Oldal átméretezés
function windowResized()
{
    updateStickyHeader();
}

// Sticky fejléc frissítése
function updateStickyHeader()
{
    if (sticky_header_enabled === undefined || admin_bar_height === undefined || header_height === undefined || header_y === undefined)
        return;

    if (sticky_header_enabled)
    {
        if (header_height <= 0)
        {
            header_height = jQuery('#header_container').outerHeight();
            header_y = jQuery('#header_container').offset().top;
        }

        let targetY = header_height + header_y - admin_bar_height;
        setObjectClass('body', 'sticky_header_visible', scroll_position > targetY);
    }
}

// Darabszámhoz adás
function productQtyAdd(how_many)
{
    let quantity_input = jQuery('.quantity input[name=quantity]');
    if (quantity_input !== undefined)
    {
        let current_qty = parseInt(quantity_input.val());
        let minimum_qty = parseInt(quantity_input.attr('min') || 1);
        let maximum_qty = parseInt(quantity_input.attr('max') || 999);

        let new_qty = Math.max(Math.min(current_qty + parseInt(how_many), maximum_qty), minimum_qty);
        quantity_input.val(new_qty);
    }
}

function scrollToTop()
{
    window.scrollTo({top: 0, behavior: 'smooth'});
}

// Ryck Termék lapozó elementor widgetek inicializálása
function checkProductScrollWidgets()
{
    let scrolls = jQuery('.elementor-widget-container > .ryckproduct-elementor-grid');
    scrolls.each(updateProductScrollClasses);
}

function updateProductScrollClasses()
{
    let scroll = jQuery(this);

    let pages = scroll.find('.ryckproduct-page');
    if (pages.length > 1)
        setObjectClass(pages[1], 'next', true);

    let mobile_pages = scroll.find('.ryckproduct-mobile-page');
    if (mobile_pages.length > 1)
        setObjectClass(mobile_pages[1], 'next', true);


    scroll.attr('scroll-pages', pages.length);
    scroll.attr('mobile-pages', mobile_pages.length);
    scroll.attr('scroll-selected', 1);
}

function elementor_device()
{
    return jQuery('body').attr('data-elementor-device-mode');
}

function scrollProductScroller(scrollerid, direction)
{
    let target_scroller = jQuery(scrollerid);
    if (target_scroller !== undefined)
    {
        let current_index = target_scroller.attr('scroll-selected');
        let max_index = target_scroller.attr('scroll-pages');

        if (current_index !== undefined && max_index !== undefined)
        {
            let target_index = parseInt(current_index) + direction;
            if (target_index < 1)
                target_index = parseInt(max_index);
            if (target_index > parseInt(max_index))
                target_index = 1;

            gotoProductScroller(scrollerid, target_index);
        }
    }
}
function gotoProductScroller(scrollerid, page)
{
    let target_scroller = jQuery(scrollerid);
    if (target_scroller !== undefined)
    {
        let max_index = target_scroller.attr('scroll-pages');

        if (max_index !== undefined)
        {
            let nextpage = parseInt(page) + 1;
            if (nextpage > max_index)
                nextpage = 1;

            setObjectClass(target_scroller.find('.ryckproduct-page.selected'), 'selected', false);
            setObjectClass(target_scroller.find('.ryckproduct-page.next'), 'next', false);
            setObjectClass(target_scroller.find('.ryckproduct-page[page-id="'+page+'"]'), 'selected', true);
            setObjectClass(target_scroller.find('.ryckproduct-page[page-id="'+nextpage+'"]'), 'next', true);
            target_scroller.attr('scroll-selected', page);

            // ryckproduct-controls dots" scroll-id
            let dotcontrol = jQuery('.ryckproduct-controls.dots.desktop[scroll-id="'+scrollerid+'"]');
            if (dotcontrol !== undefined)
            {
                setObjectClass(dotcontrol.find('.current'), 'current', false);
                setObjectClass(dotcontrol.find('.dot[page-id="'+page+'"]'), 'current', true);
            }
        }
    }
}

function scrollMobileScroller(scrollerid, direction)
{
    let target_scroller = jQuery(scrollerid);
    if (target_scroller !== undefined)
    {
        let current_index = target_scroller.attr('mobile-selected');
        let max_index = target_scroller.attr('mobile-pages');

        if (current_index !== undefined && max_index !== undefined)
        {
            let target_index = parseInt(current_index) + direction;
            if (target_index < 1)
                target_index = parseInt(max_index);
            if (target_index > parseInt(max_index))
                target_index = 1;

            gotoMobileScroller(scrollerid, target_index);
        }
    }
}
function gotoMobileScroller(scrollerid, page)
{
    let target_scroller = jQuery(scrollerid);
    if (target_scroller !== undefined)
    {
        let max_index = target_scroller.attr('mobile-pages');

        if (max_index !== undefined)
        {
            let nextpage = parseInt(page) + 1;
            if (nextpage > max_index)
                nextpage = 1;

            setObjectClass(target_scroller.find('.ryckproduct-mobile-page.selected'), 'selected', false);
            setObjectClass(target_scroller.find('.ryckproduct-mobile-page.next'), 'next', false);
            setObjectClass(target_scroller.find('.ryckproduct-mobile-page[page-id="'+page+'"]'), 'selected', true);
            setObjectClass(target_scroller.find('.ryckproduct-mobile-page[page-id="'+nextpage+'"]'), 'next', true);
            target_scroller.attr('mobile-selected', page);

            // ryckproduct-controls dots" scroll-id
            let dotcontrol = jQuery('.ryckproduct-controls.dots.mobile[scroll-id="'+scrollerid+'"]');
            if (dotcontrol !== undefined)
            {
                setObjectClass(dotcontrol.find('.current'), 'current', false);
                setObjectClass(dotcontrol.find('.dot[mobile-page-id="'+page+'"]'), 'current', true);
            }
        }
    }
}

// Korhatár ellenőrzés megjelenítése
function openAgeConfirmation()
{
    setObjectClass('body', 'age-confirmation-showen', true);

    setTimeout(function() {
        if (confirm('Elmúltál már 18 éves?'))
        {
            ageConfirmed();
            setObjectClass('body', 'age-confirmation-showen', false);
        }
        else
        {
            setObjectClass('body', 'age-confirmation-showen', false);
        }
    }, 100);
}
function ageConfirmed()
{
    document.cookie = "ryck_age_confirmed=yes";
    setObjectClass('body', 'age-confirmed', true);
}

// Mobil menü előhúzása
function toggleMobileMenu()
{
    jQuery('body').toggleClass('mobile-menu-opened');
}

// Kosár előhúzása
function toggleCart()
{
    jQuery('body').toggleClass('mini-cart-opened');

    if (jQuery('body').hasClass('mini-cart-opened'))
        cartOpened();
    else
        cartClosed();
}
function closeCart()
{
    setObjectClass('body', 'mini-cart-opened', false);
    updateFragments();
}
function cartOpened()
{
    let cart = jQuery('iframe#MINI-CART');
    if (cart !== undefined)
    {
        console.log(template_uri);
        cart.attr('src', template_uri + 'ajax/iframe-minicart.php');
    }
}
function cartClosed()
{
    let cart = jQuery('iframe#MINI-CART');
    if (cart !== undefined)
    {
        //cart.attr('src', false);
    }
}

// Ajax kereső logika
function searchTermClick()
{
    let term = jQuery(this);
    let uid = term.attr('u-id');
    let tid = parseInt(term.attr('t-id'));

    if (uid !== undefined && tid !== undefined)
    {
        let term_id_input = jQuery('#' + uid + ' input.search-term-id');
        term_id_input.attr('value', tid);
        setObjectClass('#' + uid, 'terms-opened', false);
        jQuery('#' + uid).attr('selected-term', tid);

        if (tid !== -1)
            jQuery('#' + uid + '.ryck-ajax-search > form > button.ajax-term-select > span').text(term.text());
        else
            jQuery('#' + uid + '.ryck-ajax-search > form > button.ajax-term-select > span').text(jQuery('#' + uid + '.ryck-ajax-search > form > button.ajax-term-select').attr('default-name'));

        refreshSelectedTermClass();
        jQuery('.ryck-ajax-search > form > input.ajax-input-field').trigger("change");
        setObjectClass('#' + uid, 'has-results', false);
        jQuery('.ryck-ajax-search > form > .ryck-ajax-items > content.results').html('');
    }
}
function refreshSelectedTermClass()
{
    let search_fields = jQuery('.ryck-ajax-search');
    search_fields.each(function() {
        let search_field = jQuery(this);
        let search_t_id = search_field.attr('selected-term');
        let all_term = search_field.find('term');
        all_term.each(function() {
            let term = jQuery(this);
            let term_id = term.attr('t-id');
            setObjectClass(term, 'selected', search_t_id == term_id);
        });
    });
}
function searchStringChanged()
{
    let search_field = jQuery(this);
    let uid = search_field.attr('u-id');
    search_text = search_field.val();
    if (uid !== undefined && search_text !== undefined)
    {
        setObjectClass('#' + uid, 'items-visible', search_text !== '');
        setObjectClass('body', 'ajax-search-loading', true);

        search_term_type = jQuery('#' + uid).find('input.search-term-type').val();
        search_term_id = jQuery('#' + uid).find('input.search-term-id').val();

        if (search_text == "")
            ajax_timer = undefined;
        else
            ajax_timer = setTimeout(updateSearchAjax, search_ajax_interval);
    }
}
function updateSearchAjax()
{
    console.log(ajax_folder_uri+'product-search.php');
    jQuery.ajax({
        method: "POST",
        url: ajax_folder_uri+'product-search.php',
        data: { q: search_text, term_id: search_term_id, term_type: search_term_type }
    }).done(function(result) {
        updateSearchAjaxResults(result);
    });
}
function updateSearchAjaxResults(result_str)
{
    let results = jQuery('.ryck-ajax-search > form > .ryck-ajax-items > content.results');
    setObjectClass('.ryck-ajax-search', 'has-results', result_str != '-1');
    setObjectClass('body', 'ajax-search-loading', false);

    if (result_str == '-1')
    {
        results.html('');
    }
    else
    {
        let results_html = '';
        let results_obj = JSON.parse(result_str);
        console.log(results_obj);

        for(let i=0; i<results_obj.length; i++)
        {
            results_html += '<result url="'+results_obj[i].guid+'" id="'+results_obj[i].ID+'">'+results_obj[i].post_title+'</result>';
        }

        results.html(results_html);

        let items = jQuery('.ryck-ajax-search > form > .ryck-ajax-items > content.results > result');
        items.click(searchResultClick);
    }
}

function searchResultClick()
{
    let item = jQuery(this);
    let url = item.attr('url');
    if (url !== undefined)
        window.location.href = url;
}
