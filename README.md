# Mi ez? #

Ez a testreszabható összekattintható Woo sablonunk kódja.

### Miért jó ez? ###

* Itt mindíg elérhető lesz a legfrissebb verzió
* A hibajavítások egyértelműbbek lesznek
* Automatizálhatjuk a frissítéseket
* Be tudjuk dobni ezt a repót a Trellóba