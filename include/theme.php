<?php

// Mappák
$assets_folder = get_template_directory_uri() . '/assets/';
$css_folder = get_template_directory_uri() . '/assets/css/';
$js_folder = get_template_directory_uri() . '/assets/js/';

// Admin menü magassága
$admin_bar_height = is_user_logged_in() ? 32 : 0;

// CSS Felépítése
function build_css() {

	global $admin_bar_height;

	?><style id="generated-css">
	/* LINKEK SZíNE */
	a:not(.nolink) 			{ color: <?php print_pref('color-links', 'regular'); ?>; --e-global-color-accent: <?php print_pref('color-links', 'regular'); ?>; } /* Normál */
	a:not(.nolink):hover 	{ color: <?php print_pref('color-links', 'hover'); ?>; } /* Egér alatt */
	a:not(.nolink):visited 	{ color: <?php print_pref('color-links', 'visited'); ?>; } /* Látogatott */
	a:not(.nolink):active 	{ color: <?php print_pref('color-links', 'active'); ?>; } /* Kattintott */

	/* ALAP SZíNEK */
	body { color: <?php print_pref('main-color'); ?>; /* Betűszín */ }

	/* FEJLÉC */
	#header_container { color: <?php print_pref('header-forecolor'); ?>; } /* Alap betűszín */

	/* STICKY FEJLÉC */
	#sticky_header_container { color: <?php print_pref('sticky-header-forecolor'); ?>; position: fixed; left: 0px; right: 0px; z-index: 99999; top: calc( -100% + <?php echo $admin_bar_height; ?>px ); transition-property: top; transition-duration: <?php print_pref('sticky-header-animation-duration'); ?>s; }
	body.sticky_header_visible #sticky_header_container { top: <?php echo $admin_bar_height; ?>px; }

	/* TARTALOM BLOKK */
	#container { color: <?php print_pref('wrapper-forecolor'); ?>; } /* Alap betűszín */

	/* LÁBLÉC */
	#footer_container { color: <?php print_pref('footer-forecolor'); ?>; } /* Alap betűszín */

	/* WOOCOMMERCE DOBOZOK */
	.ryckproduct { <?php print_pref_properties('webshop-item-margin', array('units')); ?> }
	.ryckproduct .ryckproduct_content { <?php woo_box_css(); ?> }
	.ryckproduct .product_title_price { <?php print_pref_properties('webshop-item-title-price-padding', array('units')); ?> }
	.ryckproduct .product_title_price div { <?php woo_title_price_display_css(); ?> }
	.ryckproduct .product_title_price .product_title { <?php woo_title_price_css('title'); ?> } /* Termék neve */
	.ryckproduct .product_title_price .product_price { <?php woo_title_price_css('price'); ?> } /* Termék ára */
	.ryckproduct .product_images_container { <?php print_pref_properties('webshop-item-images-padding', array('units')); ?> }
	.ryckproduct .product_images_container .product_images { <?php woo_images_css(); ?> }
	.ryckproduct .product_buttons { <?php print_pref_properties('webshop-item-buttons-padding', array('units')); ?> text-align:<?php print_pref('webshop-item-buttons-align'); ?>;}

	/* SZÖVEG ELEMEK */
	/* CíMSOROK */
	label.entry-title { <?php head_css('title'); ?> }
	.entry-content h1 { <?php head_css('h1'); ?> }
	.entry-content h2 { <?php head_css('h2'); ?> }
	.entry-content h3 { <?php head_css('h3'); ?> }
	.entry-content h4, h5, h6 { <?php head_css('h4'); ?> }
	/* BEKEZDÉSEK */
	.entry-content > p { <?php paragraph_css(); ?> }
	/* IDÉZETEK */
	.entry-content > blockquote, .entry-content p > blockquote { <?php blockquote_css(); ?> }
	/* LISTÁK */
	.entry-content ul { <?php ul_ol_css('ul'); ?> }
	.entry-content ul li { <?php ul_ol_items_css('ul'); ?> } .entry-content ul li:first-child { margin-top: 0; }
	.entry-content ul li::before { <?php ul_items_before_css(); ?> }
	.entry-content ol { <?php ul_ol_css('ol'); ?> list-style-position:inside; }
	.entry-content ol li { <?php ul_ol_items_css('ol'); ?> } .entry-content ol li:first-child { margin-top: 0; }
	.entry-content ol li::marker { <?php ol_item_marker_css(); ?> }

	/* GOMBOK */
	button:not(.nostyle), a.button:not(.nostyle), a.button:not(.nostyle):visited, #container *:not(.nostyle) > * > * > a.elementor-button:not(.nostyle), #container a.elementor-button:not(.nostyle):visited, button:not(.nostyle):focus, input[type=submit]:not(.nostyle), .product_buttons a.added_to_cart, .product_buttons a.added_to_cart:visited { <?php button_css('normal'); ?> }
	button:not(.nostyle):hover, a.button:not(.nostyle):hover, #container *:not(.nostyle) > * > * > a.elementor-button:not(.nostyle):hover, input[type=submit]:not(.nostyle):hover, .product_buttons a.added_to_cart:hover { <?php button_css('hover'); ?> }
	button:not(.nostyle):active, a.button:not(.nostyle):active, #container *:not(.nostyle) > * > * > a.elementor-button:not(.nostyle):active, input[type=submit]:not(.nostyle):active, .product_buttons a.added_to_cart:active { <?php button_css('active'); ?> }
	button:not(.nostyle) > i, a.button:not(.nostyle) > i, #container *:not(.nostyle) > * > * > a.elementor-button:not(.nostyle) > i { <?php button_icon_css('normal'); ?> }
	button.just_icon > i, a.button.just_icon > i { margin:0; }
	button:not(.nostyle):hover > i, a.button:not(.nostyle):hover > i, #container *:not(.nostyle) > * > * > a.elementor-button:not(.nostyle):hover > i { <?php button_icon_css('hover'); ?> }
	button:not(.nostyle):active > i, a.button:not(.nostyle):active > i, #container *:not(.nostyle) > * > * > a.elementor-button:not(.nostyle):active > i { <?php button_icon_css('active'); ?> }
	/* KOSÁR GOMBOK */
	.quantity input[name=quantity] {height:100%;}
	.button.single_add_to_cart_button {margin-left:<?php print_pref('product-page-cart-qty-btn-gap'); ?>px;}
	form.cart { text-align:<?php print_pref('product-page-cart-qty-btn-align'); ?>;}

	/* Oldalsávok */
	<?php
		$left_sidebar_width_raw = pref('sidebar-left-width');
		$left_sidebar_width_col2_raw = $left_sidebar_width_raw[2];
		$left_sidebar_width_col3_raw = $left_sidebar_width_raw[1];
		$left_sidebar_gap = pref('sidebar-left-gap');
		$left_sidebar_width_col2 = $left_sidebar_width_col2_raw - $left_sidebar_gap;
		$left_sidebar_width_col3 = $left_sidebar_width_col3_raw - $left_sidebar_gap;

		$right_sidebar_width_raw = pref('sidebar-right-width');
		$right_sidebar_width_col2_raw = $right_sidebar_width_raw[2];
		$right_sidebar_width_col3_raw = $right_sidebar_width_raw[1];
		$right_sidebar_gap = pref('sidebar-right-gap');
		$right_sidebar_width_col2 = $right_sidebar_width_col2_raw - $right_sidebar_gap;
		$right_sidebar_width_col3 = $right_sidebar_width_col3_raw - $right_sidebar_gap;

		$content_width_minus_left = $left_sidebar_width_col2_raw;
		$content_width_minus_right = $right_sidebar_width_col2_raw;
		$content_width_minus_leftright = $left_sidebar_width_col3_raw + $right_sidebar_width_col3_raw;
	?>
	body.column-left #container #center, body.column-right #container #center, body.column-left #container > .sidebar, body.column-right #container > .sidebar { display: inline-block; vertical-align: top; }

	body.column-left:not(.column-right) #container #center { width: calc(100% - <?php echo $content_width_minus_left; ?>px); }
	body.column-right:not(.column-left) #container #center { width: calc(100% - <?php echo $content_width_minus_right; ?>px); }
	body.column-right.column-left #container #center { width: calc(100% - <?php echo $content_width_minus_leftright; ?>px); }

	body.column-left:not(.column-right) #container > .sidebar-left { width: <?php echo $left_sidebar_width_col2; ?>px; }
	body.column-right:not(.column-left) #container > .sidebar-right { width: <?php echo $right_sidebar_width_col2; ?>px; }
	body.column-left.column-right #container > .sidebar-left { width: <?php echo $left_sidebar_width_col3; ?>px; }
	body.column-right.column-left #container > .sidebar-right { width: <?php echo $right_sidebar_width_col3; ?>px; }
	#container > .sidebar-left { margin-right: <?php echo $left_sidebar_gap; ?>px; }
	#container > .sidebar-right { margin-left: <?php echo $right_sidebar_gap; ?>px; }

	.sidebar .sidebar_inner { <?php sidebar_inner_css(); ?> }
	.sidebar_inner .widget-container .widget-title { <?php widget_title_css(); ?> }
	.sidebar_inner .widget-container > div, .sidebar_inner .widget-container > ul, .sidebar_inner .widget-container > form { <?php widget_box_css(); ?> }
	ul.widget_list li:last-child > div, ul.widget_list li:last-child > ul, ul.widget_list li:last-child > form { margin-bottom: 0; }

	<?php if (pref('topbutton-enabled') == true): ?>
	/* TOP GOMB */
	#TOP_BUTTON { <?php top_button_position_css(); top_button_background_css(); ?>transition: all ease-in 0.1s;cursor:pointer; }
	#TOP_BUTTON:hover { opacity:0.8; transform:scale(0.9); }
	body.scroll-on-top #TOP_BUTTON { display: none !important; }
	#TOP_BUTTON > <?php echo pref('topbutton-icon-type') == 'icon' ? 'i' : 'img'; ?>.icon { <?php top_button_icon_css(); ?> }
	<?php endif; ?>

	/* Woocommerce headerek */
	<?php if (pref('webshop-title-mode') == 'jumbo'): ?>
	#wrapper > .woocommerce-jumbo-header { <?php woocommerce_header_css(); ?> }
	#wrapper > .woocommerce-jumbo-header h1 { font-size: 1em !important; }
	<?php elseif (pref('webshop-title-mode') == 'normal'): ?>
	#center > .woocommerce-products-header { <?php woocommerce_header_css(); ?> }
	#center > .woocommerce-products-header h1 { font-size: 1em !important; }
	<?php endif; ?>

	/* Cart Checkout headerek */
	<?php if (pref('checkout-title-mode') == 'jumbo'): ?>
	#wrapper > .cart-checkout-jumbo-header { <?php cart_checkout_header_css(); ?> }
	#wrapper > .cart-checkout-jumbo-header h1 { font-size: 1em !important; }
	<?php elseif (pref('checkout-title-mode') == 'normal'): ?>
	#center > .woocommerce-cart-checkout-header { <?php cart_checkout_header_css(); ?> }
	#center > .woocommerce-cart-checkout-header h1 { font-size: 1em !important; }
	<?php endif; ?>

	/* Page Single headerek */
	<?php if (pref('pages-title-mode') == 'jumbo'): ?>
	#wrapper > .pages-jumbo-header { <?php page_header_css(); ?> }
	#wrapper > .pages-jumbo-header h1 { font-size: 1em !important; }
	<?php elseif (pref('pages-title-mode') == 'normal'): ?>
	#center > .pages-header { <?php page_header_css(); ?> }
	#center > .pages-header h1 { font-size: 1em !important; }
	<?php endif; ?>

	/* EGYEDI CSS */
	<?php print_pref('custom-css'); ?>
	</style><?php
}

function woocommerce_header_css()
{
	print_pref_properties('webshop-title-typo', array('font-options', 'google'));	// Betűtípus

	print_pref_properties('webshop-title-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('webshop-title-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("webshop-title-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín

	print_pref_properties('webshop-title-padding');	// Térköz
}
function dynamic_category_header_check()
{
    global $woo_category_img_url;
    if (is_product_category()) {
        $woo_category_img_url = 'igen';
		global $wp_query;
	    $cat = $wp_query->get_queried_object();
	    $thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id', true);
		$thumbnail_url = wp_get_attachment_url($thumbnail_id);
	    $woo_category_img_url = is_string($thumbnail_url) && strlen($thumbnail_url) > 0 ? $thumbnail_url : null;
    }
}
function woocommerce_jumbo_header()
{
	if (pref('webshop-title-mode') == 'jumbo')
	{
		if (is_product_category() || is_product_tag()):

		global $woo_category_img_url;
		$header_attrs = '';
		if (isset($woo_category_img_url))
			$header_attrs = ' style="background-image:url(\''. $woo_category_img_url .'\');"';

		?>
		<div class="woocommerce-jumbo-header faded"<?php echo $header_attrs; ?>>
			<div class="fader"></div>
			<div class="<?php print_pref('webshop-title-width-class'); ?> fade-front">
				<header class="woocommerce-products-header row">
					<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
						<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
					<?php endif;
				?></header>
			</div>
		</div>
		<?php endif;
	}
}
function woocommerce_normal_header()
{
	if (pref('webshop-title-mode') == 'normal')
	{
		?>
		<header class="woocommerce-products-header faded">
			<div class="fader"></div>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
				<h1 class="woocommerce-products-header__title page-title fade-front"><?php woocommerce_page_title(); ?></h1>
			<?php endif;
		?></header>
		<?php
	}
}

function cart_checkout_header_css()
{
	print_pref_properties('checkout-title-typo', array('font-options', 'google'));	// Betűtípus

	print_pref_properties('checkout-title-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('checkout-title-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("checkout-title-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín

	print_pref_properties('checkout-title-padding');	// Térköz
}
function cart_checkout_jumbo_header()
{
	if (pref('checkout-title-mode') == 'jumbo')
	{
		if (is_cart() || is_checkout()):

		$cart_url = wc_get_cart_url();
		$checkout_url = wc_get_checkout_url();

		$cart_class = 'bc-link';
		$checkout_class = 'bc-link';

		if (is_cart())
			$cart_class .= ' current';

		if (is_checkout())
			$checkout_class .= ' current';

		$cart_element = "<a href=\"$cart_url\" class=\"$cart_class\">Kosár</a>";
		$checkout_element = "<a href=\"$checkout_url\" class=\"$checkout_class\">Pénztár</a>";
		$complete_element = '<label class="bc-link">Rendelés kész</label>';

		$breadcrumbs_items = array(
			$cart_element,
			$checkout_element,
			$complete_element
		);

		$breadcrumbs_text = implode(build_icon('chevron-right'), $breadcrumbs_items);

		?>
		<div class="cart-checkout-jumbo-header faded">
			<div class="fader"></div>
			<div class="<?php print_pref('checkout-title-width-class'); ?> fade-front">
				<header class="cart-checkout-products-header row">
					<h1 class="woocommerce-cart-chekout-header__title page-title"><?php echo $breadcrumbs_text; ?></h1>
				</header>
			</div>
		</div>
		<?php endif;
	}
}
function cart_checkout_normal_header()
{
	if (pref('checkout-title-mode') == 'normal')
	{
		if (is_cart() || is_checkout()):

		$cart_url = wc_get_cart_url();
		$checkout_url = wc_get_checkout_url();
		$shop_url = get_permalink(woocommerce_get_page_id('shop'));

		$cart_class = 'bc-link';
		$checkout_class = 'bc-link';

		if (is_cart())
			$cart_class .= ' current';

		if (is_checkout())
			$checkout_class .= ' current';

		$cart_element = "<a href=\"$cart_url\" class=\"$cart_class\">Kosár</a>";
		$checkout_element = "<a href=\"$checkout_url\" class=\"$checkout_class\">Pénztár</a>";
		$complete_element = '<label class="bc-link">Rendelés kész</label>';

		$breadcrumbs_items = array(
			$cart_element,
			$checkout_element,
			$complete_element
		);

		$breadcrumbs_text = implode(build_icon('chevron-right'), $breadcrumbs_items);

		?>
		<header class="woocommerce-cart-checkout-header faded">
			<div class="fader"></div>
			<h1 class="woocommerce-cart-checkout-header__title page-title fade-front"><?php echo $breadcrumbs_text; ?></h1>
		</header>
		<?php endif;
	}
}

function page_header_css()
{
	print_pref_properties('pages-title-typo', array('font-options', 'google'));	// Betűtípus

	print_pref_properties('pages-title-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('pages-title-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("pages-title-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín

	print_pref_properties('pages-title-padding');	// Térköz
}
function page_jumbo_header()
{
	if (pref('pages-title-mode') == 'jumbo')
	{
		if (is_page() || is_single()):
		$page_title = is_page() ? get_the_title() : get_the_title();
		?>
		<div class="pages-jumbo-header faded">
			<div class="fader"></div>
			<div class="<?php print_pref('pages-title-width-class'); ?> fade-front">
				<header class="pages-header row">
					<h1 class="pages-header__title page-title"><?php echo $page_title; ?></h1>
				</header>
			</div>
		</div>
		<?php endif;
	}
}
function page_normal_header()
{
	if (pref('pages-title-mode') == 'normal')
	{
		if (is_page() || is_single()):
		$page_title = is_page() ? get_the_title() : get_the_title();
		?>
		<header class="pages-header faded">
			<div class="fader"></div>
			<h1 class="pages-header__title page-title fade-front"><?php echo $page_title; ?></h1>
		</header>
		<?php endif;
	}

}


// JS Felépítése
function build_js() {
	global $js_folder; global $admin_bar_height; global $ajax_folder_uri; global $template_root_folder; global $cart_url; global $checkout_url;

	?><script type="text/javascript" id="generated-js">
	// VÁLTOZÓK
	var admin_bar_height = <?php echo $admin_bar_height; ?>;
	var sticky_header_enabled = <?php echo (pref('sticky-header-enabled') == true ? 'true' : 'false'); ?>;

	var template_uri = '<?php echo $template_root_folder; ?>';
	var ajax_folder_uri = '<?php echo $ajax_folder_uri; ?>';

	var cart_url = '<?php echo $cart_url; ?>';
	var checkout_url = '<?php echo $checkout_url; ?>';

	// EGYEDI JS
	<?php print_pref('custom-js'); ?>
	</script>
	<script src="<?php echo $js_folder.'main.js'; ?>" id="main-js"></script><?php
}

// Beállítás CSS-é alakítása
function pref_properties($pref_name, $ignored_keys = array())
{
	$pref_array = pref($pref_name);

	if ($pref_array != null && is_array($pref_array) && sizeof($pref_array) > 0 && is_array($ignored_keys))
	{
		$result_css = '';

		foreach($pref_array as $pref_key => $pref_value)
		{
			if (isset($pref_value) && !is_null($pref_value) && $pref_value != null && !in_array($pref_key, $ignored_keys))
				$result_css .= "$pref_key:$pref_value;";
		}

		return $result_css;
	}
	else
		return '/*HIBA*/';
}
function print_pref_properties($pref_name, $ignored_keys = array())
{
	print pref_properties($pref_name, $ignored_keys);
}

// Címsor CSS
function head_css($head_name = "h1")
{
	if (!is_string($head_name) || $head_name == "")
		return;

	print_pref_properties("text-$head_name-typo", array('font-options', 'google'));	// Betűtípus
	print_pref_properties("text-$head_name-border"); // Szegélyek

	$h_border_radius = pref("text-$head_name-border-radius");		// Szegények Lekerekítése
	if (is_numeric($h_border_radius) && $h_border_radius > 0)
	{
		$h_b_r = $h_border_radius."px";
		print "border-radius:$h_b_r;";
		print "-o-border-radius:$h_b_r;";
		print "-ms-border-radius:$h_b_r;";
		print "-moz-border-radius:$h_b_r;";
		print "-webkit-border-radius:$h_b_r;";
	}

	$h_bg = pref("text-$head_name-backcolor", 'rgba');
	if ($h_bg != null && $h_bg != "transparent")
		print "background-color:$h_bg;"; // Háttérszín

	$h_underline = pref("text-$head_name-underline");
	if ($h_underline == true)
		print "text-decoration:underline;"; // Aláhúzott

	$h_display = pref("text-$head_name-display");
	print "display:$h_display;";
}

// Bekezdés CSS
function paragraph_css()
{
	print_pref_properties('paragraph-typo', array('font-options', 'google'));	// Betűtípus
	print_pref_properties('paragraph-border');	// Szegélyek

	$p_border_radius = pref('paragraph-border-radius');		// Szegények Lekerekítése
	if (is_numeric($p_border_radius) && $p_border_radius > 0)
	{
		$p_b_r = $p_border_radius."px";
		print "border-radius:$p_b_r;";
		print "-o-border-radius:$p_b_r;";
		print "-ms-border-radius:$p_b_r;";
		print "-moz-border-radius:$p_b_r;";
		print "-webkit-border-radius:$p_b_r;";
	}

	print_pref_properties('paragraph-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('paragraph-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("paragraph-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín
}

// Idézet CSS
function blockquote_css()
{
	print_pref_properties('blockquote-typo', array('font-options', 'google'));	// Betűtípus
	print_pref_properties('blockquote-border');	// Szegélyek

	if (pref('blockquote-italic') == true)
		print 'font-style:italic;';

	$p_border_radius = pref('blockquote-border-radius');		// Szegények Lekerekítése
	if (is_numeric($p_border_radius) && $p_border_radius > 0)
	{
		$p_b_r = $p_border_radius."px";
		print "border-radius:$p_b_r;";
		print "-o-border-radius:$p_b_r;";
		print "-ms-border-radius:$p_b_r;";
		print "-moz-border-radius:$p_b_r;";
		print "-webkit-border-radius:$p_b_r;";
	}

	print_pref_properties('blockquote-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('blockquote-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("blockquote-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín
}

// Lista CSS
function ul_ol_css($type)
{
	print_pref_properties("$type-items-typo", array('font-options', 'google'));
	print_pref_properties("$type-container-background", array('media', 'background-image'));
	$ul_bimage = pref("$type-container-background", 'background-image');
	if ($ul_bimage != null && $ul_bimage != "")
		print "background-image:url('$ul_bimage');";

	print_pref_properties("$type-container-border");
	$ul_border_radius = pref("$type-container-border-radius");
	if (is_numeric($ul_border_radius) && $ul_border_radius > 0)
	{
		$ul_b_r = $ul_border_radius."px";
		print "border-radius:$ul_b_r;";
		print "-o-border-radius:$ul_b_r;";
		print "-ms-border-radius:$ul_b_r;";
		print "-moz-border-radius:$ul_b_r;";
		print "-webkit-border-radius:$ul_b_r;";
	}

	print_pref_properties("$type-container-padding");
	print_pref_properties("$type-container-margin");

	$ul_bcolor = pref("$type-container-backcolor", 'rgba');
	if ($ul_bcolor != null)
		print "background-color:$ul_bcolor;";

	if ($type == 'ol')
	{
		$ol_type_modes = array(
			1 => 'decimal',
			2 => 'upper-roman',
			3 => 'upper-alpha'
		);

		$ol_type = pref('ol-items-type');
		if ($ol_type > 0 && $ol_type < 4)
		{
			$ol_style_type = $ol_type_modes[$ol_type];
			print "list-style-type:$ol_style_type;";
		}
	}
}

// Woo boxok
function woo_box_css()
{
	print_pref_properties('webshop-item-border');	// Szegélyek

	$wb_border_radius = pref('webshop-item-border-radius');		// Szegények Lekerekítése
	if (is_numeric($wb_border_radius) && $wb_border_radius > 0)
	{
		$wb_b_r = $wb_border_radius."px";
		print "border-radius:$wb_b_r;";
		print "-o-border-radius:$wb_b_r;";
		print "-ms-border-radius:$wb_b_r;";
		print "-moz-border-radius:$wb_b_r;";
		print "-webkit-border-radius:$wb_b_r;";
	}

	print_pref_properties('webshop-item-background', array('media', 'background-image')); // Háttérkép
	$wb_bg_img = pref('webshop-item-background', 'background-image');
	if ($wb_bg_img != null && $wb_bg_img != "")
		print "background-image:url('$wb_bg_img');";

	$wb_bg = pref("webshop-item-backcolor", 'rgba');
	if ($wb_bg != null && $wb_bg != "")
		print "background-color:$wb_bg;"; // Háttérszín
}

// Woo termékképek
function woo_images_css()
{
	print_pref_properties('webshop-item-images-border');	// Szegélyek

	$wi_border_radius = pref('webshop-item-images-border-radius');		// Szegények Lekerekítése
	if (is_numeric($wi_border_radius) && $wi_border_radius > 0)
	{
		$wi_b_r = $wi_border_radius."px";
		print "border-radius:$wi_b_r;";
		print "-o-border-radius:$wi_b_r;";
		print "-ms-border-radius:$wi_b_r;";
		print "-moz-border-radius:$wi_b_r;";
		print "-webkit-border-radius:$wi_b_r;";
	}

	$wi_bg = pref('webshop-item-images-backcolor', 'rgba');
	if ($wi_bg != null && $wi_bg != "")
		print "background-color:$wi_bg;"; // Háttérszín
}

// Woo termék név CSS
function woo_title_price_display_css()
{
	?>display:<?php print_pref('webshop-item-title-price-mode'); ?>;
	<?php if (pref('webshop-item-title-price-mode') == 'inline-block'): ?>vertical-align:<?php print_pref('webshop-item-title-price-vertical'); ?>;<?php endif;
}
function woo_title_price_css($type = 'title')
{
	print_pref_properties("webshop-item-$type-typo", array('font-options', 'google'));	// Betűtípus

	if (pref('webshop-item-title-price-mode') == 'inline-block')
	{
		$tp_width = intval(pref('webshop-item-title-price-width-ratio'));
		if ($type === 'price')
			$tp_width = 100 - $tp_width;

		$tp_h_half = "0px";
		$tp_h_space = pref('webshop-item-title-price-distance-horizontal', 'width');
		if ($tp_h_space !== null && is_string($tp_h_space) && $tp_h_space != '')
		{
			$tp_h_val = floatval(intval($tp_h_space));
			$tp_h_half = $tp_h_val / 2;
			$tp_h_half = $type === 'title' ? ceil($tp_h_half) : floor($tp_h_half);
			$tp_h_half .= 'px';
			$dir = $type === 'title' ? 'right' : 'left';
			print "margin-$dir:$tp_h_half;";
		}

		print "width:calc( $tp_width% - $tp_h_half );";
	}
	else
	{
		if ($type === 'title')
		{
			$tp_v_space = pref('webshop-item-title-price-distance-vertical', 'height');
			if ($tp_v_space !== null && is_string($tp_v_space) && $tp_v_space != '')
				print "margin-bottom:$tp_v_space;";
		}
	}

	if (pref("webshop-item-$type-backcolor-type") == 'label')
		print " } .ryckproduct .product_title_price .product_$type > label { display:inline-block;";

	$wt_bg = pref("webshop-item-$type-backcolor", 'rgba');
	if ($wt_bg != null && $wt_bg != "")
		print "background-color:$wt_bg;"; // Háttérszín

	print_pref_properties("webshop-item-$type-padding"); // Padding
	print_pref_properties("webshop-item-$type-border"); // Border
	$woo_border_radius = pref("webshop-item-$type-border-radius");		// Szegények Lekerekítése
	if (is_numeric($woo_border_radius) && $woo_border_radius > 0)
	{
		$w_b_r = $woo_border_radius."px";
		print "border-radius:$w_b_r;";
		print "-o-border-radius:$w_b_r;";
		print "-ms-border-radius:$w_b_r;";
		print "-moz-border-radius:$w_b_r;";
		print "-webkit-border-radius:$w_b_r;";
	}
}

// Lista elemek CSS
function ul_ol_items_css($type)
{
	print_pref_properties("$type-items-padding");

	$ul_items_distance = pref("$type-items-distance", 'height');
	if ($ul_items_distance != null && $ul_items_distance != "")
		print "margin-top:$ul_items_distance;";
}
function ul_items_before_css()
{
	$ul_type_characters = array(
		1 => '●',
		2 => '■',
		3 => '─'
	);
	$ul_items_type = pref('ul-items-type');
	if ($ul_items_type > 0 && $ul_items_type < 4)
	{
		$ul_items_color = pref('ul-items-color', 'rgba');
		$type_character = $ul_type_characters[$ul_items_type];

		$margin_right = pref('ul-items-bullet-distance', 'width');

		print "content:'$type_character';";

		if ($ul_items_color != null && $ul_items_color != "")
			print "color:$ul_items_color;";

		if ($margin_right != null && $margin_right != "")
			print "margin-right:$margin_right;";
	}
	elseif ($ul_items_type == 4)
	{
		$ul_items_image = pref('ul-items-image', 'url');
		if ($ul_items_image != null && $ul_items_image != "")
			print "content:' ';display:inline-block;width:1em;height:1em;vertical-align:middle;background-size:contain;background-repeat:no-repeat;background-position:center;background-image:url('$ul_items_image');";

		$margin_right = pref('ul-items-bullet-distance', 'width');
		if ($margin_right != null && $margin_right != "")
			print "margin-right:$margin_right;";
	}
}
function ol_item_marker_css()
{
	$marker_color = pref('ol-items-color', 'rgba');
	if ($marker_color != null && $marker_color != "")
		print "color:$marker_color;";
}

// Gombok CSS
function button_css($type = 'normal')
{
	if ($type == 'normal')
	{
		print 'display:inline-block;text-decoration:none;cursor:pointer;position:relative;border-top:0;border-bottom:0;border-left:0;border-right:0;';

		print_pref_properties('text-buttons-typo', array('font-options', 'google'));	// Betűtípus
		print_pref_properties('text-buttons-border'); // Szegélyek
		print_pref_properties('text-buttons-padding'); // Belső térközök

		$but_border_radius = pref('text-buttons-border-radius');		// Szegények Lekerekítése
		if (is_numeric($but_border_radius) && $but_border_radius > 0)
		{
			$b_b_r = $but_border_radius."px";
			print "border-radius:$b_b_r;";
			print "-o-border-radius:$b_b_r;";
			print "-ms-border-radius:$b_b_r;";
			print "-moz-border-radius:$b_b_r;";
			print "-webkit-border-radius:$b_b_r;";
		}
	}

	print_pref_properties("text-buttons-$type-background", array('media', 'background-image')); // Háttér
	$but_bg_img = pref("text-buttons-normal-background", 'background-image');
	if ($but_bg_img != null && $but_bg_img != "")
		print "background-image:url('$but_bg_img');";

	$but_bg_color = pref("text-buttons-$type-backcolor", 'rgba');
	if ($but_bg_color != null && $but_bg_color != "")
		print "background-color:$but_bg_color;"; // Háttérszín

	$but_border_color = pref("text-buttons-$type-bordercolor", 'rgba');
	if ($but_border_color != null && $but_border_color != "")
		print "border-color:$but_border_color;"; // Szegély színe

	$but_text_color = pref("text-buttons-$type-textcolor");
	if ($but_text_color != null && $but_text_color != "")
		print "color:$but_text_color;"; // Szöveg színe
}
function button_icon_css($type = 'normal')
{
	$icon_color = pref("text-buttons-$type-iconcolor");
	if ($icon_color != null && $icon_color != "")
		print "color:$icon_color;"; // Ikon színe

	if ($type == 'normal')
	{
		$icon_type = pref('text-buttons-icon-type');

		if ($icon_type == 'left-2')
			print 'float:left;';
		elseif ($icon_type == 'right-1')
			print 'position:absolute;'; // TODO
		elseif ($icon_type == 'left-1')
			print 'position:absolute;'; // TODO

		$icon_side = strpos($icon_type, 'left') !== false ? 'left' : 'right';
		$icon_place = strpos($icon_type, '1') !== false ? 'out' : 'in';

		$margin_value_key = $icon_place == 'in' ? 'text-buttons-icon-gap-1' : 'text-buttons-icon-gap-2';
		$margin_value = pref($margin_value_key);

		$css_property = $icon_place == 'in' ? ($icon_side == 'left' ? 'margin-right' : 'margin-left') : $icon_side;

		if (is_numeric($margin_value) && $margin_value > 0) {
			$margin_value .= 'px';
			print "$css_property:$margin_value;";
		}
	}
}

// Oldalsáv CSS
function sidebar_inner_css()
{
	print_pref_properties('sidebar-main-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('sidebar-main-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("sidebar-main-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín

	print_pref_properties('sidebar-main-border');	// Szegélyek
	$p_border_radius = pref('sidebar-main-border-radius');		// Szegények Lekerekítése
	if (is_numeric($p_border_radius) && $p_border_radius > 0)
	{
		$p_b_r = $p_border_radius."px";
		print "border-radius:$p_b_r;";
		print "-o-border-radius:$p_b_r;";
		print "-ms-border-radius:$p_b_r;";
		print "-moz-border-radius:$p_b_r;";
		print "-webkit-border-radius:$p_b_r;";
	}

	print_pref_properties('sidebar-main-padding'); // Padding
}
function widget_title_css()
{
	print_pref_properties('sidebar-widget-title-typo', array('font-options', 'google'));	// Betűtípus

	$bg_mode = pref('sidebar-widget-title-backcolor-type');
	print "display:$bg_mode;";

	$p_bg = pref("sidebar-widget-title-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín

	print_pref_properties('sidebar-widget-title-border');	// Szegélyek
	$p_border_radius = pref('sidebar-widget-title-border-radius');		// Szegények Lekerekítése
	if (is_numeric($p_border_radius) && $p_border_radius > 0)
	{
		$p_b_r = $p_border_radius."px";
		print "border-radius:$p_b_r;";
		print "-o-border-radius:$p_b_r;";
		print "-ms-border-radius:$p_b_r;";
		print "-moz-border-radius:$p_b_r;";
		print "-webkit-border-radius:$p_b_r;";
	}

	print_pref_properties('sidebar-widget-title-padding'); // Padding
	print_pref_properties('sidebar-widget-title-margin'); // Margin
}
function widget_box_css()
{
	// sidebar-boxes-backcolor
	// sidebar-boxes-background
	print_pref_properties('sidebar-boxes-background', array('media', 'background-image')); // Háttérkép
	$bg_img = pref('sidebar-boxes-background', 'background-image');
	if ($bg_img != null && $bg_img != "")
		print "background-image:url('$bg_img');";

	$p_bg = pref("sidebar-boxes-backcolor", 'rgba');
	if ($p_bg != null && $p_bg != "")
		print "background-color:$p_bg;"; // Háttérszín

	// sidebar-boxes-border
	// sidebar-boxes-border-radius
	print_pref_properties('sidebar-boxes-border');	// Szegélyek
	$p_border_radius = pref('sidebar-boxes-border-radius');		// Szegények Lekerekítése
	if (is_numeric($p_border_radius) && $p_border_radius > 0)
	{
		$p_b_r = $p_border_radius."px";
		print "border-radius:$p_b_r;";
		print "-o-border-radius:$p_b_r;";
		print "-ms-border-radius:$p_b_r;";
		print "-moz-border-radius:$p_b_r;";
		print "-webkit-border-radius:$p_b_r;";
	}

	// sidebar-boxes-padding
	// sidebar-boxes-margin
	print_pref_properties('sidebar-boxes-padding'); // Padding
	print_pref_properties('sidebar-boxes-margin'); // Margin
}

// Top gomb
function top_button_position_css()
{
	$spacing_h = pref('topbutton-spacing', 'width');
	$spacing_v = pref('topbutton-spacing', 'height');

	echo 'position:fixed;z-index:100001;';

	$horizontal_prop = strpos(pref('topbutton-position'), 'left') !== false ? 'left' : 'right';
	$vertical_prop = strpos(pref('topbutton-position'), 'top') !== false ? 'top' : 'bottom';

	echo "$horizontal_prop:$spacing_h;$vertical_prop:$spacing_v;";
}
function top_button_background_css()
{
	if (pref('topbutton-background-enabled') == true)
	{
		print_pref_properties('topbutton-background', array('media', 'background-image')); // Háttérkép
		$bg_img = pref('topbutton-background', 'background-image');
		if ($bg_img != null && $bg_img != "")
			print "background-image:url('$bg_img');";

		$p_bg = pref("topbutton-backcolor", 'rgba');
		if ($p_bg != null && $p_bg != "")
			print "background-color:$p_bg;"; // Háttérszín

		print_pref_properties('topbutton-padding');	// Térköz
		print_pref_properties('topbutton-border');	// Szegélyek

		$p_border_radius = pref('topbutton-border-radius');		// Szegények Lekerekítése
		if (is_numeric($p_border_radius) && $p_border_radius > 0)
		{
			$p_b_r = $p_border_radius."px";
			print "border-radius:$p_b_r;";
			print "-o-border-radius:$p_b_r;";
			print "-ms-border-radius:$p_b_r;";
			print "-moz-border-radius:$p_b_r;";
			print "-webkit-border-radius:$p_b_r;";
		}
	}
}
function top_button_icon_css()
{
	// FontAwesome ikon
	if (pref('topbutton-icon-type') == 'icon')
	{
		print_pref_properties('topbutton-icon-typo');
		print 'width: 1em !important;height: 1em !important;';
	}
	// Kép
	else
	{
		$img_size = pref('topbutton-icon-image-size').'px';
		print "width:$img_size;height:auto;";
	}
}
function build_top_button()
{
	if (pref('topbutton-enabled') == true):
	?><div id="TOP_BUTTON" onclick="scrollToTop();"><?php

		if (pref('topbutton-icon-type') == 'icon')
		{
			if (pref('topbutton-icon-fa') != '')
				print_icon(pref('topbutton-icon-fa'), '', 'solid', 'icon');
		}
		else
		{
			$icon_src = pref('topbutton-icon-image', 'url');
			if ($icon_src != null && is_string($icon_src) && $icon_src != '')
				print "<img class=\"icon\" src=\"$icon_src\" />";
		}

	?></div><?php endif;
}

// Fejléc Felépítése
function build_header() {
	global $theme_parts_folder;

	$selected_header_name = 'header-fallback';

	// Előre elkészített fejléc
	if (pref('header-type') === 'premade') {
		$expected_name = pref('premade-header-type');
		if (is_string($expected_name) && $expected_name !== "" && file_exists($theme_parts_folder . "headers/$expected_name.php")) {
			$selected_header_name = $expected_name;
		}
	}
	// Egyedi Elementor fejléc
	elseif (pref('header-type') === 'elementor') {
		$selected_header_name = "header-elementor";
	}

	// Beillesztés
	?><div id="header_container" class="header noselect <?php echo $selected_header_name; ?> <?php print_pref('header-width-class'); ?>"><?php
	include_once($theme_parts_folder . "headers/$selected_header_name.php");
	?></div><?php
}

// Sticky fejléc felépítése
function build_sticky_header()
{
	if (pref('sticky-header-enabled') == true)
	{
		global $theme_parts_folder;
		$selected_sticky_header_name = 'sticky-header-fallback';

		// Előre elkészített sticky fejléc
		if (pref('sticky-header-type') === 'premade')
		{
			$expected_name = pref('premade-sticky-header-type');
			if (is_string($expected_name) && $expected_name !== "" && file_exists($theme_parts_folder . "sticky-headers/$expected_name.php"))
				$selected_sticky_header_name = $expected_name;
		}
		// Egyedi elementor sticky fejléc
		elseif (pref('sticky-header-type') === 'elementor')
			$selected_sticky_header_name = 'sticky-header-elementor';

		//Beillesztés
		?><div id="sticky_header_container" class="sticky_header noselect <?php echo $selected_sticky_header_name; ?> <?php print_pref('sticky-header-width-class'); ?>"><?php
		include_once($theme_parts_folder . "sticky-headers/$selected_sticky_header_name.php");
		?></div><?php
	}
}

// Lábléc Felépítése
function build_footer() {
	global $theme_parts_folder;

	$selected_footer_name = 'footer-fallback';

	// Előre elkészített lábléc
	if (pref('footer-type') === 'premade') {
		$expected_name = pref('premade-footer-type');
		if (is_string($expected_name) && $expected_name !== "" && file_exists($theme_parts_folder . "footers/$expected_name.php"))
			$selected_footer_name = $expected_name;
	}
	// Egyedi Elementor lábléc
	elseif (pref('footer-type') === 'elementor')
		$selected_footer_name = "footer-elementor";

	// Beillesztés
	?><div id="footer_container" class="footer noselect <?php echo $selected_footer_name; ?> <?php print_pref('footer-width-class'); ?>"><?php
	include_once($theme_parts_folder . "footers/$selected_footer_name.php");
	?></div><?php
}

// Komplex főmenü
function complex_main_menu()
{
	if (pref('mainmenu-mode') == 'complex')
	{
		if (pref('mainmenu-complex-itemselector') != ''):
		?><script type="text/javascript">
		var main_menu_items_selector = '<?php echo pref('mainmenu-complex-itemselector'); ?>';
		</script><?php
		endif;

		$main_menu_items = menu_by_location();

		?><style>
		ul.complex_menu_submenu { display: none; }
		<?php foreach($main_menu_items as $mm_item): ?>
		.complex_main_menu[selected-menu-id="<?php echo $mm_item->ID; ?>"] ul.complex_menu_submenu[mm-id="<?php echo $mm_item->ID; ?>"] { display: block !important; }
		<?php endforeach; ?>
		</style><?php

		print '<div class="complex_main_menu noselect">';
		print '<div class="complex_main_menu_wrapper container"><div class="row">';

		foreach($main_menu_items as $mm_item_index => $mm_item)
		{
			$mm_item_id = $mm_item->ID;
			print_mega_menu_content($mm_item_id);
		}

		print '</div></div>';
		print '</div>';
	}
}
function print_mega_menu_content($mm_item_id, $classprefix = 'complex_menu', $mobile_mode = false, $navmenu_mode = false)
{
	$mm_item_submenu_id = pref("mainmenu-complex-mitem-$mm_item_id");
	if ($mm_item_submenu_id && is_numeric($mm_item_submenu_id) && $mm_item_submenu_id !== null)
	{
		$submenu_items = menu_by_id($mm_item_submenu_id);
		$submenu_item_columns = pref("mainmenu-complex-mitem-$mm_item_id-columns");
		if (!is_numeric($submenu_item_columns) || $submenu_item_columns == null || $submenu_item_columns < 1)
			$submenu_item_columns = 1;

		if (is_array($submenu_items) && sizeof($submenu_items) > 0)
		{
			$mm_attrs = $navmenu_mode ? '' : "mm-id=\"$mm_item_id\" mm-submenu-id=\"$mm_item_submenu_id\"";
			$menu_list = "<ul class=\"".$classprefix."_submenu item-$mm_item_id\" $mm_attrs". ($mobile_mode && !$navmenu_mode ? ' style="display:none;"' : '') .">";
			$submenu = false;
			$parent_id = -1;
			$count = 0;
			$main_count = 0;

			foreach ($submenu_items as $sm_item_index => $sm_item) {
				$sm_item_id = $sm_item->ID;
				$sm_item_name = $sm_item->title;
				$sm_item_link = $sm_item->url;

				if (!$sm_item->menu_item_parent) {
					$parent_id = $sm_item->ID;
					$main_count ++;

					$smattrs = $navmenu_mode ? '' : "sm-id=\"$mm_item_submenu_id\" sm-item-id=\"$sm_item_id\"";
					$menu_list .= "<li class=\"".$classprefix."_submenu_item\" $smattrs" . (!$mobile_mode && !$navmenu_mode ? " style=\"width: calc(100% / $submenu_item_columns);\"" : '') . ">";
					$menu_list .= "<a href=\"$sm_item_link\" class=\"".$classprefix."_submenu_link\">$sm_item_name</a>";
				}

				elseif ($parent_id == $sm_item->menu_item_parent) {
					if (!$submenu) {
						$submenu = true;
						$menu_list .= '<ul class="'.$classprefix.'_submenu_item_sub">';
					}

					$smattrs = $navmenu_mode ? '' : "sm-id=\"$mm_item_submenu_id\" sm-item-id=\"$sm_item_id\"";
					$menu_list .= "<li class=\"".$classprefix."_submenu_item_sub_item\" $smattrs>";
					$menu_list .= "<a href=\"$sm_item_link\" class=\"".$classprefix."_submenu_link\">$sm_item_name</a>";
					$menu_list .= '</li>';

					if ($submenu_items[$count + 1]->menu_item_parent != $parent_id && $submenu) {
						$menu_list .= '</ul>';
						$submenu = false;
					}
				}

				if ($submenu_items[$count + 1]->menu_item_parent != $parent_id) {
					$menu_list .= '</li>';
					$submenu = false;
				}

				$count++;
			}

			$menu_list .= '</ul>';

			if (!$navmenu_mode)
				print $menu_list;
			else
				return $menu_list;
		}
	}
	return '';
}
?>
