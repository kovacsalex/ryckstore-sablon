<?php

// Fordított szöveg
// EN - Text	HU - Szöveg
//
// pl: [f]Szöveg[/f]
function translate_function($atts, $content = null) {
    return esc_html__($content, 'ryckstore');
}
add_shortcode('f', 'translate_function');

// Egyszerű ikon
// A stílust a Sablon beállítások > Szöveg elemek > Ikonok oldalon módosíthatja
//
// pl: [fa]address-card[/fa]                            - Alap betűszín
//     [fa color="#333"]address-card[/fa]               - Szürke
//     [fa color="white"]address-card[/fa]              - Fehér
//     [fa color="rgba(0,0,0,.75)"]address-card[/fa]    - Áttetsző fekete
function auto_icon_function($atts, $content = null) {
    if ($content == null || $content == '' || !is_string($content))
        return '';

    $color_data = is_array($atts) && sizeof($atts) > 0 && isset($atts['color']) && is_string($atts['color']) && $atts['color'] != '' ? ' style="color:'.$atts['color'].';"' : '';
    $auto_style = pref('text-icons-fontawesome-type');
    if (!is_string($auto_style) || $auto_style == '' || $auto_style == null)
        $auto_style = 'solid';

    return build_icon($content, $color_data, $auto_style);
}
add_shortcode('fa', 'auto_icon_function');

// Regular ikon
//
// pl: [far]address-card[/far]                          - Alap betűszín
//     [far color="#333"]address-card[/far]             - Szürke
//     [far color="white"]address-card[/far]            - Fehér
//     [far color="rgba(0,0,0,.75)"]address-card[/far]  - Áttetsző fekete
function regular_icon_function($atts, $content = null) {
    if ($content == null || $content == '' || !is_string($content))
        return '';

    $color_data = is_array($atts) && sizeof($atts) > 0 && isset($atts['color']) && is_string($atts['color']) && $atts['color'] != '' ? ' style="color:'.$atts['color'].';"' : '';

    return build_icon($content, $color_data, 'regular');
}
add_shortcode('far', 'regular_icon_function');

// Solid ikon
//
// pl: [fas]address-card[/fas]                          - Alap betűszín
//     [fas color="#333"]address-card[/fas]             - Szürke
//     [fas color="white"]address-card[/fas]            - Fehér
//     [fas color="rgba(0,0,0,.75)"]address-card[/fas]  - Áttetsző fekete
function solid_icon_function($atts, $content = null) {
    if ($content == null || $content == '' || !is_string($content))
        return '';

    $color_data = is_array($atts) && sizeof($atts) > 0 && isset($atts['color']) && is_string($atts['color']) && $atts['color'] != '' ? ' style="color:'.$atts['color'].';"' : '';

    return build_icon($content, $color_data, 'solid');
}
add_shortcode('fas', 'solid_icon_function');

// Kapcsolat adatok
// Az adatokat a Sablon beállítások > Általános > Kapcsolat oldalon módosíthatja
//
// pl: [kapcsolat]                  - Automatikus elrendezés
// pl: [kapcsolat type="list"]      - Egymás alatti elrendezés
// pl: [kapcsolat type="inline"]    - Egymás melletti elrendezés
function contact_text_function($atts, $content = null) {

    $mode = 1;

    if (isset($atts['type']) && is_string($atts['type']))
    {
        if ($atts['type'] === 'list')
            $mode = 0;
    }

    $mode_styles = array(
        'style="display:block;"',
        'style="display:inline-block;"',
    );

    $mode_style = $mode_styles[$mode];

    $link_list = array();

    $c_phone = pref('contact-phone');
    $c_email = pref('contact-email');
    $c_address = pref('contact-address');
    $c_address_link = pref('contact-address-link');
    $c_facebook = pref('contact-facebook');
    $c_twitter = pref('contact-twitter');
    $c_instagram = pref('contact-instagram');

    if ($c_phone !== null && is_string($c_phone) && $c_phone != '')
        $link_list []= build_link(build_icon('phone') . $c_phone, "tel:$c_phone", '_blank', 'contact phone', $mode_style);

    if ($c_email !== null && is_string($c_email) && $c_email != '')
        $link_list []= build_link(build_icon('envelope') . $c_email, "mailto:$c_email", '_blank', 'contact email', $mode_style);

    if ($c_address !== null && is_string($c_address) && $c_address != '') {
        $c_c_address = urlencode($c_address);
		$address_link = $c_address_link !== null && strlen($c_address_link) > 0 && is_string($c_address_link) ? $c_address_link : "https://www.google.com/maps/place/$c_c_address";
        $link_list []= build_link(build_icon('map-marker-alt') . $c_address, $address_link, '_blank', 'contact address', $mode_style);
    }

    if ($c_facebook !== null && is_string($c_facebook) && $c_facebook != '')
        $link_list []= build_link(build_icon('facebook', '', 'brands') . $c_facebook, "https://facebook.com/$c_facebook", '_blank', 'contact facebook', $mode_style);

    if ($c_twitter !== null && is_string($c_twitter) && $c_twitter != '')
        $link_list []= build_link(build_icon('twitter', '', 'brands') . $c_twitter, "https://twitter.com/$c_twitter", '_blank', 'contact twitter', $mode_style);

    if ($c_instagram !== null && is_string($c_instagram) && $c_instagram != '')
        $link_list []= build_link(build_icon('instagram', '', 'brands') . $c_instagram, "https://instagram.com/$c_instagram", '_blank', 'contact instagram', $mode_style);

    if (sizeof($link_list) == 0)
        return '';

    return '<div class="contact-info">'.implode('', $link_list).'</div>';
}
add_shortcode('kapcsolat', 'contact_text_function');

// Termékdoboz
//
// pl: [termek]26[/termek]                              - Egy termék
// pl: [termek]26,28,30[/termek]                        - Több termék
function products_draw_function($atts, $content = null) {
    if ($content == null || $content == '' || !is_string($content))
        return '';

    $result = '';

    $ids = array($content);

    if (strpos($content, ',') !== false)
        $ids = explode(',', $content);

    foreach($ids as $id)
    {
        if (is_numeric($id))
        {
            $product = new RyckProduct(intval($id));
        	$result .= $product->display(false, false);
        }
    }

    return $result;
}
add_shortcode('termek', 'products_draw_function');

// Mega menü Navmenü
//
// pl: [megamenu]
function megamenu_nav_function($atts, $content) {

    $mega_navmenu_items = menu_by_location();

    $result = '';

    //print '<pre>'; print_r($mega_navmenu_items); print '</pre>';
    $result .= '<ul class="mega-nav-menu">';

    foreach($mega_navmenu_items as $mm_item) {
        $item_id = $mm_item->ID;
        $item_name = $mm_item->title;
        $item_url = $mm_item->url;
        $result .= '<li class="mega-nav-main"><a href="' . $item_url . '">' . $item_name . '</a></li>';
        $result .= print_mega_menu_content($item_id, 'mega-nav', false, true);
    }

    $result .= '</ul>';
    
    return $result;
}
add_shortcode('megamenu', 'megamenu_nav_function');

?>
