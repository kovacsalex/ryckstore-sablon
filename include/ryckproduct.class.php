<?php
class RyckProduct
{
    private $pid = -1;
    private $wc_product_object = null;
    private $image_size = 'full';
    public $data_cache = array();

    // $termek = new RyckProduct(1234);
    function __construct($product_id)
    {
        if (is_numeric($product_id))
            $this->pid = $product_id;
        elseif(is_object($product_id))
            $this->pid = $product_id->get_id();
    }

    private function check_product_object()
    {
        if ($this->wc_product_object === null && $this->pid !== -1)
            $this->wc_product_object = wc_get_product($this->pid);
    }

    // $termek_nev = $termek->get('name');
    // $termek->get('name', true);
    function get($property)
    {
        $result = null;


        // Felesleges adatbázis lekérdezések mellőzése
        if (array_key_exists($property, $this->data_cache))
            $result = $this->data_cache[$property];

        else {
            $this->check_product_object();

            if ($this->wc_product_object !== null && is_object($this->wc_product_object)) {
                switch ($property) {
                    case 'name': $result = $this->wc_product_object->get_name(); break;                                     // Terméknév
                    case 'slug': $result = $this->wc_product_object->get_slug(); break;                                     // URL végződés
                    case 'url': $result = $this->wc_product_object->get_permalink(); break;                                 // URL
                    case 'sku': $result = $this->wc_product_object->get_sku(); break;                                       // Cikkszám
                    case 'price': $result = $this->wc_product_object->get_price(); break;                                   // Ár
                    case 'price text': global $currency; $result = $this->get('price').$currency; break;                    // Ár szöveg
                    case 'sale price': $result = $this->wc_product_object->get_sale_price(); break;                         // Akciós ár
                    case 'sale price text': global $currency; $result = $this->get('sale price').$currency; break;          // Akciós ár szöveg
                    case 'sale from': $result = $this->wc_product_object->get_date_on_sale_from(); break;                   // Akciós ár kezdete (dátum)
                    case 'sale to': $result = $this->wc_product_object->get_date_on_sale_to(); break;                       // Akciós ár vége (dátum)
                    case 'sale': $result = $this->is_sale(); break;                                                         // Akciós?
                    case 'regular price': $result = $this->wc_product_object->get_regular_price(); break;                   // Normál ár
                    case 'regular price text': global $currency; $result = $this->get('regular price').$currency; break;    // Normál ár szöveg
                    case 'total sales': $result = $this->wc_product_object->get_total_sales(); break;                       // Összes eladás
                    case 'stock status': $result = $this->wc_product_object->get_stock_status(); break;                     // Készlet státusz
                    case 'stock quantity': $result = $this->wc_product_object->get_stock_quantity(); break;                 // Készlet mennyiség
                    case 'stock management': $result = $this->wc_product_object->get_manage_stock(); break;                 // Készletkezelés?
                    case 'available': $result = $this->is_available(); break;                                               // Elérhető?
                    case 'weight': $result = $this->wc_product_object->get_weight(); break;                                 // Súly
                    case 'weight text': global $weight_unit; $result = $this->get('weight').$weight_unit; break;            // Súly szöveg
                    case 'length': $result = $this->wc_product_object->get_length(); break;                                 // Hossz
                    case 'length text': global $size_unit; $result = $this->get('length').$size_unit; break;                // Hossz szöveg
                    case 'width': $result = $this->wc_product_object->get_width(); break;                                   // Szélesség
                    case 'width text': global $size_unit; $result = $this->get('width').$size_unit; break;                  // Szélesség szöveg
                    case 'height': $result = $this->wc_product_object->get_height(); break;                                 // Magasság
                    case 'height text': global $size_unit; $result = $this->get('height').$size_unit; break;                // Magasság szöveg
                    case 'dimensions': $result = $this->wc_product_object->get_dimensions(); break;                         // Méretek (szöveg)
                    case 'tax status': $result = $this->wc_product_object->get_tax_status(); break;                         // Adó státusz
                    case 'tax class': $result = $this->wc_product_object->get_tax_class(); break;                           // Adó osztály (ID)
                    case 'shipping class': $result = $this->wc_product_object->get_shipping_class_id(); break;              // Szállítási osztály (ID)
                    case 'upsell products': $result = $this->wc_product_object->get_upsell_ids(); break;                    // Felértékesítés termékek (ID)
                    case 'cross products': $result = $this->wc_product_object->get_cross_sell_ids(); break;                 // Keresztértékesítés term. (ID)
                    case 'parent product': $result = $this->wc_product_object->get_cross_sell_ids(); break;                 // Szülő termék (ID)
                    case 'variations': $result = $this->wc_product_object->get_children(); break;                           // Termék variációk (ID)
                    case 'attributes raw': $result = $this->wc_product_object->get_attributes(); break;                     // Nyers attribútumok (Object[])
                    case 'attributes': $result = $this->attributes(); break;                                                // Egyszerű attribútumok
                    case 'variation attributes': $result = $this->variation_attributes(); break;                            // Variációs attribútumok
                    case 'categories': $result = $this->wc_product_object->get_categories(); break;                         // Kategóriák listája (linkek)
                    case 'category ids': $result = $this->wc_product_object->get_category_ids(); break;                     // Kategóriák listája (ID)
                    case 'tags': $result = $this->get_tags(); break;                                                        // Címkék listája (linkek)
                    case 'tag ids': $result = $this->wc_product_object->get_tag_ids(); break;                               // Címkék listája (ID)
                    case 'thumbnail id': $result = $this->wc_product_object->get_image_id(); break;                         // Thumbnail kép (ID)
                    case 'thumbnail': $result = $this->thumbnail_url(); break;                                              // Thumbnail kép (URL)
                    case 'thumbnail size': $result = $this->thumbnail_size(); break;                                        // Thumbnail kép méret (tömb)
                    case 'gallery ids': $result = $this->wc_product_object->get_gallery_image_ids(); break;                 // Galéria képek (ID)
                    case 'gallery': $result = $this->gallery_urls(); break;                                                 // Galéria képek (URL)
                    case 'gallery sizes': $result = $this->gallery_sizes(); break;                                          // Galéria képek  méretei (tömb)
                    case 'images ids': $result = $this->images_ids(); break;
                    case 'images': $result = $this->images_urls(); break;
                    case 'images sizes': $result = $this->images_sizes(); break;
                    case 'reviews': $result = $this->wc_product_object->get_reviews_allowed(); break;                       // Véleményezhető?
                    case 'review count': $result = $this->wc_product_object->get_review_count(); break;                     // Értékelések száma
                    case 'rating': $result = $this->wc_product_object->get_average_rating(); break;                         // Átlagos értékelés
                    case 'ratings count': $result = $this->wc_product_object->get_rating_counts(); break;                   // Értékelések száma (egyenként)
                    case 'created': $result = $this->wc_product_object->get_date_created(); break;                          // Létrehozás (dátum Object)
                    case 'modified': $result = $this->wc_product_object->get_date_modified(); break;                        // Szerkesztés (dátum Object)
                    case 'status': $result = $this->wc_product_object->get_status(); break;                                 // Státusz
                    case 'featured': $result = $this->wc_product_object->get_featured(); break;                             // Kiemelt?
                    case 'add to cart url': $result = $this->wc_product_object->add_to_cart_url(); break;                  // Kosárhoz adás link
                    case 'type': $result = $this->wc_product_object->get_type(); break;                                     // Termék típusa
                }
            }

            if ($result !== null)
                $this->data_cache[$property] = $result;
        }

        return $result;
    }

    function print($property)
    {
        $value = $this->get($property);

        $product_id = $this->pid;
        print $value === null ? "error ($product_id)" : $value;
    }

    // Termékdoboz
    public function display($print = true, $use_columns = true, $column_overrides = '', $attributes = '')
    {
        $product_rows = pref('webshop-item-rows');

        $product_column = $use_columns ? 'col-12' : '';
        if ($use_columns)
        {
            $column_sizes = array();
            $column_sizes []= pref('webshop-page-xl-col');
            $column_sizes []= pref('webshop-page-lg-col');
            $column_sizes []= pref('webshop-page-md-col');
            $column_sizes []= pref('webshop-page-sm-col');

            $product_column = implode(' ', $column_sizes);
        }
        elseif (is_string($column_overrides) && $column_overrides != '')
        {
            $product_column .= " $column_overrides";
        }

        $is_product_age_limited = get_post_meta($this->pid, '_product_age_limited', true);

        ob_start();

        ?><div class="ryckproduct product <?php echo $this->pid; ?> <?php echo $product_column; ?><?php echo $is_product_age_limited ? ' age-limited' : ''; ?>" <?php echo $attributes; ?>>
            <div class="ryckproduct_content rel">
                <?php
                    foreach($product_rows as $row_id => $row_data) {
                        if ($row_data !== false) {
                            switch($row_id)
                            {
                                case 'photos': $this->display_photos(); break;
                                case 'title-price': $this->display_title_price(); break;
                                case 'buttons': $this->display_buttons(); break;
                                case 'details': $this->display_details(); break;
                                case 'description': $this->display_description(); break;
                            }
                        }
                    }
                ?>
            </div>
            <?php if ($is_product_age_limited): ?>
            <div class="ryckproduct_age_content" onclick="openAgeConfirmation();">
                <label><?php ___('Korhatáros termék'); ?></label>
            </div>
            <?php endif; ?>
        </div><?php

        $product_box = do_shortcode(ob_get_clean());

        if ($print)
            print $product_box;

        return $product_box;
    }
    // Termékfotók
    private function display_photos()
    {
        $photo_max_size = intval(pref('webshop-item-images-size'));
        $photo_height_percent = pref('webshop-item-images-height');

        $actual_image_size = array(
            $photo_max_size,
            round(floatval($photo_max_size) * floatval($photo_height_percent) / 100)
        );

        $product_photo_mode = pref('webshop-item-images-mode');

        // Összes kép lekérése
        $oldsize = $this->$image_size;
        $this->set_image_size($photo_max_size);
        $all_product_photo = $this->get('images');
        $this->set_image_size($oldsize);

        // Egy képes mód
        if ($product_photo_mode === 'single' && sizeof($all_product_photo) > 1)
        {
            $first_photo = $all_product_photo[0];
            $all_product_photo = array($first_photo);
        }

        // Placeholder
        if (sizeof($all_product_photo) == 0)
            array_push($all_product_photo, wc_placeholder_img_src($photo_max_size));

        ?><a class="nolink" href="<?php $this->print('url'); ?>"><div class="product_images_container noselect"><div class="product_images <?php echo $product_photo_mode; ?> rel" style="padding-bottom:<?php echo $photo_height_percent; ?>%;">
            <?php
            foreach($all_product_photo as $photo_index => $photo_url):
            //print_r($actual_image_size);
                ?><div
                class="product_image image_<?php echo $photo_index; if ($photo_index === 0):?> current<?php endif; ?>"
                style="background-image:url('<?php echo $photo_url; ?>');"></div><?php
            endforeach; ?>
        </div></div></a><?php
    }
    // Terméknév és ár
    private function display_title_price()
    {
        $price_separeted_class = pref('webshop-item-title-price-mode') == 'block' ? ' separated' : '';

        ?><a class="nolink" href="<?php $this->print('url'); ?>"><div class="product_title_price<?php echo $price_separeted_class; ?> rel ffix">
            <div class="product_title normal"><label><?php $this->print('name'); ?></label></div>
            <div class="product_price normal noselect"><label><?php $this->print('price text'); ?></label></div>
        </div></a><?php
    }
    // Termék gombok
    private function display_buttons()
    {
        ?><div class="product_buttons noselect rel">
            <?php

                $wishlist_button_shortcode = pref('webshop-item-favorite-button-shortcode');
				$wishlist_button_result = '';

                if (is_string($wishlist_button_shortcode) && strlen($wishlist_button_shortcode) > 0)
                    $wishlist_button_result = do_shortcode($wishlist_button_shortcode);

				if (strlen($wishlist_button_result) === 0 && is_string($wishlist_button_shortcode) && strlen($wishlist_button_shortcode) > 0)
					$wishlist_button_result = do_shortcode("[ti_wishlists_addtowishlist loop=yes]");
				if (strlen($wishlist_button_result) === 0 && is_string($wishlist_button_shortcode) && strlen($wishlist_button_shortcode) > 0)
					$wishlist_button_result = '<a role="button" aria-label="Kedvencekhez adás" class="tinvwl_add_to_wishlist_button tinvwl-icon-heart FAVORITES-BUTTON FORCED tinvwl-position-shortcode" data-tinv-wl-list="[]" data-tinv-wl-product="'.$this->pid.'" data-tinv-wl-productvariation="0" data-tinv-wl-productvariations="[0]" data-tinv-wl-producttype="simple" data-tinv-wl-action="add"></a>';

				echo $wishlist_button_result;

				$this->add_to_cart_button();

			?>
        </div><?php
    }
    // Egyéb adatok
    private function display_details()
    {

    }
    // Rövid leírás
    private function display_description()
    {

    }

    // Kosárhoz adás gomb
    public function add_to_cart_button($ajax_override = false, $use_ajax = false)
    {
        $ajax_button = pref('webshop-item-buttons-ajax') == true;
        if ($ajax_override)
            $ajax_button = $use_ajax;

        $button_text = pref('webshop-item-cart-button-text');
        if (!is_string($button_text) || $button_text == '')
            $button_text = 'Add to Cart';

        $button_icon = pref('webshop-item-cart-button-icon');

        ?><a
        href="<?php $this->print('add to cart url'); ?>"
        value="<?php echo $this->pid; ?>"
        rel="nofollow"
        data-product_id="<?php echo $this->pid; ?>"
        data-product_sku="<?php $this->print('sku'); ?>"
        class="<?php if ($ajax_button): ?>ajax_add_to_cart <?php endif; ?>button add_to_cart_button product_type_<?php $this-print('type'); ?>"
        ><?php print $button_text; ?>[fas]<?php echo $button_icon; ?>[/fas]</a><?php
    }

    // Elérhető?
    private function is_available()
    {
        return $this->get('stock status') === 'instock';
    }

    // Akciós?
    private function is_sale()
    {
        $sale_price = $this->get('sale price');

        if (!is_numeric($sale_price) || $sale_price < 0 || $sale_price === null)
            return false;

        $display_price = $this->get('price');
        return $display_price == $sale_price;
    }

    // Címkék listája (linkek)
    private function get_tags()
    {
        if ($this->pid === -1)
            return null;

        $terms = wp_get_post_terms($this->pid, 'product_tag');
        if (sizeof($terms) > 0) {
            $links = array();

            foreach($terms as $term) {
                $term_id   = $term->term_id;
                $term_name = $term->name;
                $term_link = get_term_link($term, 'product_tag');

                array_push($links, "<a href=\"$term_link\">$term_name</a>");
            }

            return implode(', ', $links);
        }

        return '';
    }

    // Képek méretének felülírása
    // $termek->set_image_size();                   - Visszaállítás ("full" - teljes méret)
    // $termek->set_image_size(300);                - 300x300 méret
    // $termek->set_image_size(array(400, 300));    - 400x300 méret
    // $termek->set_image_size('thumbnail');        - "thumbnail" méret
    public function set_image_size($size = 'full')
    {
        if (is_string($size) || (is_array($size) && sizeof($size) == 2 && is_numeric($size[0]) && is_numeric($size[1]) && $size[0] > 0 && $size[1] > 0))
            $this->image_size = $size;
        elseif (is_numeric($size) && $size > 0)
            $this->image_size = array($size, $size);
    }

    // Kép URL és felbontás
    private function get_photo_data($photo_id)
    {
        if (!is_numeric($photo_id) || $photo_id === null)
            return array();

        $data = wp_get_attachment_image_src($photo_id, $this->image_size);

        return array(
            'url' => (sizeof($data) >= 1 && is_string($data[0]) && $data[0] != "" ? $data[0] : ''),
            'size' => array(
                (sizeof($data) >= 3 && is_numeric($data[1]) && $data[1] > 0 ? $data[1] : 0),
                (sizeof($data) >= 3 && is_numeric($data[2]) && $data[2] > 0 ? $data[2] : 0)
            )
        );
    }

    // Thumbnail kép (URL)
    private function thumbnail_url()
    {
        $thumb_id = $this->get('thumbnail id');
        if (!is_numeric($thumb_id) || $thumb_id === null)
            return '';

        $photo_data = $this->get_photo_data($thumb_id);
        return $photo_data['url'];
    }

    // Thumbnail kép méretek (tömb)
    private function thumbnail_size()
    {
        $thumb_id = $this->get('thumbnail id');
        if (!is_numeric($thumb_id) || $thumb_id === null)
            return '';

        $photo_data = $this->get_photo_data($thumb_id);
        return $photo_data['size'];
    }

    // Galéria képek (URL)
    private function gallery_urls()
    {
        $gallery_ids = $this->get('gallery ids');
        if (!is_array($gallery_ids) || sizeof($gallery_ids) == 0)
            return array();

        $gallery_urls = array();

        foreach($gallery_ids as $gallery_id) {
            $data = $this->get_photo_data($gallery_id);
            array_push($gallery_urls, $data['url']);
        }

        return $gallery_urls;
    }

    // Galéria képek méretei (tömb)
    private function gallery_sizes()
    {
        $gallery_ids = $this->get('gallery ids');
        if (!is_array($gallery_ids) || sizeof($gallery_ids) == 0)
            return array();

        $gallery_sizes = array();

        foreach($gallery_ids as $gallery_id) {
            $data = $this->get_photo_data($gallery_id);
            array_push($gallery_sizes, $data['size']);
        }

        return $gallery_sizes;
    }

    private function images_ids()
    {
        $id_array = array();

        $thumbnail_id = $this->get('thumbnail id');
        if (is_numeric($thumbnail_id))
            array_push($id_array, $thumbnail_id);

        $gallery_ids = $this->get('gallery ids');
        foreach($gallery_ids as $gallery_id)
        {
            if (is_numeric($gallery_id))
                array_push($id_array, $gallery_id);
        }

        return $id_array;
    }
    private function images_urls()
    {
        $url_array = array();

        $thumbnail_url = $this->get('thumbnail');
        if (is_string($thumbnail_url) && $thumbnail_url !== '')
            array_push($url_array, $thumbnail_url);

        $gallery_urls = $this->get('gallery');
        foreach($gallery_urls as $gallery_url)
        {
            if (is_string($gallery_url) && $gallery_url !== '')
                array_push($url_array, $gallery_url);
        }

        return $url_array;
    }
    private function images_sizes()
    {
        $size_array = array();

        $thumbnail_size = $this->get('thumbnail size');
        if (is_array($thumbnail_size) && sizeof($thumbnail_size) == 2)
            array_push($size_array, $thumbnail_size);

        $gallery_sizes = $this->get('gallery sizes');
        foreach($gallery_sizes as $gallery_size)
        {
            if (is_array($gallery_size) && sizeof($gallery_size) == 2)
                array_push($size_array, $gallery_size);
        }

        return $size_array;
    }

    // Attribútumok (nem variációkhoz használt)
    private function attributes()
    {
        $all_attributes = $this->get('attributes raw');
        if (!is_array($all_attributes) || sizeof($all_attributes) == 0)
            return array();

        $attrs = array();
        foreach($all_attributes as $ai => $aobj)
        {
            $adata = $aobj->get_data(); //$this->wc_product_object->get_attribute($ai);
            if (!$adata['is_variation'])
                $attrs[$ai] = array($adata['name'], $adata['value'], $adata['is_visible']);
        }
        return $attrs;
    }

    // Attribútumok (variációkhoz használt)
    private function variation_attributes()
    {
        $all_attributes = $this->get('attributes raw');
        if (!is_array($all_attributes) || sizeof($all_attributes) == 0)
            return array();

        $attrs = array();
        foreach($all_attributes as $ai => $aobj)
        {
            $adata = $aobj->get_data(); //$this->wc_product_object->get_attribute($ai);
            if ($adata['is_variation'])
                $attrs[$ai] = array($adata['name'], $adata['options']);
        }
        return $attrs;
    }
}
?>
