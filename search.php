<?php get_header(); ?>
	<main id="content">
		<?php
		
		if (have_posts()):
		?><header class="header">
			<h1 class="entry-title"><?php printf(__v('Search Results for: %s'), get_search_query()); ?></h1>
		</header><?php
		
		while (have_posts()) {
			the_post();
			get_template_part('entry');
		}
		get_template_part('nav', 'below');
		
		else:
		?><article id="post-0" class="post no-results not-found">
			<header class="header">
				<h1 class="entry-title"><?php ___('Nothing Found'); ?></h1>
			</header>
			<div class="entry-content">
				<p><?php ___('Sorry, nothing matched your search. Please try again.'); ?></p>
				<?php get_search_form(); ?>
			</div>
		</article>
		<?php endif; ?>
	</main>
	
	<?php get_sidebar(); ?>
<?php get_footer(); ?>