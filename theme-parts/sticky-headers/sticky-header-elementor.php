<?php

    $expected_id = pref('elementor-sticky-header-id');

    // Van megadva Elementor template?
    if (is_numeric($expected_id) && intval($expected_id) > 0) {
        // Ha van, megjelenítjük
        echo do_shortcode("[elementor-template id=\"$expected_id\"]");
    } else {
        // Ha nincs, akkor nyilván nem..
        ?><h3>Elementor Sablon nem található</h3><?php
    }
?>
