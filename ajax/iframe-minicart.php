<?php

    require_once('../../../../wp-load.php');

    $currency = html_entity_decode(get_woocommerce_currency_symbol());

?>
<html>
    <body id="BODY" style="opacity: 0;">
        <button class="close-cart" type="button" onclick="closeCart();">×</button>
        <content>
            <?php
                global $woocommerce;
                $items = $woocommerce->cart->get_cart();

                foreach($items as $item => $values) {
                    $_product = wc_get_product($values['data']->get_id());

                    $getProductDetail = wc_get_product($values['product_id']);
                    $image_src = $getProductDetail->get_image();
                    $name = $_product->get_title();
                    $quantity = $values['quantity'];
                    $price = $_product->get_price();

                    $remove_url = wc_get_cart_remove_url($item);

                    ?>
                    <div class="cart-item">
                        <?php echo $image_src; ?>
                        <div class="product-info">
                            <label class="product-name"><?php echo $name; ?></label>
                            <label class="product-price"><?php echo $price.$currency; ?></label>
                        </div>
                        <a class="product-remove" type="button" href="<?php echo $remove_url; ?>">×</a>
                    </div>
                    <?php
                }
            ?>
        </content>
        <div class="price">
            <label>Részösszeg:</label>
            <label><?php echo $woocommerce->cart->get_cart_total(); ?></label>
        </div>
        <div class="buttons">
            <button class="cart" type="button" onclick="openCart();">Kosár</button>
            <button class="checkout" type="button" onclick="openCheckout();">Fizetés</button>
        </div>
    </body>
    <script type="text/javascript">
        function closeCart()
        {
            //window.parent.postMessage({ message: "close" });
            parent.postMessage("close", "*")
        }
        function openCart()
        {
            //window.parent.postMessage({ message: "goCart" });
            parent.postMessage("goCart", "*")
        }
        function openCheckout()
        {
            //window.parent.postMessage({ message: "goCheckout" });
            parent.postMessage("goCheckout", "*")
        }

        window.addEventListener("beforeunload", function (event) {
            var element = document.getElementById("BODY");
            element.classList.add("loading");
            parent.postMessage('refreshCart', '*');
        });
    </script>
    <style>

        * {
            font-family: Roboto, arial, tahoma, Open Sans, sans-serif;
        }
        body {
            padding: 20px;
            margin: 0;
            opacity: 1 !important;
        }
        body.loading {
            opacity: 0 !important;
        }

        body > button.close-cart {
            position: fixed;
            top: 0;
            right: 0;
            font-size: 36px;
            line-height: 30px;
            height: 30px;
            width: 30px;
            border: 0;
            padding: 0;
            background: transparent;
            color: black;
            cursor: pointer;
            transition: transform ease-in 0.1s;
            z-index: 1000;
            outline: 0 !important;
        }
        body > button.close-cart:hover {
            transform: scale(1.2);
        }

        .cart-item {
            position: relative;
            margin-bottom: 10px;
        }
        .cart-item > * {
            display: inline-block;
            vertical-align: middle;
        }
        .cart-item > img {
            width: 60px;
            height: auto;
        }
        .cart-item > .product-info > * {
            display: block;
        }
        .cart-item > .product-info {
            padding-left: 12px;
            max-width: 60%;
        }
        .cart-item > .product-info > .product-price {
            font-size: 1.1em;
            font-weight: bold;
            margin-top: 4px;
        }
        .cart-item > .product-remove {
            position: absolute;
            right: 8px;
            top: 50%;
            transform: translateY(-50%);
            font-size: 20px;
            line-height: 20px;
            height: 20px;
            width: 20px;
            border: 0;
            padding: 0;
            background-color: black;
            color: white;
            transition: all ease-in 0.1s;
            cursor: pointer;
            outline: 0 !important;
            text-align: center;
            text-decoration: none;
        }
        .cart-item > .product-remove:hover {
            transform: translateY(-50%) scale(1.2);
            opacity: 0.8;
        }
        body > .price {
            margin-top: 20px;
            text-align: center;
        }
        body > .buttons {
            margin-top: 20px;
            position: relative;
            height: 40px;
        }
        .buttons > button {
            position: absolute;
            top: 0;
            height: 100%;
            width: 45%;
			background: #d22329;
			border: 1px solid #d22329;
			padding: 0;
			color: white;
			cursor: pointer;
			transition: all 0.1s ease-in;
        }
        .buttons > button:nth-child(2n+1) {
            left: 0;
        }
        .buttons > button:nth-child(2n+2) {
            right: 0;
        }

		.buttons > button:hover {
			background: white;
			color: #d22329;
		}
    </style>
</html>
