<?php

    $cookie_name = isset($_GET['cookie']) ? $_GET['cookie'] : false;
    $cookie_value= isset($_GET['value'] ) ? $_GET['value' ] : false;

    if ($cookie_name !== false && $cookie_value !== false)
    {
        setcookie($cookie_name, $cookie_value, strtotime('+30 days'));
        echo 'success';
    }
    else
        echo 'fail';

?>
