<?php
    include_once "../../../../wp-load.php";
    include_once "../functions.php";

    $search_string = isset($_POST['q']) ? $_POST['q'] : false;
    $search_term_type = isset($_POST['term_type']) ? $_POST['term_type'] : false;
    $search_term_id = isset($_POST['term_id']) ? intval($_POST['term_id']) : false;
    $search_max_items = isset($_POST['max_items']) ? intval($_POST['max_items']) : -1;

    if ($search_string !== false)
    {
        $all_products = productsSearch($search_string, $search_term_id, $search_term_type, $search_max_items, false);

        if (sizeof($all_products) > 0)
            echo json_encode($all_products);
        else
            echo "-1";
    }
    else
        echo "-1";
?>
