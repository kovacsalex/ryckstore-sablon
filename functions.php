<?php

// Redux Framework integráció
if (!class_exists('ReduxFramework')) {
    require_once(dirname(__FILE__) . '/redux/framework.php');
}
if (!isset($ryckstore_pref)) {
    require_once(dirname(__FILE__) . '/functions/admin-config.php');
}
// Beállítás lekérése
function pref($key, $key2 = null, $key3 = null)
{
    global $ryckstore_pref;

    if ($key3 === null)
    {
        if ($key2 === null)
        {
            // Egy dimenziós tömb
            if (isset($ryckstore_pref[$key]))
                return $ryckstore_pref[$key];
        }
        else
        {
            // Két dimenziós tömb
            if (isset($ryckstore_pref[$key]) && is_array($ryckstore_pref[$key]) && isset($ryckstore_pref[$key][$key2]))
                return $ryckstore_pref[$key][$key2];
        }
    }
    else if ($key2 !== null)
    {
        // Három dimenziós tömb
        if (isset($ryckstore_pref[$key]) && is_array($ryckstore_pref[$key]) && isset($ryckstore_pref[$key][$key2]) && is_array($ryckstore_pref[$key][$key2]) && isset($ryckstore_pref[$key][$key2][$key3]))
            return $ryckstore_pref[$key][$key2][$key3];
    }

    return null;
}
// Beállítás írása
function print_pref($key, $key2 = null, $key3 = null)
{
    $value = pref($key, $key2, $key3);
    if ($value !== null)
        print $value;
    else
        print '[ERROR]';
}
// Minden beállítás írása
function print_r_pref()
{
    global $ryckstore_pref;
    print '<pre id="RYCKSTORE_PREF_DEBUG">'; print_r($ryckstore_pref); print '</pre>';
}

// Sablon funkciók
$template_root_folder = get_template_directory_uri() . '/';
$ajax_folder_uri = $template_root_folder.'ajax/';
$include_folder = dirname(__FILE__) . '/include/';
$elementor_folder = dirname(__FILE__) . '/elementor/';
$theme_parts_folder = dirname(__FILE__) . '/theme-parts/';
require_once($include_folder . 'theme.php');
require_once($include_folder . 'shortcodes.php');
require_once($elementor_folder . 'custom-widgets.php');

$cart_url = wc_get_cart_url();
$checkout_url = wc_get_checkout_url();

// Mértékegységek
$size_unit = defval(get_option('woocommerce_dimension_unit'), 'cm');
$weight_unit = defval(get_option('woocommerce_weight_unit'), "kg");
$currency = defval(html_entity_decode(get_woocommerce_currency_symbol()), "Ft");

// Változók
$has_left_sidebar = false;
$has_right_sidebar = false;
$mini_cart_included = false;
$woo_category_img_url = null;

// Sablon osztályok
require_once($include_folder . 'ryckproduct.class.php');

// Telepités után
add_action('after_setup_theme', 'ryckstore_setup');
function ryckstore_setup() {
	load_theme_textdomain('ryckstore', get_template_directory() . '/languages');

	add_theme_support('title-tag');
	add_theme_support('automatic-feed-links');
	add_theme_support('post-thumbnails');
	add_theme_support('html5', array('search-form'));
    add_theme_support('custom-logo');
    add_theme_support('woocommerce');

	global $content_width;
	if (!isset($content_width)) {
		$content_width = 1920;
	}
	register_nav_menus(array('main-menu' => esc_html__('Main Menu', 'ryckstore')));
}

// Google analytics kód
function google_analytics_code()
{
    if (pref('ga-api-id') != null && pref('ga-api-id') != "")
    {
        if (pref('ga-api-mode') == 'async'):
        ?>
        <script>
        window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
        ga('create', '<?php print_pref('ga-api-id'); ?>', 'auto');
        ga('send', 'pageview');
        </script>
        <script async src='https://www.google-analytics.com/analytics.js'></script>
        <?php
        else:
        ?>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '<?php print_pref('ga-api-id'); ?>', 'auto');
        ga('send', 'pageview');
        </script>
        <?php
        endif;
    }
}
add_action('wp_head', 'google_analytics_code');

// Facebook analytics kód
function facebook_analytics_code()
{
    if (pref('fb-api-id') != null && pref('fb-api-id') != "")
    {
        ?>
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '<?php print_pref('fb-api-id'); ?>');
          fbq('track', 'PageView');
        </script>
        <noscript>
          <img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=<?php print_pref('fb-api-id'); ?>&ev=PageView&noscript=1"/>
        </noscript>
        <?php
    }
}
add_action('wp_head', 'facebook_analytics_code');

// Woocommerce CSS deaktiválása
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

// Karbantartás mód
add_action('get_header', 'ryck_maintenance_mode');
function ryck_maintenance_mode() {
    if ((pref('maintenance-mode') == true) && (!current_user_can('edit_themes') || !is_user_logged_in())) {
        wp_die('<h1>'.pref('maintenance-title').'</h1><br />'.pref('maintenance-content'));
    }
}

// Találatok számának elrejtése
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

// the_content helyettesítése
add_filter('the_content', 'ryck_content');
function ryck_content($content) {
	echo do_shortcode($content);
}

// CSS & jQuery betöltése
add_action('wp_enqueue_scripts', 'ryckstore_load_scripts');
function ryckstore_load_scripts() {
	global $template_root_folder; global $assets_folder; global $css_folder; global $js_folder; global $IS_CHILD_THEME;

    if (!isset($IS_CHILD_THEME) || !$IS_CHILD_THEME) {
    	wp_enqueue_style('ryckstore-style', get_stylesheet_uri());
        wp_enqueue_style('ryckstore-mobile-style', $template_root_folder.'style.mobile.css');
    }

	wp_enqueue_style('bootstrap-base', $css_folder.'bootstrap.custom.css');
    wp_enqueue_style('fontawesome', $css_folder.'all.min.css');
	wp_enqueue_script('jquery');
}

// Eszközfelismerés
add_action('wp_footer', 'ryckstore_footer_scripts');
function ryckstore_footer_scripts() {
	?><script>
		jQuery(document).ready(function($) {
			var deviceAgent = navigator.userAgent.toLowerCase();

			if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
				$("html").addClass("ios");
				$("html").addClass("mobile");
			}
			if (navigator.userAgent.search("MSIE") >= 0) {
				$("html").addClass("ie");
			}
			else if (navigator.userAgent.search("Chrome") >= 0) {
				$("html").addClass("chrome");
			}
			else if (navigator.userAgent.search("Firefox") >= 0) {
				$("html").addClass("firefox");
			}
			else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
				$("html").addClass("safari");
			}
			else if (navigator.userAgent.search("Opera") >= 0) {
				$("html").addClass("opera");
			}
		});
	</script><?php
}

// Cím és elválasztó
add_filter('document_title_separator', 'ryckstore_document_title_separator');
function ryckstore_document_title_separator($sep) {
	$sep = pref('title-separator');
	return $sep;
}
add_filter('the_title', 'ryckstore_title');
function ryckstore_title( $title ) {
	if ( $title == '' ) {
		return '...';
	}
	else {
		return $title;
	}
}

// Tovább linkek
add_filter( 'the_content_more_link', 'ryckstore_read_more_link' );
function ryckstore_read_more_link() {
	if (!is_admin()) {
		return '<a href="' . esc_url(get_permalink()) . '" class="more-link">...</a>';
	}
}
add_filter('excerpt_more', 'ryckstore_excerpt_read_more_link');
function ryckstore_excerpt_read_more_link( $more ) {
	if (!is_admin()) {
		global $post;
		return '<a href="' . esc_url(get_permalink($post->ID)) . '" class="more-link">...</a>';
	}
}

// Képméret korrekció
add_filter('intermediate_image_sizes_advanced', 'ryckstore_image_insert_override');
function ryckstore_image_insert_override($sizes) {
	unset($sizes['medium_large']);
	return $sizes;
}

// Widgetek
add_action('widgets_init', 'ryckstore_widgets_init');
function ryckstore_widgets_init() {
	register_sidebar(array(
		'name' => __v('Sidebar').' 1',
		'id' => 'sidebar-widgets-1',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

    register_sidebar(array(
		'name' => __v('Sidebar').' 2',
		'id' => 'sidebar-widgets-2',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

    register_sidebar(array(
		'name' => __v('Sidebar').' 3',
		'id' => 'sidebar-widgets-3',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

    register_sidebar(array(
		'name' => __v('Sidebar').' 4',
		'id' => 'sidebar-widgets-4',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

    register_sidebar(array(
		'name' => __v('Sidebar').' 5',
		'id' => 'sidebar-widgets-5',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

    register_sidebar(array(
		'name' => __v('Sidebar').' 6',
		'id' => 'sidebar-widgets-6',
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}

// Menü elemek hely alapján
// menu_by_location('main-menu');
function menu_by_location($theme_location = 'main-menu')
{
    if (($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location])) {
        $menu = get_term($locations[$theme_location], 'nav_menu');
        return wp_get_nav_menu_items($menu->term_id);
    }
    return array();
}

// Menü elemek ID alapján
// menu_by_id(21);
function menu_by_id($menu_id = -1)
{
    if ($menu_id === -1)
        return array();

    return wp_get_nav_menu_items($menu_id);
}

// Kommentek száma, ping & válasz script
add_action('comment_form_before', 'ryckstore_enqueue_comment_reply_script');
function ryckstore_enqueue_comment_reply_script() {
	if (get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
function ryckstore_custom_pings( $comment ) {
	?><li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li><?php
}
add_filter( 'get_comments_number', 'ryckstore_comment_count', 0 );
function ryckstore_comment_count( $count ) {
	if (!is_admin()) {
		global $id;
		$get_comments = get_comments('status=approve&post_id=' . $id);
		$comments_by_type = separate_comments($get_comments);
		return count($comments_by_type['comment']);
	}
	else {
		return $count;
	}
}

// Fordított szöveg
function ___($string) {
	esc_html_e($string, 'ryckstore');
}
function __v($string) {
	return esc_html__($string, 'ryckstore');
}

// Alapértelmezett szöveg
function defval($value, $default, $use_default = false)
{
	if ($use_default)
		return $default;

	if (!is_string($value) && !is_numeric($value))
		return $default;

	if ($value == '' || $value == null)
		return $default;

	return $value;
}

function shortcode_preview($shortcode_text = '[shortcode]')
{
    ob_start();
    ?><pre style="padding:5px 10px;border:1px solid #ccc;background-color:#333;color:#eee;font-size:1.15em;display:inline-block;border-radius:5px;-o-border-radius:5px;-ms-border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;margin: 0 0 0 10px;vertical-align:middle"><?php echo $shortcode_text; ?></pre>
    <?php return ob_get_clean();
}

function fa_example_shortcodes()
{
    $normal_preview = shortcode_preview('[fa]address-card[/fa]');
    $color_preview = shortcode_preview('[fa color="#333"]address-card[/fa]').shortcode_preview('[fa color="green"]address-card[/fa]');
    $regular_preview = shortcode_preview('[far]address-card[/far]');
    $solid_preview = shortcode_preview('[fas]address-card[/fas]');
    $multi_preview = shortcode_preview('[fas color="#333"]address-card[/fas]');
    return "<strong>Előre beállított stílus:</strong></br>egyszerű ikon: $normal_preview</br>színes ikon: $color_preview</br></br><hr></br><strong>Választott stílus:</strong></br>körvonalazott ikon: $regular_preview</br>telített ikon: $solid_preview</br>telített színes ikon: $multi_preview";
}

function contact_shortcode_preview()
{
    $auto_preview = shortcode_preview('[kapcsolat]');
    $list_preview = shortcode_preview('[kapcsolat type="list"]');
    $inline_preview = shortcode_preview('[kapcsolat type="inline"]');
    return "Automatikus elrendezés: $auto_preview</br>Egymás alatt: $list_preview</br>Egymás mellett: $inline_preview";
}

// Ikon összeállítás
function build_icon($icon_name, $color_data = '', $style = 'solid', $extra_classes = '')
{
    $i_class = $style == 'solid' ? 'fas' : ($style == 'regular' ? 'far' : 'fab');

    $icon_classes = $icon_name;
    if (is_string($extra_classes) && $extra_classes != '')
        $icon_classes .= " $extra_classes";

    return "<i class=\"$i_class fa-$icon_classes\"$color_data></i>";
}
function print_icon($icon_name, $color_data = '', $style = 'solid', $extra_classes = '')
{
    print build_icon($icon_name, $color_data, $style, $extra_classes);
}

// Link összeállítás
function build_link($link_label, $link_url, $link_target = null, $link_classes = '', $link_attributes = '')
{
    if (!is_string($link_label) || $link_label == '' || !is_string($link_url) || $link_url == '')
        return '';

    $all_attr_array = array();

    if (is_string($link_target) && $link_target != '')
        $all_attr_array []= "target=\"$link_target\"";
    if (is_string($link_classes) && $link_classes != '')
        $all_attr_array []= "class=\"$link_classes\"";
    if (is_string($link_attributes) && $link_attributes != '')
        $all_attr_array []= $link_attributes;

    $all_attr_string = implode(' ', $all_attr_array);

    return "<a href=\"$link_url\" $all_attr_string>$link_label</a>";
}
function print_link($link_label, $link_url, $link_target = null, $link_classes = '', $link_attributes = '')
{
    print build_link($link_label, $link_url, $link_target, $link_classes, $link_attributes);
}

// Értékelés csillagok HTML
function rating_stars($rating = 2.5, $max_rating = 5, $draw_text = true, $color = null)
{
    $color_data = '';

    if ($color !== null && is_string($color) && $color != '')
        $color_data = " color=\"$color\"";

    $stars = array(
    	'1' => "[fas$color_data]star[/fas]",
    	'0.5' => "[fas$color_data]star-half-alt[/fas]",
    	'0' => "[far$color_data]star[/far]",
    );

    $rating_icons = array();
    $full_stars = floor(floatval($rating));
    $not_full_stars = ceil(floatval($rating) - $full_stars);
    $empty_stars = floatval($max_rating) - ($not_full_stars + $full_stars);

    for ($fs = 0; $fs < $full_stars; $fs++) {
    	$rating_icons []= $stars['1'];
    }
    for ($nfs = 0; $nfs < $not_full_stars; $nfs++) {
    	$rating_icons []= $stars['0.5'];
    }
    for ($es = 0; $es < $empty_stars; $es++) {
    	$rating_icons []= $stars['0'];
    }

    $after_text = $draw_text ? " <small>($rating)</small>" : '';

    return do_shortcode(implode('',$rating_icons)).$after_text;
}
function print_rating_stars($rating = 2.5, $max_rating = 5, $draw_text = true, $color = null)
{
    print rating_stars($rating, $max_rating, $draw_text, $color);
}

// Mennyisés gombok
function qty_minus_button()
{
    qty_button(-1, 'minus', 'minus');
}
function qty_plus_button()
{
    qty_button(1, 'plus', 'plus');
}
function qty_button($qty = 1, $class_name = 'plus', $icon_name = 'plus')
{
    if (pref('product-page-cart-qtybtns') == true)
    {
        $icon_html = build_icon($icon_name);
        print "<button type=\"button\" class=\"just_icon nostyle qty qty_$class_name\" onclick=\"productQtyAdd($qty);\">$icon_html</button>";
    }
}

function is_blog () {
    return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

// Oldalsávok funkció
function sidebar_left()
{
    sidebars('left');
}
function sidebar_right()
{
    sidebars('right');
}
function sidebar_switches()
{
    global $has_left_sidebar, $has_right_sidebar;

    $has_left_sidebar = sidebars('left', true);
    $has_right_sidebar = sidebars('right', true);

    if ($has_left_sidebar || $has_right_sidebar):
?>
    <div class="mobile-sidebar-switches" style="display:none;">
        <?php if ($has_left_sidebar): ?><button type="button" class="nostyle sidebar-switch left" onclick="setObjectClass('body #container > .sidebar-left', 'showen', true);"><?php print_icon('angle-double-right'); ?></button><?php endif; ?>
        <?php if ($has_right_sidebar): ?><button type="button" class="nostyle sidebar-switch right" onclick="setObjectClass('body #container > .sidebar-right', 'showen', true);"><?php print_icon('angle-double-left'); ?></button><?php endif; ?>
    </div>
<?php
    endif;
}
function sidebars($side = 'left', $check = false)
{
    if (is_shop() || is_product_category() || is_product_tag())
        return page_sidebar('webshop-page', $side, $check);

    elseif (is_product())
        return page_sidebar('product-page', $side, $check);

    elseif (is_cart())
        return page_sidebar('cart-page', $side, $check);

    elseif (is_checkout())
        return page_sidebar('checkout-page', $side, $check);

    elseif (is_page())
        return page_sidebar('simple-page', $side, $check);

    elseif (is_single())
        return page_sidebar('blogpost-single', $side, $check);

    elseif (is_archive() || is_blog ())
        return page_sidebar('blogpost-archive', $side, $check);

    return false;
}
// Oszlopok
add_filter('body_class', 'add_columns_classes');
function add_columns_classes($classes) {
    $prefname = null;

    // Terméklista
    if (is_shop() || is_product_category() || is_product_tag())
        $prefname = 'webshop-page-sidebar-mode';

    // Termék oldal
    elseif (is_product())
        $prefname = 'product-page-sidebar-mode';

    // Kosár oldal
    elseif (is_cart())
        $prefname = 'cart-page-sidebar-mode';

    // Fizetés oldal
    elseif (is_checkout())
        $prefname = 'checkout-page-sidebar-mode';

    elseif (is_page())
        $prefname = 'simple-page-sidebar-mode';

    elseif (is_single())
        $prefname = 'blogpost-single-sidebar-mode';

    elseif (is_archive() || is_blog ())
        $prefname = 'blogpost-archive-sidebar-mode';


    if ($prefname != null)
    {
        $columns = 0;
        $column_classes = array();
        $pref = pref($prefname);
        if (is_string($pref) && $pref != '')
        {
            if ($pref == 'left' || $pref == 'right')
            {
                $columns = 2;
                $column_classes []= "column-$pref";
            }
            elseif ($pref == 'left-right')
            {
                $columns = 3;
                $column_classes []= 'column-left';
                $column_classes []= 'column-right';
            }
        }
        $classes []= 'columns-'.$columns;
        foreach($column_classes as $column_class)
            $classes []= $column_class;
    }

    return $classes;
}
function page_sidebar($page_id, $side = 'left', $check = false)
{
    if (!is_string($page_id) || $page_id == '' || !is_string($side) || $side == '')
        return false;

    $sidebar_mode = pref("$page_id-sidebar-mode");
    if (!is_string($sidebar_mode) || $sidebar_mode == '')
    	$sidebar_mode = 'none';

    if (strpos($sidebar_mode, $side) !== false)
    {
    	$sidebar_id = pref("$page_id-sidebar-$side");
    	if (!is_string($sidebar_id) || $sidebar_id == '')
    		$sidebar_id = 'sidebar-widgets-1';

        if (!$check)
            draw_sidebar($sidebar_id, $side);

        return true;
    }

    return false;
}
function draw_sidebar($sidebar_id = 'sidebar-widgets-1', $side = null)
{
    $oside = is_string($side) ? $side : 'unknown';

    if ($side == null || !is_string($side))
        $side = '';
    else
        $side = " sidebar-$side";
    print "<div class=\"sidebar$side\" sidebar-id=\"$sidebar_id\" sidebar-side=\"$oside\">";
    print '<button type="button" class="nostyle close-sidebar" onclick="setObjectClass(\'body #container > .sidebar\', \'showen\', false);" style="display:none;">' . build_icon('times') . '</button>';
    print '<div class="sidebar_inner"><ul class="widget_list">';

    ob_start();
    dynamic_sidebar($sidebar_id);
    $sidebar_content = ob_get_clean();
    echo do_shortcode($sidebar_content);

    print '</ul></div></div>';
}

// Üres kép
function placeholder_image()
{
    ?><div class="placeholder"></div><?php
}

// Szállítási limitációk
add_filter('woocommerce_package_rates', 'woocommerce_shipping_limitations', 10, 2);
function woocommerce_shipping_limitations($rates, $package)
{
    // Beállítások betöltése
    $shipping_limits = pref('shipping-limits');
    $shl_debug_mode = pref('shipping-limits-debug') == 1;

    // Összeszedjük a szabályokat
    // szállítási-osztály|szállítási-mód-1,szállítási-mód-2
    $limit_array = array();

    if (is_string($shipping_limits) && strlen($shipping_limits) > 0)
    {
        $limit_rows = explode(PHP_EOL, $shipping_limits);

        foreach ($limit_rows as $limit_row) {
            if (strpos($limit_row, '|') !== false)
            {
                $limit_row_parts = explode('|', $limit_row);
                if (sizeof($limit_row_parts) === 2)
                {
                    $limit_shipping_class = $limit_row_parts[0];
                    $limit_shipping_methods = array();

                    if (strpos($limit_row_parts[1], ',') !== false)
                        $limit_shipping_methods = explode(',', $limit_row_parts[1]);
                    else
                        $limit_shipping_methods = array($limit_row_parts[1]);

                    $limit_array []= array('shipping-class' => $limit_shipping_class, 'shipping-methods' => $limit_shipping_methods);
                }
            }
        }
    }

    if ($shl_debug_mode) {
        print '<label>limits:</label>';
        print '<pre style="max-height:200px;margin-bottom:10px;">'; print_r($limit_array); print '</pre>';
    }

    // Ha nincs limitáció, visszalépünk
    if (sizeof($limit_array) == 0)
        return $rates;
	
    // Összeszedjük a kosárban lévő termékek osztályait
	$shipping_class_in_cart = array();

	foreach(WC()->cart->get_cart() as $key => $values) {
        $shipping_class = $values['data']->get_shipping_class();
        if ($shipping_class === null || $shipping_class === '' || !isset($shipping_class))
            continue;

	    $shipping_class_in_cart []= $shipping_class;
	}

    // Ha nincsenek szállítási osztályok, visszalépünk
    if (sizeof($shipping_class_in_cart) == 0)
        return $rates;

    if ($shl_debug_mode) {
        print '<label>classes in cart:</label>';
        print '<pre style="max-height:200px;margin-bottom:10px;">'; print_r($shipping_class_in_cart); print '</pre>';

        print '<label>before:</label>';
        print '<pre style="max-height:200px;margin-bottom:10px;">'; print_r($rates); print '</pre>';
    }

    // Ha van egyezés, azt a módot deaktiváljuk
    foreach($limit_array as $limit)
    {
		foreach ($rates as $rate_key => $rate) {
			if (is_object($rate) &&  in_array($rate->method_id, $limit['shipping-methods'])) {
				unset($rates[$rate_key]);
			}
		}
		
    }

    if ($shl_debug_mode) {
        print '<label>after:</label>';
        print '<pre style="max-height:200px;margin-bottom:10px;">'; print_r($rates); print '</pre>';
    }

    // Visszaküldjük az eredményt
    return $rates;
}

add_filter( 'woocommerce_add_to_cart_fragments', 'mini_cart_button_fragment', 10, 1 );
function mini_cart_button_fragment($fragments)
{
    $fragments['.ryck-minicart-button .ryck-minicart-total'] = '<label class="ryck-minicart-total">' . WC()->cart->get_cart_total() . '</label>';
    $fragments['.ryck-minicart-button .ryck-minicart-items > .ryck-minicart-items-count'] = '<span class="ryck-minicart-items-count">' . WC()->cart->get_cart_contents_count() . '</span>';
    return $fragments;
}

function productsSearch($search_string, $search_term_id, $search_term_type, $max_products = -1, $just_ids = false)
{
    if (!is_string($search_string) || $search_string == "")
        return array();

    $all_products = array();

    $args = array(
        'post_type'  => 'product',
        'numberposts' => -1
    );

    if (is_numeric($search_term_id) && $search_term_id !== -1 && is_string($search_term_type) && $search_term_type !== '')
    {
        $args['tax_query'] = array(
                                array(
                                    'taxonomy'      => $search_term_type,
                                    'field' => 'term_id',
                                    'terms'         => intval($search_term_id),
                                    'operator'      => 'IN'
                                ),
                                array(
                                    'taxonomy'      => 'product_visibility',
                                    'field'         => 'slug',
                                    'terms'         => 'exclude-from-catalog',
                                    'operator'      => 'NOT IN'
                                )
                            );
    }

    $all_products = get_posts($args);

    $products = array();
    foreach($all_products as $product)
    {
        if ($max_products > 0 && sizeof($products) >= intval($max_products))
            break;

        $found = false;

        if (strpos(strtolower($product->post_title), strtolower($search_string)) !== false)
            $found = true;
        elseif (strpos(strtolower($product->post_content), strtolower($search_string)) !== false)
            $found = true;
        elseif (strpos(strtolower($product->post_excerpt), strtolower($search_string)) !== false)
            $found = true;

        if ($found)
            array_push($products, $just_ids ? $product->ID : $product);
    }

    return $products;
}
function getProductsSearchData($product_array, $search_string = false)
{
    $search_result_array = array();

    foreach($product_array as $product_object)
    {
        $product_name = $product_object->post_title;
        if ($search_string !== false && strlen($search_string) > 0)
        {
            $string_positions = strpos_all(strtolower($product_name), strtolower($search_string));
            $string_length = strlen($search_string);
            for ($i = sizeof($string_positions) - 1; $i >= 0; $i--)
            {
                $old_string = substr($product_name, $string_positions[$i], $string_length);
                $product_name = substr_replace($product_name, $old_string, $string_positions[$i], $string_length);
            }
        }
        array_push($search_result_array, array("ID" => $product_object->ID, "name" => $product_name, "url" => $product_object->guid));
    }

    return $search_result_array;
}
