<?php
/**
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


$is_ajax = pref('header-searchbar-useajax') == true; // TODO
$dropdown_tax = pref('header-searchbar-showtaxonomy');
$has_select = $dropdown_tax !== 'none';


?><div class="ryck-product-search<?php if (!$has_select): ?> no-dropdown<?php endif; ?>">
	<form role="search" method="get" action="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">
		<div class="search-bar-input">
			<input type="text" name="s" value="<?php echo get_search_query(); ?>" placeholder="<?php ___('Keresés...'); ?>" />
		</div>
		<?php if ($has_select): ?><div class="search-bar-select"><?php

			$actual_tax = is_string($dropdown_tax) && $dropdown_tax != '' ? $dropdown_tax : 'product_cat';

			$args = array(
			   'taxonomy' => $actual_tax,
			   'name' => $actual_tax,
			   'value_field' => 'slug',
			   'class' => 'something'
			);

			wp_dropdown_categories( $args );

		?></div><?php endif; ?>
		<input type="hidden" name="post_type" value="product" />
		<button class="nostyle" type="submit"><?php print_icon('search'); ?></button>
	</form>
</div>
