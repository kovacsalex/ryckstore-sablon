<?php

namespace Elementor;

// Termék terméklapozó
class RyckMobileMegaMenu extends \Elementor\Widget_Base {

    // Azonosítás
    public function get_name() { return 'ryckmobilemegamenu'; }
    public function get_title() { global $cw_nameprefix; return "$cw_nameprefix Mobil Menü"; }
    public function get_icon() { return 'fa fa-list-ul'; }
    public function get_categories() { return [ 'ryck-basic' ]; }

    // Beállítások
    protected function _register_controls() {
        $this->start_controls_section(
			'design_section',
			[
				'label' => 'Testreszabás',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'label',
			[
				'label'     => 'Gomb neve',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: Menü, Főmenü',
			]
		);

        $this->add_control(
			'icon',
			[
				'label'     => 'FA Ikon neve',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: bars, chevron-down stb.',
			]
		);

		$this->end_controls_section();
    }

    // Megjelenítés
    protected function render() {

        $settings = $this->get_settings_for_display();

        $menu_label = is_string($settings['label']) && strlen($settings['label']) > 0 ? $settings['label'] : '';
        $menu_icon = is_string($settings['icon']) && strlen($settings['icon']) > 0 ? $settings['icon'] : 'bars';

        ?>
        <button type="button" class="mobile-mega-menu-switch" onclick="toggleMobileMenu();"><?php print $menu_label; print_icon($menu_icon); ?></button>
        <div id="mobile-mega-menu-content" style="display:none;">
            <?php

                $main_menu_items = menu_by_location();

                foreach($main_menu_items as $mm_item_index => $mm_item)
        		{
                    $mm_item_id = $mm_item->ID;
                    $mm_item_name = $mm_item->title;
                    $mm_item_url = $mm_item->url;

                    $mm_item_submenu_id = pref("mainmenu-complex-mitem-$mm_item_id");
                    $mm_submenu_items = menu_by_id($mm_item_submenu_id);
                    $mm_items_has_submenus = is_array($mm_submenu_items) && sizeof($mm_submenu_items) > 0;

                    ?>
                    <div class="mobile-mega-menu-item">
                        <div class="title-link">
                            <a href="<?php echo $mm_item_url; ?>"><?php echo $mm_item_name; ?></a>
                            <?php if ($mm_items_has_submenus): ?><button type="button" class="mobile-mega-menu-item-toggler" onclick="jQuery('.mobile-complex-menu_submenu.item-<?php echo $mm_item_id; ?>').toggleClass('opened');"><?php print_icon('plus'); ?></button><?php endif; ?>                        </div>
                        <?php print_mega_menu_content($mm_item_id, 'mobile-complex-menu', true); ?>
                    </div>
                    <?php
                }

            ?>
        </div>
        <?php
    }
}

?>
