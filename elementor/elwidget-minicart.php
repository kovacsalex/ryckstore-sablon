<?php

namespace Elementor;

// Termék terméklapozó
class RyckMiniCart extends \Elementor\Widget_Base {

    // Azonosítás
    public function get_name() { return 'ryckminicart'; }
    public function get_title() { global $cw_nameprefix; return "$cw_nameprefix Mini Kosár"; }
    public function get_icon() { return 'fa fa-shopping-cart'; }
    public function get_categories() { return [ 'ryck-woo' ]; }

    // Beállítások
    protected function _register_controls() {
        $this->start_controls_section(
			'button',
			[
				'label' => 'Gomb',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'icon',
			[
				'label'       => 'Kosár ikon',
                'type'        => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: shopping-cart, shopping-basket stb.',
                'default'     => 'shopping-basket',
			]
		);

		$this->end_controls_section();
    }

    // Megjelenítés
    protected function render() {

        $settings = $this->get_settings_for_display();

        global $woocommerce;
        $cart_item_count = $woocommerce->cart->cart_contents_count;

        ?>
        <button type="button" class="nostyle ryck-minicart-button" onclick="toggleCart();">
            <label class="ryck-minicart-total"><?php echo $woocommerce->cart->get_cart_total(); ?></label>
            <div class="ryck-minicart-items">
                <?php print_icon($settings['icon'], '', 'solid', 'ryck-minicart-items-icon'); ?>
                <span class="ryck-minicart-items-count"><?php echo $cart_item_count; ?></span>
            </div>
        </button>
        <?php
    }
}

?>
