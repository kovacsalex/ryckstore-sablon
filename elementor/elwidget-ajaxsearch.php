<?php

namespace Elementor;

// Termék terméklapozó
class RyckAjaxSearch extends \Elementor\Widget_Base {

    // Azonosítás
    public function get_name() { return 'ryckajaxsearch'; }
    public function get_title() { global $cw_nameprefix; return "$cw_nameprefix Ajax Kereső"; }
    public function get_icon() { return 'fa fa-search'; }
    public function get_categories() { return [ 'ryck-basic' ]; }

    // Beállítások
    protected function _register_controls() {
        $this->start_controls_section(
			'parameters',
			[
				'label' => 'Paraméterek',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'search_url',
			[
				'label'     => 'Keresőoldal URL',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: /kereses/',
                'default' => '/kereses/',
			]
		);

        $this->add_control(
			'taxonomy',
			[
				'label'     => 'Megjelenítendő csoportok',
                'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'product_cat'    => 'Termék kategóriák',
                    'product_tag'    => 'Termék címkék',
				],
				'default' => 'product_cat',
			]
		);

        $this->add_control(
			'sort_property',
			[
				'label' => 'Elemek rendezése',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'rand'         => 'Véletlenszerű',
					'title'        => 'Név alapján',
					'date'         => 'Dátum alapján',
				],
				'default' => 'title',
			]
		);

        $this->add_control(
			'sort_direction',
			[
				'label' => 'Rendezés iránya',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'asc'          => 'Növekvő',
					'desc'         => 'Csökkenő',
				],
				'default' => 'asc',
			]
		);

        $this->add_control(
			'term_max_items',
			[
				'label'     => 'Csoportok száma',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 1,
				'max'       => 300,
				'step'      => 1,
				'default'   => 5,
			]
		);

        $this->add_control(
			'max_items',
			[
				'label'     => 'Ajax találatok száma',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 1,
				'max'       => 100,
				'step'      => 1,
				'default'   => 5,
			]
		);

		$this->end_controls_section();

        $this->start_controls_section(
			'design',
			[
				'label' => 'Megjelenés',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'search_height',
			[
				'label'     => 'Kereső magassága (%)',
                'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 90,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 30,
				],
			]
		);

        $this->add_control(
			'input_width',
			[
				'label'     => 'Szövegmező szélessége (%)',
                'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ '%' ],
				'range' => [
					'%' => [
						'min' => 10,
						'max' => 90,
					],
				],
				'default' => [
					'unit' => '%',
					'size' => 70,
				],
			]
		);

        $this->add_control(
			'input_placeholder',
			[
				'label'     => 'Szövegmező placeholder',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: Keresés...',
			]
		);

        $this->add_control(
			'ajax_notfound_msg',
			[
				'label'     => '"Nincs találat" üzenet',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: Nincs találat...',
                'default' => 'Nincs találat...',
			]
		);

        $this->add_control(
			'button_width',
			[
				'label'     => 'Gomb szélessége (px)',
                'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 10,
						'max' => 90,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 32,
				],
			]
		);

        $this->add_control(
			'input_order',
			[
				'label' => 'Mezők sorrendje',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'input_right'          => 'Legördülő, Szövegmező',
					'input_left'         => 'Szövegmező, Legördülő',
				],
				'default' => 'input_right',
			]
		);

        $this->add_control(
			'dropdown_name',
			[
				'label'     => 'Legördülő neve',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: Kategóriák, Címkék',
			]
		);

        $this->add_control(
			'dropdown_icon',
			[
				'label'     => 'Legördülő ikonja',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: caret-down, plus',
			]
		);

        $this->add_control(
			'button_icon',
			[
				'label' => 'Gomb FA ikon',
                'type'        => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: search',
                'default'     => 'search',
			]
		);

		$this->end_controls_section();
    }

    // Megjelenítés
    protected function render() {

        $settings = $this->get_settings_for_display();
        $uid = uniqid('ras-');
        $settings['uid'] = $uid;

        ?>
        <div id="<?php echo $uid; ?>" class="ryck-ajax-search" style="height:<?php echo $settings['search_height']['size']; ?>px;" selected-term="-1">
            <form method="get" action="<?php echo $settings['search_url']; ?>" autocomplete="off">
                <?php
                    if ($settings['input_order'] == 'input_left'): $this->input_field($settings); else: $this->dropdown_field($settings); endif;
                    if ($settings['input_order'] != 'input_left'): $this->input_field($settings); else: $this->dropdown_field($settings); endif;
                    $this->search_button($settings);
                ?>
                <div class="ryck-ajax-dropdown" style="<?php $this->dropdown_style($settings); ?>">
                    <?php $this->dropdown_items($settings); ?>
                </div>
                <div class="ryck-ajax-items" style="<?php $this->items_style($settings); ?>">
                    <label class="empty-search"><?php echo $settings['ajax_notfound_msg']; ?></label>
                    <content class="results"></content>
                </div>
                <input type="hidden" class="search-term-id" name="term-id" value="-1"></input>
                <input type="hidden" class="search-term-type" name="term-type" value="<?php echo $settings['taxonomy']; ?>"></input>
            </form>
        </div>
        <?php
    }
    protected function input_field($settings) {
        ?><input type="text" name="q" class="ajax-input-field" u-id="<?php echo $settings['uid']; ?>" placeholder="<?php echo $settings['input_placeholder']; ?>" style="width:<?php echo $settings['input_width']['size']; ?>%;"></input><?php
    }
    protected function dropdown_field($settings) {
        ?><button type="button" class="nostyle ajax-term-select" onclick="jQuery('#<?php echo $settings['uid']; ?>').toggleClass('terms-opened');" default-name="<?php echo $settings['dropdown_name']; ?>" style="width:calc( 100% - ( <?php echo $settings['input_width']['size']; ?>% + <?php echo $settings['button_width']['size']; ?>px ) );"><span><?php echo $settings['dropdown_name']; ?></span><?php print_icon($settings['dropdown_icon']); ?></button><?php
    }
    protected function search_button($settings) {
        ?><button type="submit" class="nostyle ajax-search-button" style="width:<?php echo $settings['button_width']['size']; ?>px;"><?php print_icon($settings['button_icon']); ?></button><?php
    }
    protected function dropdown_style($settings) {
        $left_param = '';
        $right_param = '';

        if ($settings['input_order'] != 'input_left')
        {
            $left_param = '0px';
            $right_param = 'calc( ' . $settings['input_width']['size'] . '% + ' . $settings['button_width']['size'] . 'px )';
        }
        else
        {
            $left_param = $settings['input_width']['size'] . '%';
            $right_param = $settings['button_width']['size'] . 'px';
        }

        ?>left:<?php echo $left_param; ?>;right:<?php echo $right_param; ?>;<?php
    }
    protected function items_style($settings) {
        $left_param = '';
        $right_param = '';

        if ($settings['input_order'] != 'input_left')
        {
            $left_param = 'calc( 100% - ( ' . $settings['input_width']['size'] . '% + ' . $settings['button_width']['size'] . 'px ) )';
            $right_param = $settings['button_width']['size'] . 'px';
        }
        else
        {
            $left_param = '0px';
            $right_param = 'calc( 100% -  ' . $settings['input_width']['size'] . '% )';
        }

        ?>left:<?php echo $left_param; ?>;right:<?php echo $right_param; ?>;<?php
    }
    protected function dropdown_items($settings) {

        ?><term t-id="-1" u-id="<?php echo $settings['uid']; ?>">Összes</term><?php

        $terms = array();

        $args = array(
            'taxonomy'     => $settings['taxonomy'],
            'orderby'      => $settings['sort_property'],
            'order'        => $settings['sort_direction'],
            'show_count'   => 0,
            'pad_counts'   => 0,
            'hierarchical' => 0,
            'title_li'     => '',
            'hide_empty'   => 0,
        );

        $all_categories = get_categories($args);
        foreach ($all_categories as $index => $cat) {
            if ($settings['max_items'] != -1 && $index >= intval($settings['term_max_items']))
                break;

            //$menu_title = $cat->name;
            $terms []= array(
                'id' => $cat->term_id,
                'name' => $cat->name
            );
        }

        foreach($terms as $term)
        {
            ?><term t-id="<?php print $term['id']; ?>" u-id="<?php echo $settings['uid']; ?>"><?php echo $term['name']; ?></term><?php
        }
    }
}

?>
