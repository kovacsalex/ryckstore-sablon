<?php

namespace Elementor;

// Termék terméklapozó
class RyckProductGrid extends \Elementor\Widget_Base {

    // Azonosítás
    public function get_name() { return 'ryckproductgrid'; }
    public function get_title() { global $cw_nameprefix; return "$cw_nameprefix Terméklapozó"; }
    public function get_icon() { return 'fa fa-th'; }
    public function get_categories() { return [ 'ryck-woo' ]; }

    // Beállítások
    protected function _register_controls() {
        $this->start_controls_section(
			'content_section',
			[
				'label' => 'Tartalom',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'columns',
			[
				'label'     => 'Oszlopok száma',
                'type' => \Elementor\Controls_Manager::NUMBER,
                'min'       => 1,
				'max'       => 12,
				'step'      => 1,
				'default'   => 4,
			]
		);

        $this->add_control(
			'rows',
			[
				'label'     => 'Sorok száma',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 1,
				'max'       => 10,
				'step'      => 1,
				'default'   => 1,
			]
		);

		$this->add_control(
			'height',
			[
				'label'     => 'Sormagasság (px)',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 100,
				'max'       => 600,
				'step'      => 1,
				'default'   => 300,
			]
		);

		$this->add_control(
			'mobile_height',
			[
				'label'     => 'Mobil magasság (%)',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 10,
				'max'       => 100,
				'step'      => 1,
				'default'   => 100,
			]
		);

        $this->add_control(
			'margin',
			[
				'label'     => 'Elemek közti távolság',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 0,
				'max'       => 100,
				'step'      => 5,
				'default'   => 10,
			]
		);

		$this->add_control(
			'pages',
			[
				'label'     => 'Oldalak száma',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => 1,
				'max'       => 10,
				'step'      => 1,
				'default'   => 3,
			]
		);

        $this->add_control(
			'query',
			[
				'label' => 'Megjelenítendő termékek',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'all'          => 'Minden termék',
					'sale'         => 'Akciós termékek',
					'featured'     => 'Kiemelt termékek',
					'category'     => 'Kategória alapján',
					'tag'          => 'Címke alapján',
                    'author'       => 'Szerző alapján'
				],
				'default' => 'all',
			]
		);

        $this->add_control(
			'sort_property',
			[
				'label' => 'Termékek rendezése',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'rand'         => 'Véletlenszerű',
					'title'        => 'Név alapján',
					'date'         => 'Dátum alapján',
					'author'       => 'Szerző alapján',
					'price'        => 'Ár alapján',
					'sales'        => 'Eladások alapján',
				],
				'default' => 'title',
			]
		);

        $this->add_control(
			'sort_direction',
			[
				'label' => 'Rendezés iránya',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'asc'          => 'Növekvő',
					'desc'         => 'Csökkenő',
				],
				'default' => 'asc',
			]
		);

		$this->end_controls_section();

        $this->start_controls_section(
			'controls_section',
			[
				'label' => 'Vezérlés',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'buttons',
			[
				'label' => 'Nyilak',
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => 'Be',
				'label_off' => 'Ki',
				'return_value' => true,
				'default' => true,
			]
		);

        $this->add_control(
			'dots',
			[
				'label' => 'Pontok',
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => 'Be',
				'label_off' => 'Ki',
				'return_value' => true,
				'default' => true,
			]
		);

		$this->end_controls_section();
    }

    // Megjelenítés
    protected function render() {

        $settings = $this->get_settings_for_display();

        $column_num = intval($settings['columns']);
        $margins = intval($settings['margin']);
        $marginspx = $margins.'px';

        $product_per_page = $column_num * intval($settings['rows']);
        $all_product_count = intval($settings['pages']) * $product_per_page;
        $actual_product_count = 0;
        $actual_page_count = 0;

        $query_args = array(
            'post_type'      => 'product',
            'posts_per_page' => $all_product_count,
            'nopaging'       => false,
            'post_status'    => 'publish',
            'order'          => $settings['sort_direction'],
        );

        // Honnan vegyük a termékeket?
        if ($settings['query'] != 'all')
        {
            switch ($settings['query']) {

                // Akciós termékek
                case 'sale':
                    $query_args['meta_query'] = array(
                                                    'relation' => 'OR',
                                                    array(
                                                        'key'           => '_sale_price',
                                                        'value'         => 0,
                                                        'compare'       => '>',
                                                        'type'          => 'numeric'
                                                    ),
                                                    array(
                                                        'key'           => '_min_variation_sale_price',
                                                        'value'         => 0,
                                                        'compare'       => '>',
                                                        'type'          => 'numeric'
                                                    )
                                                );
                    break;


                case 'featured':
                    $query_args['post__in'] = wc_get_featured_product_ids();
                    break;

                case 'category':
                    // TODO
                    break;

                case 'tag':
                    // TODO
                    break;

                case 'author':
                    // TODO
                    break;

                default: break;
            }
        }

        // Mi alapján rendezzük a termékeket?
        if ($settings['sort_property'] != 'price' && $settings['sort_property'] != 'sales')
            $query_args['orderby'] = $settings['sort_property'];

        else
        {
            // Woo szempont
            // TODO
        }

        // print '<pre>'; print_r($query_args); print '</pre>';

        $loop = new \WP_Query($query_args);

        $pindex = 0;
        $opened = false;
        $first = true;

        $scrollid = uniqid('ryckscrollerwidget_');

        $height_raw = intval($settings['height']) * intval($settings['rows']);
        $height_raw += (intval($settings['rows']) > 0 ? intval($settings['bottom_padding']) * (intval($settings['rows']) - 1) : 0) * 2;
        $height_value = $height_raw.'px';
        $mobile_height_value = $settings['mobile_height'].'%';

        ?><style for-product-grid="<?php echo $scrollid; ?>">
        body[data-elementor-device-mode="tablet"] .elementor-widget-container > <?php echo "#$scrollid"; ?>,
        body[data-elementor-device-mode="desktop"] .elementor-widget-container > <?php echo "#$scrollid"; ?> { height:<?php echo $height_value; ?>; padding-bottom: 0; }
        body[data-elementor-device-mode="mobile"] .elementor-widget-container > <?php echo "#$scrollid"; ?> { height: initial; padding-bottom: <?php echo $mobile_height_value; ?>; }
        </style><?php

        echo "<div id=\"$scrollid\" class=\"ryckproduct-elementor-grid\" desktop-height=\"$height_value;\" mobile-height=\"$mobile_height_value\" mobile-selected=\"1\">";

        echo '<div class="ryckproduct-product-pages">';

        $page_id = 1;

        while ($loop->have_posts())
        {
            $actual_product_count ++;

            if ($pindex == 0)
            {
                $extraclass = $first ? ' selected' : '';
                echo "<div class=\"ryckproduct-page$extraclass\" page-id=\"$page_id\">";
                echo "<div class=\"ryckproduct-page-content flex-container\" style=\"--columns:$column_num;--margin:$marginspx;\">";
                $opened = true;
            }

            $extraclass2 = $actual_product_count == 1 ? ' selected' : '';
            echo "<div class=\"ryckproduct-mobile-page$extraclass2\" page-id=\"$actual_product_count\">";

            $loop->the_post();
            global $product;
            $rproduct = new \RyckProduct($product);
            $rproduct->display(true, false, 'flex-item');

            echo '</div>';

            $pindex++;

            if ($pindex >= $product_per_page)
            {
                echo '</div>';
                echo '</div>';
                $opened = false;
                $pindex = 0;
                $page_id++;
            }

            $first = false;
        }

        $actual_page_count = intval(ceil(floatval($actual_product_count) / floatval($product_per_page)));

        if ($opened)
            echo '</div></div>';

        echo '</div>';

        if ($settings['buttons'] == true)
        {
            echo '<div class="ryckproduct-controls buttons desktop">';
            echo '<button type="button" class="navigation left" onclick="scrollProductScroller(\'#'.$scrollid.'\',-1);">' . do_shortcode('[fa]chevron-left[/fa]') . '</button>';
            echo '<button type="button" class="navigation right" onclick="scrollProductScroller(\'#'.$scrollid.'\',1);">' . do_shortcode('[fa]chevron-right[/fa]') . '</button>';
            echo '</div>';

            echo '<div class="ryckproduct-controls buttons mobile">';
            echo '<button type="button" class="navigation left" onclick="scrollMobileScroller(\'#'.$scrollid.'\',-1);">' . do_shortcode('[fa]chevron-left[/fa]') . '</button>';
            echo '<button type="button" class="navigation right" onclick="scrollMobileScroller(\'#'.$scrollid.'\',1);">' . do_shortcode('[fa]chevron-right[/fa]') . '</button>';
            echo '</div>';
        }

        echo '</div>';

        if ($settings['dots'] == true)
        {
            echo '<div class="ryckproduct-controls dots desktop" scroll-id="#' . $scrollid . '">';
            for ($pi = 1; $pi <= $actual_page_count; $pi++)
            {
                $dot_plus_class = $pi == 1 ? ' current' : '';
                echo "<button type=\"button\" class=\"nostyle dot$dot_plus_class\" onclick=\"gotoProductScroller('#$scrollid', $pi);\" page-id=\"$pi\"></button>";
            }
            echo '</div>';

            echo '<div class="ryckproduct-controls dots mobile" scroll-id="#' . $scrollid . '">';
            for ($pi = 1; $pi <= $actual_product_count; $pi++)
            {
                $dot_plus_class = $pi == 1 ? ' current' : '';
                echo "<button type=\"button\" class=\"nostyle dot$dot_plus_class\" onclick=\"gotoMobileScroller('#$scrollid', $pi);\" mobile-page-id=\"$pi\"></button>";
            }
            echo '</div>';
        }

        wp_reset_postdata();
    }
}

?>
