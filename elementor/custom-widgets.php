<?php

$cw_nameprefix = '[RYCK]';

class RyckElementorWidgets
{
    protected static $instance = null;

	public static function get_instance() {
		if ( ! isset( static::$instance ) ) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	protected function __construct() {
        require_once('elwidget-ryckgrid.php');
        require_once('elwidget-ryckdropdown.php');
        require_once('elwidget-mobilemegamenu.php');
        require_once('elwidget-minicart.php');
        require_once('elwidget-ajaxsearch.php');
		add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets']);
	}

	public function register_widgets() {
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\RyckProductGrid() );
    	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\RyckDropdown() );
    	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\RyckMobileMegaMenu() );
    	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\RyckMiniCart() );
    	\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \Elementor\RyckAjaxSearch() );
	}
}

add_action( 'init', 'ryck_elementor_init' );
function ryck_elementor_init() {
	RyckElementorWidgets::get_instance();
}

function add_elementor_widget_categories($elements_manager)
{
	$elements_manager->add_category(
		'ryck-woo',
		[
			'title' => 'Ryck Woocommerce',
			'icon' => 'fa fa-shopping-bag',
		]
	);
	$elements_manager->add_category(
		'ryck-basic',
		[
			'title' => 'Ryck Alapok',
			'icon' => 'fa fa-commenting-o',
		]
	);

}
add_action( 'elementor/elements/categories_registered', 'add_elementor_widget_categories' );

?>
