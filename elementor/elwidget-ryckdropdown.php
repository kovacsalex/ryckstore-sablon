<?php

namespace Elementor;

// Termék terméklapozó
class RyckDropdown extends \Elementor\Widget_Base {

    // Azonosítás
    public function get_name() { return 'ryckdropdown'; }
    public function get_title() { global $cw_nameprefix; return "$cw_nameprefix Legördülő menü"; }
    public function get_icon() { return 'fa fa-bars'; }
    public function get_categories() { return [ 'ryck-woo' ]; }

    // Beállítások
    protected function _register_controls() {
        $this->start_controls_section(
			'content_section',
			[
				'label' => 'Tartalom',
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_control(
			'taxonomy',
			[
				'label'     => 'Megjelenítendő csoportok',
                'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'product_cat'    => 'Termék kategóriák',
                    'product_tag'    => 'Termék címkék',
                    'menu'           => 'WP Menü',
				],
				'default' => 'product_cat',
			]
		);

        $this->add_control(
			'wp_menu_id',
			[
				'label'     => 'WP Menü ID',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => -1,
				'max'       => 99999,
				'step'      => 1,
				'default'   => -1,
			]
		);

        $this->add_control(
			'sort_property',
			[
				'label' => 'Elemek rendezése',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'rand'         => 'Véletlenszerű',
					'title'        => 'Név alapján',
					'date'         => 'Dátum alapján',
				],
				'default' => 'title',
			]
		);

        $this->add_control(
			'sort_direction',
			[
				'label' => 'Rendezés iránya',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'asc'          => 'Növekvő',
					'desc'         => 'Csökkenő',
				],
				'default' => 'asc',
			]
		);

        $this->add_control(
			'max_items',
			[
				'label'     => 'Elemek maximális száma',
                'type'      => \Elementor\Controls_Manager::NUMBER,
				'min'       => -1,
				'max'       => 100,
				'step'      => 1,
				'default'   => 6,
			]
		);

        $this->add_control(
			'label',
			[
				'label'     => 'Legördülő neve',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: Kategóriák, Címkék',
			]
		);

        $this->add_control(
			'icon',
			[
				'label'     => 'FA Ikon neve',
                'type'      => \Elementor\Controls_Manager::TEXT,
				'placeholder' => 'pl: far fa-bars, fas fa-chevron-down stb.',
			]
		);

        $this->add_control(
			'link_open',
			[
				'label' => 'Linkek megnyitása',
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => [
					'_self'        => 'Azonos lapon',
					'_blank'       => 'Új lapon',
				],
				'default' => '_self',
			]
		);

		$this->end_controls_section();
    }

    // Megjelenítés
    protected function render() {

        $settings = $this->get_settings_for_display();

        // taxonomy
        // wp_menu_id
        // sort_property
        // sort_direction
        // max_items
        // label
        // icon
        // link_open

        $links = array();
        $error = null;

        $target = $settings['link_open'];

        // Menü mód
        if ($settings['taxonomy'] === 'menu')
        {
            if ($settings['wp_menu_id'] === -1)
                $error = 'Menü ID hiányzik!';
            else
            {
                $array_menu = wp_get_nav_menu_items($settings['wp_menu_id']);
                foreach ($array_menu as $index => $m) {
                    if ($settings['max_items'] != -1 && $index >= intval($settings['max_items']))
                        break;

                    if (empty($m->menu_item_parent)) {
                        $menu_link = $m->ID;
                        $menu_title = $m->title;
                        $links []= "<a href=\"$menu_link\" target=\"$target\">$menu_title</a>";
                    }
                }
            }
        }
        // Query mód
        else
        {
            $args = array(
                'taxonomy'     => $settings['taxonomy'],
                'orderby'      => $settings['sort_property'],
                'order'        => $settings['sort_direction'],
                'show_count'   => 0,
                'pad_counts'   => 0,
                'hierarchical' => 0,
                'title_li'     => '',
                'hide_empty'   => 0,
            );

            $all_categories = get_categories($args);
            foreach ($all_categories as $index => $cat) {
                if ($settings['max_items'] != -1 && $index >= intval($settings['max_items']))
                    break;

                $menu_title = $cat->name;
                $menu_link = get_term_link($cat->slug, 'product_cat');
                $links []= "<a href=\"$menu_link\" target=\"$target\">$menu_title</a>";
            }
        }

        if ($error === null)
        {
            ?>
            <div class="ryck-dropdown">
                <div class="ryck-drowdown-title">
                    <?php if (is_string($settings['icon']) && $settings['icon'] != ''): ?><i class="left-icon <?php echo $settings['icon']; ?>"></i><?php endif; ?>
                    <label class="title"><?php echo is_string($settings['label']) && $settings['label'] != '' ? $settings['label'] : 'Menü'; ?></label>
                    <div class="chevrons">
                        <i class="fas fa-chevron-up opened"></i>
                        <i class="fas fa-chevron-down closed"></i>
                    </div>
                </div>
                <div class="ryck-dropdown-items">
                    <div class="items-list">
                        <?php foreach($links as $link): echo $link; endforeach; ?>
                    </div>
                </div>
            </div>
            <?php
        }
        else
            echo "HIBA: $error";
    }
}

?>
