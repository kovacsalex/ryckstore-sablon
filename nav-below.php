<?php $args = array(
	'prev_text' => sprintf( __v('%s older'), '<span class="meta-nav">&larr;</span>'),
	'next_text' => sprintf( __v('newer %s'), '<span class="meta-nav">&rarr;</span>')
);
the_posts_navigation($args); ?>