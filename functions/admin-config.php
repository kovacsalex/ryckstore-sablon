<?php

    if (!class_exists('Redux')) { return; }




    $opt_name = "ryckstore_pref";

    $theme = wp_get_theme();

    $args = array(
        'opt_name'             => $opt_name,
        'display_name'         => $theme->get('Name'),
        'display_version'      => $theme->get('Version'),
        'menu_type'            => 'menu',
        'allow_sub_menu'       => true,

        // Címsor
        'menu_title'           => 'Sablon beállítások',
        'page_title'           => 'Sablon beállítások',

        'google_api_key'       => '',
        'google_update_weekly' => false,
        'async_typography'     => true,
        'admin_bar'            => true,

        // Ikon
        'admin_bar_icon'       => 'dashicons-admin-customizer',

        'admin_bar_priority'   => 50,
        'global_variable'      => '',

        // Fejlesztő mód
        'dev_mode'             => false,

        'update_notice'        => true,
        'customizer'           => true,
        'page_priority'        => null,
        'page_parent'          => 'themes.php',
        'page_permissions'     => 'manage_options',
        'menu_icon'            => 'dashicons-admin-customizer',
        'last_tab'             => '',
        'page_icon'            => 'icon-themes',
        'page_slug'            => '_options',
        'save_defaults'        => true,
        'default_show'         => false,
        'default_mark'         => '',
        'show_import_export'   => true,

        // ADVANCED CUCCOK
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        'output_tag'           => true,
        'database'             => '',
        'use_cdn'              => true,

        // TOOLTIP Beállítások
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKEK
    $args['admin_bar_links'][] = array(
        'id'    => 'ryck-contact-1',
        'href'  => 'mailto:kovacsalex95@gmail.com',
        'title' => 'Segítség',
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'ryck-about',
        'href'  => 'https://ryckposter.hu',
        'title' => 'A sablonról',
    );

    // SOCIAL IKONOK
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/ryckposter',
        'title' => 'Facebook oldalunk',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'https://ryckposter.hu',
        'title' => 'Weboldalunk',
        'icon'  => 'el el-globe'
    );

    // Beállítások feletti / alatti szövegek
    /*  $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
        $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' ); */

    Redux::setArgs( $opt_name, $args );

    // Help fülek
    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Help oldalsáv
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );



    // SZEKCIÓK ÉS MEZŐK


    // Általános
    Redux::setSection($opt_name, array(
        'title'         => 'Általános',
        'id'            => 'basic',
        'desc'          => 'Általános weboldal beállítások',
        'icon'          => 'el el-website'
    ));
    // Általános - Weboldal
    Redux::setSection($opt_name, array(
        'title'         => 'Weboldal',
        'id'            => 'basic-website',
        'desc'          => 'címsor szöveg, háttér, logó, szlogen stb.',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'          => 'font',
                'type'        => 'typography',
                'title'       => 'Fő betűtípus',
                'google'      => true,
                'font-backup' => true,
                'subsets'     => false,
                'text-align'  => false,
                'line-height' => false,
                'color'       => false,
                'output'      => array('body, .ffix > .normal'),
                'units'       => 'px',
                'default'     => array(
                    'font-style'  => '400',
                    'font-family' => 'Roboto',
                    'google'      => true,
                    'font-size'   => '16px'
                )
            ),
            array(
                'id'       => 'main-color',
                'type'     => 'color',
                'title'    => 'Betűszín',
                'default'  => '#333',
                'validate' => 'color',
                'transparent' => false
            ),
            array(
                'id'       => 'body-background',
                'type'     => 'background',
                'output'   => array( 'body' ),
                'title'    => 'Oldal háttere',
                'transparent' => false,
            ),
            array(
                'id'       => 'title-separator',
                'type'     => 'text',
                'title'    => 'Címsor elválasztó',
                'subtitle' => 'Aloldal <small>>></small> | <small><<</small> Weboldalam neve',
                'validate' => array('not_empty'),
                'default'  => ' | '
            ),
        )
    ));
    // Általános - Kapcsolat
    Redux::setSection($opt_name, array(
        'title'         => 'Kapcsolat',
        'id'            => 'basic-contact',
        'desc'          => 'Kapcsolati adatok beágyazása',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'basic-contact-shortcode',
                'type' => 'raw',
                'title' => 'Shortcode',
                'subtitle' => 'Példa kód kapcsolati adatok beágyazására',
                'full_width' => false,
                'content' => contact_shortcode_preview(),
            ),
            array(
                'id' => 'contact-phone',
                'type' => 'text',
                'title' => 'Telefonszám',
            ),
            array(
                'id' => 'contact-email',
                'type' => 'text',
                'title' => 'Email cím',
            ),
            array(
                'id' => 'contact-address',
                'type' => 'text',
                'title' => 'Cím',
            ),
			array(
				'id' => 'contact-address-link',
				'type' => 'text',
				'title' => 'Cím linkje',
				'subtitle' => 'Egyedi link URL-je',
			),
            array(
                'id' => 'contact-facebook',
                'type' => 'text',
                'title' => 'Facebook',
                'subtitle' => 'https://facebook.com/<small><strong>[OLDAL-NEVE]</strong></small>',
            ),
            array(
                'id' => 'contact-twitter',
                'type' => 'text',
                'title' => 'Twitter',
                'subtitle' => 'https://twitter.com/<small><strong>[OLDAL-NEVE]</strong></small>',
                'desc' => '<strong>@</strong> karakter nélkül',
            ),
            array(
                'id' => 'contact-instagram',
                'type' => 'text',
                'title' => 'Instagram',
                'subtitle' => 'https://instagram.com/<small><strong>[OLDAL-NEVE]</strong></small>',
                'desc' => '<strong>@</strong> karakter nélkül',
            ),
        )
    ));

    // Főelemek
    Redux::setSection($opt_name, array(
        'title'         => 'Főelemek',
        'id'            => 'mainelements',
        'desc'          => 'Fejléc, tartalom, és lábléc szegmensek beállításai',
        'icon'          => 'el el-th-large'
    ));
    // Főelemek - Fejléc
    Redux::setSection($opt_name, array(
        'title'         => 'Fejléc',
        'id'            => 'mainelements-header',
        'desc'          => 'Fejléc elrendezése és beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'header-select-start',
                'type' => 'section',
                'title' => 'Fejléc kiválasztása',
                'indent' => true
            ),
            array(
                'id'       => 'header-type',
                'type'     => 'button_set',
                'title'    => 'Típus',
                'options' => array(
                        'premade' => 'Előre elkészített',
                        'elementor' => 'Elementor sablon'
                     ),
                'default' => 'premade'
            ),
            array(
                'id'        => 'premade-header-type',
                'type'      => 'select',
                'title'     => 'Előre elkészített fejléc',
                'options'   => array(
                    'header-1'  => 'Fejléc 1',
                    'header-2'  => 'Fejléc 2',
                    'header-3'  => 'Fejléc 3'
                ),
                'default'   => 'header-1',
                'required' => array( 'header-type', '=', 'premade' )
            ),
            array(
                'id'        => 'elementor-header-id',
                'type'      => 'select',
                'title'     => 'Elementor sablon',
                'data'  => 'posts',
                'args'  => array(
                    'post_type'      => 'elementor_library',
                    'posts_per_page' => -1,
                    'orderby'        => 'title',
                    'order'          => 'ASC',
                ),
                'required' => array( 'header-type', '=', 'elementor' )
            ),
            array(
                'id' => 'header-select-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'header-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id'       => 'header-margin',
                'type'     => 'spacing',
                'title'    => 'Térköz',
                'subtitle' => 'a fejléc alatt és felett <small>(pixel)</small>',
                'output'   => array( '#header_container' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'left'     => false,
                'right'    => false,
                'mode'     => 'margin'
            ),
            array(
                'id'       => 'header-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container-fluid'
            ),
            array(
                'id' => 'header-layout-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'header-style-start',
                'type' => 'section',
                'title' => 'Stílus',
                'indent' => true
            ),
            array(
                'id' => 'header-background-color',
                'type' => 'color_rgba',
                'title' => 'Háttér',
                'output' => array( 'background-color' => '#header_container' ),
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'header-background',
                'type'     => 'background',
                'output'   => array( '#header_container' ),
                'background-color'    => false,
            ),
            array(
                'id'       => 'header-forecolor',
                'type'     => 'color',
                'title'    => 'Betűszín',
                'subtitle' => 'Csak azokra az elemekre vonatkozik amelyeknek nincs szín meghatározva!',
                'validate' => 'color',
                'transparent' => false
            ),
            array(
                'id' => 'header-style-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'header-searchbar-start',
                'type' => 'section',
                'title' => 'Keresőmező',
                'indent' => true
            ),
            array(
                'id' => 'header-searchbar-useajax',
                'type' => 'switch',
                'title' => 'Keresési javaslatok',
                'subtitle' => 'Ajax élő keresés',
                'default' => false,
            ),
            array(
                'id' => 'header-searchbar-showtaxonomy',
                'type' => 'button_set',
                'title' => 'Legördülő lista',
                'subtitle' => 'Kategóriák vagy címkék megjelenítéséhez',
                'options' => array(
                    'none' => 'Kikapcsolva',
                    'category' => 'Blog kategória',
                    'product_cat' => 'Termék kategória',
                    'product_tag' => 'Termék címke'
                ),
                'default' => 'product_tag',
            ),
            array(
                'id' => 'header-searchbar-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Főelemek - Sticky Fejléc
    Redux::setSection($opt_name, array(
        'title'         => 'Sticky Fejléc',
        'id'            => 'mainelements-stickyheader',
        'desc'          => 'Ragadós fejléc aktiválása, elrendezése és beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'       => 'sticky-header-enabled',
                'type'     => 'switch',
                'title'    => 'Engedélyezve',
                'default'  => false,
            ),
            array(
                'id' => 'sticky-header-select-start',
                'type' => 'section',
                'title' => 'Sticky fejléc kiválasztása',
                'indent' => true,
                'required' => array( 'sticky-header-enabled', '=', true)
            ),
            array(
                'id'       => 'sticky-header-type',
                'type'     => 'button_set',
                'title'    => 'Típus',
                'options' => array(
                        'premade' => 'Előre elkészített',
                        'elementor' => 'Elementor sablon'
                     ),
                'default' => 'premade',
            ),
            array(
                'id'        => 'premade-sticky-header-type',
                'type'      => 'select',
                'title'     => 'Előre elkészített fejléc',
                'options'   => array(
                    'sticky-header-1'  => 'Fejléc 1',
                    'sticky-header-2'  => 'Fejléc 2',
                    'sticky-header-3'  => 'Fejléc 3'
                ),
                'default'   => 'sticky-header-1',
                'required' => array('sticky-header-type', '=', 'premade'),
            ),
            array(
                'id'        => 'elementor-sticky-header-id',
                'type'      => 'select',
                'title'     => 'Elementor sablon',
                'data'  => 'posts',
                'args'  => array(
                    'post_type'      => 'elementor_library',
                    'posts_per_page' => -1,
                    'orderby'        => 'title',
                    'order'          => 'ASC',
                ),
                'required' => array('sticky-header-type', '=', 'elementor'),
            ),
            array(
                'id' => 'sticky-header-select-end',
                'type' => 'section',
                'indent' => false,
                'required' => array( 'sticky-header-enabled', '=', true)
            ),
            array(
                'id' => 'sticky-header-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true,
                'required' => array( 'sticky-header-enabled', '=', true)
            ),
            array(
                'id'       => 'sticky-header-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container-fluid',
            ),
            array(
                'id' => 'sticky-header-layout-end',
                'type' => 'section',
                'indent' => false,
                'required' => array( 'sticky-header-enabled', '=', true)
            ),
            array(
                'id' => 'sticky-header-style-start',
                'type' => 'section',
                'title' => 'Stílus',
                'indent' => true,
                'required' => array( 'sticky-header-enabled', '=', true)
            ),
            array(
                'id'       => 'sticky-header-animation-duration',
                'type'     => 'slider',
                'title'    => 'Animáció időtartama',
                'subtitle' => 'Előtűnés és eltűnés időtartama <small>(másodperc)</small>',
                "default"  => .5,
                "min"      => 0,
                "step"     => .1,
                "max"      => 1.5,
                'resolution' => 0.1,
                'display_value' => 'text',
            ),
            array(
                'id' => 'sticky-header-background-color',
                'type' => 'color_rgba',
                'title' => 'Háttér',
                'output' => array( 'background-color' => '#sticky_header_container' ),
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'sticky-header-background',
                'type'     => 'background',
                'output'   => array( '#sticky_header_container' ),
                'background-color'    => false,
            ),
            array(
                'id'       => 'sticky-header-forecolor',
                'type'     => 'color',
                'title'    => 'Betűszín',
                'subtitle' => 'Csak azokra az elemekre vonatkozik amelyeknek nincs szín meghatározva!',
                'validate' => 'color',
                'transparent' => false,
            ),
            array(
                'id' => 'sticky-header-style-end',
                'type' => 'section',
                'indent' => false,
                'required' => array( 'sticky-header-enabled', '=', true)
            ),
        )
    ));

    // Főelemek - Főmenü
    $mainmenu_fields = array(
        array(
            'id' => 'mainmenu-mode',
            'type' => 'button_set',
            'title' => 'Almenük módja',
            'options' => array(
                'basic' => 'Egyszerű',
                'complex' => 'Komplex',
            ),
            'default' => 'basic',
        ),

        array(
            'id' => 'mainmenu-complex-start',
            'type' => 'section',
            'title' => 'Komplex menü',
            'indent' => true,
            'required' => array('mainmenu-mode', '=', 'complex'),
        ),
        array(
            'id' => 'mainmenu-complex-itemselector',
            'type' => 'textarea',
            'validate' => 'text',
            'rows' => 3,
            'title' => 'Menüpontok kiválasztása',
            'subtitle' => 'CSS selector',
            'default' => 'nav.elementor-nav-menu--main ul.elementor-nav-menu li.menu-item',
        ),
    );

    $mainmenu_items = menu_by_location();
    foreach($mainmenu_items as $mm_item_index => $mm_item)
    {
        if (!$mm_item->menu_item_parent)
        {
            $mm_item_id = $mm_item->ID;
            $mm_item_name = $mm_item->title;
            $mainmenu_fields []= array(
                'id' => "mainmenu-complex-mitem-$mm_item_id",
                'type' => 'select',
                'title' => "\"$mm_item_name\" menüpont almenüje",
                'desc' => "A menüpont alatt megjelenő almenü",
                'data' => 'menus',
            );
            $mainmenu_fields []= array(
                'id' => "mainmenu-complex-mitem-$mm_item_id-columns",
                'type' => 'slider',
                'desc' => 'Oszlopok száma',
                'min' => 1,
                'max' => 10,
                'default' => 5,
                'required' => array("mainmenu-complex-mitem-$mm_item_id", '!=', null),
            );
        }
    }

    $mainmenu_fields []= array(
        'id' => 'mainmenu-complex-end',
        'type' => 'section',
        'indent' => false,
    );

    Redux::setSection($opt_name, array(
        'title'         => 'Főmenü',
        'id'            => 'mainelements-mainmenu',
        'desc'          => 'Főmenü módjai és beállításai',
        'subsection'    => true,
        'fields'        => $mainmenu_fields,
    ));
    // Főelemek - Tartalom
    Redux::setSection($opt_name, array(
        'title'         => 'Tartalom blokk',
        'id'            => 'mainelements-container',
        'desc'          => 'Tartalom blokk elrendezése és beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'wrapper-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true,
            ),
            array(
                'id'       => 'main-paddings',
                'type'     => 'spacing',
                'title'    => 'Belső térközök',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '#container' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'wrapper-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container'
            ),
            array(
                'id' => 'wrapper-layout-end',
                'type' => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'wrapper-style-start',
                'type' => 'section',
                'title' => 'Stílus',
                'indent' => true,
            ),
            array(
                'id' => 'wrapper-background-color',
                'type' => 'color_rgba',
                'title' => 'Háttér',
                'output' => array( 'background-color' => '#container' ),
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'wrapper-background',
                'type'     => 'background',
                'output'   => array( '#container' ),
                'default'  => array(
                    'background-color' => 'transparent'
                ),
                'background-color'    => false,
            ),
            array(
                'id' => 'wrapper-style-end',
                'type' => 'section',
                'indent' => false,
            ),
        )
    ));
    // Főelemek - Oldalsávok
    Redux::setSection($opt_name, array(
        'title'         => 'Oldalsávok',
        'id'            => 'mainelements-sidebars',
        'desc'          => 'Oldalsávok mérete és dizájnja',
        'subsection'    => true,
        'fields'        => array(

            array(
                'id'       => 'sidebar-main-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'sidebar-main-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'sidebar-main-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'sidebar-main-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'sidebar-main-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),

            array(
                'id' => 'sidebars-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'sidebar-left-width',
                'type' => 'slider',
                'title' => 'Bal oldalsáv',
                'desc' => '<strong>Szélesség</strong> (2 és 3 oszlopos elrendezés esetén<small> - a nagyobb szám a kevesebb oszlopra vonatkozik </small>)',
                'step' => 10,
                'min' => 100,
                'max' => 400,
                'handles' => 2,
                'default' => array(
                    1 => 200,
                    2 => 300,
                ),
            ),
            array(
                'id' => 'sidebar-left-gap',
                'type' => 'slider',
                'desc' => '<strong>Térköz</strong> (az oldalsáv és a tartalom blokk között<small> - az oldalsáv szélességéből vesszük el</small>)',
                'step' => 1,
                'min' => 0,
                'max' => 50,
                'default' => 30,
            ),
            array(
                'id' => 'sidebar-right-width',
                'type' => 'slider',
                'title' => 'Jobb oldalsáv',
                'desc' => '<strong>Szélesség</strong> (2 és 3 oszlopos elrendezés esetén<small> - a nagyobb szám a kevesebb oszlopra vonatkozik </small>)',
                'step' => 10,
                'min' => 100,
                'max' => 400,
                'handles' => 2,
                'default' => array(
                    1 => 200,
                    2 => 300,
                ),
            ),
            array(
                'id' => 'sidebar-right-gap',
                'type' => 'slider',
                'desc' => '<strong>Térköz</strong> (az oldalsáv és a tartalom blokk között<small> - az oldalsáv szélességéből vesszük el</small>)',
                'step' => 1,
                'min' => 0,
                'max' => 50,
                'default' => 30,
            ),
            array(
                'id' => 'sidebars-layout-end',
                'type' => 'section',
                'indent' => false
            ),

            array(
                'id' => 'sidebar-widget-title-start',
                'type' => 'section',
                'title' => 'Widget címsorok',
                'indent' => true
            ),
            array(
                'id' => 'sidebar-widget-title-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
            ),
            array(
                'id' => 'sidebar-widget-title-backcolor-type',
                'type' => 'button_set',
                'options' => array(
                    'inline-block' => 'Szöveg',
                    'block' => 'Teljes szélesség',
                ),
                'default' => 'block',
            ),
            array(
                'id'       => 'sidebar-widget-title-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'sidebar-widget-title-border',
                'type'     => 'border',
                'title' => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'sidebar-widget-title-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'sidebar-widget-title-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'sidebar-widget-title-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id' => 'sidebar-titles-end',
                'type' => 'section',
                'indent' => false
            ),

            array(
                'id' => 'sidebar-boxes-start',
                'type' => 'section',
                'title' => 'Widget dobozok',
                'indent' => true
            ),
            array(
                'id'       => 'sidebar-boxes-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'sidebar-boxes-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'sidebar-boxes-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'sidebar-boxes-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'sidebar-boxes-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'sidebar-boxes-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '30px' ),
            ),
            array(
                'id' => 'sidebar-boxes-end',
                'type' => 'section',
                'indent' => false
            ),

        )
    ));
    // Főelemek - Lábléc
    Redux::setSection($opt_name, array(
        'title'         => 'Lábléc',
        'id'            => 'mainelements-footer',
        'desc'          => 'Lábléc elrendezése és beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'footer-select-start',
                'type' => 'section',
                'title' => 'Lábléc kiválasztása',
                'indent' => true
            ),
            array(
                'id'       => 'footer-type',
                'type'     => 'button_set',
                'title'    => 'Típus',
                'options' => array(
                        'premade' => 'Előre elkészített',
                        'elementor' => 'Elementor sablon'
                     ),
                'default' => 'premade'
            ),
            array(
                'id'        => 'premade-footer-type',
                'type'      => 'select',
                'title'     => 'Előre elkészített lábléc',
                'options'   => array(
                    'footer-1'  => 'Lábléc 1',
                    'footer-2'  => 'Lábléc 2',
                    'footer-3'  => 'Lábléc 3'
                ),
                'default'   => 'footer-1',
                'required' => array( 'footer-type', '=', 'premade' )
            ),
            array(
                'id'        => 'elementor-footer-id',
                'type'      => 'select',
                'title'     => 'Elementor sablon',
                'data'  => 'posts',
                'args'  => array(
                    'post_type'      => 'elementor_library',
                    'posts_per_page' => -1,
                    'orderby'        => 'title',
                    'order'          => 'ASC',
                ),
                'required' => array( 'footer-type', '=', 'elementor' )
            ),
            array(
                'id' => 'footer-select-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'footer-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id'       => 'footer-margin',
                'type'     => 'spacing',
                'title'    => 'Térköz',
                'subtitle' => 'a lábléc alatt és felett <small>(pixel)</small>',
                'output'   => array( '#footer_container' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'left'     => false,
                'right'    => false,
                'mode'     => 'margin'
            ),
            array(
                'id'       => 'footer-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container-fluid'
            ),
            array(
                'id' => 'footer-layout-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'footer-style-start',
                'type' => 'section',
                'title' => 'Stílus',
                'indent' => true
            ),
            array(
                'id' => 'footer-background-color',
                'type' => 'color_rgba',
                'title' => 'Háttér',
                'output' => array( 'background-color' => '#footer_container' ),
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'footer-background',
                'type'     => 'background',
                'output'   => array( '#footer_container' ),
                'default'   => '#FFFFFF',
                'background-color'    => false,
            ),
            array(
                'id'       => 'footer-forecolor',
                'type'     => 'color',
                'title'    => 'Betűszín',
                'subtitle' => 'Csak azokra az elemekre vonatkozik amelyeknek nincs szín meghatározva!',
                'default'  => '#000',
                'validate' => 'color',
                'transparent' => false
            ),
            array(
                'id' => 'footer-style-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));

    // Tartalmi elemek
    Redux::setSection($opt_name, array(
        'title'         => 'Tartalmi elemek',
        'id'            => 'content',
        'desc'          => 'Tartalmi elemek beállításai',
        'icon'          => 'el el-th'
    ));
    // Tartalmi elemek - Terméklista
    Redux::setSection($opt_name, array(
        'title'         => 'Terméklista',
        'desc'          => 'Termékek elrendezése, betöltése, szűrése stb.',
        'id'            => 'content-webshoppage',
        'subsection'    => true,
        'fields'        => array(

            // Elrendezés
            array(
                'id' => 'webshop-page-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'webshop-page-xl-col',
                'type' => 'button_set',
                'title' => 'HD Asztali oszlopok száma',
                'subtitle' => '<small><strong>992px</strong> feletti oldalszélesség</small>',
                'options' => array(
                    'col-lg-12' => '1 oszlop',
                    'col-lg-6' => '2 oszlop',
                    'col-lg-4' => '3 oszlop',
                    'col-lg-3' => '4 oszlop',
                    'col-lg-2' => '6 oszlop',
                    'col-lg-1' => '12 oszlop',
                ),
                'default' => 'col-lg-4',
            ),
            array(
                'id' => 'webshop-page-lg-col',
                'type' => 'button_set',
                'title' => 'Asztali oszlopok száma',
                'subtitle' => '<small><strong>992px - 768px</strong> közötti oldalszélesség</small>',
                'options' => array(
                    'col-md-12' => '1 oszlop',
                    'col-md-6' => '2 oszlop',
                    'col-md-4' => '3 oszlop',
                    'col-md-3' => '4 oszlop',
                    'col-md-2' => '6 oszlop',
                    'col-md-1' => '12 oszlop',
                ),
                'default' => 'col-md-4',
            ),
            array(
                'id' => 'webshop-page-md-col',
                'type' => 'button_set',
                'title' => 'Tablet oszlopok száma',
                'subtitle' => '<small><strong>768px - 576px</strong> közötti oldalszélesség</small>',
                'options' => array(
                    'col-sm-12' => '1 oszlop',
                    'col-sm-6' => '2 oszlop',
                    'col-sm-4' => '3 oszlop',
                    'col-sm-3' => '4 oszlop',
                    'col-sm-2' => '6 oszlop',
                    'col-sm-1' => '12 oszlop',
                ),
                'default' => 'col-sm-6',
            ),
            array(
                'id' => 'webshop-page-sm-col',
                'type' => 'button_set',
                'title' => 'Mobil oszlopok száma',
                'subtitle' => '<small><strong>576px</strong> alatti oldalszélesség</small>',
                'options' => array(
                    'col-12' => '1 oszlop',
                    'col-6' => '2 oszlop',
                    'col-4' => '3 oszlop',
                    'col-3' => '4 oszlop',
                    'col-2' => '6 oszlop',
                    'col-1' => '12 oszlop',
                ),
                'default' => 'col-12',
            ),
            array(
                'id' => 'webshop-page-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Terméklista oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'webshop-page-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('webshop-page-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'webshop-page-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('webshop-page-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'webshop-page-layout-end',
                'type' => 'section',
                'indent' => false
            ),

            // Termékek betöltése
            array(
                'id' => 'webshop-page-loading-start',
                'type' => 'section',
                'title' => 'Termékek betöltése',
                'indent' => true
            ),
            array( // TODO
                'id' => 'webshop-page-loading-ajax',
                'type' => 'switch',
                'title' => 'Ajax termékbetöltés',
                'subtitle' => 'Új termékek betöltése a lap alján',
                'default' => false,
            ),
            array(
                'id' => 'webshop-page-loading-end',
                'type' => 'section',
                'indent' => false
            ),

            // Címsor
            array(
                'id' => 'webshop-title-start',
                'type' => 'section',
                'title' => 'Címsor',
                'subtitle' => 'Kategória vagy Címke neve',
                'indent' => true
            ),
            array(
                'id' => 'webshop-title-mode',
                'type' => 'button_set',
                'title' => 'Elhelyezés',
                'options' => array(
                    'none' => 'Kikapcsolva',
                    'normal' => 'Normál',
                    'jumbo' => 'Nagy',
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'webshop-title-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container-fluid',
                'required' => array('webshop-title-mode', '=', 'jumbo'),
            ),
            array(
                'id' => 'webshop-title-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-size'   => '20px',
                    'line-height' => '22px',
                ),
                'required' => array('webshop-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'webshop-title-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'required' => array('webshop-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'webshop-title-background',
                'type'     => 'background',
                'background-color'    => false,
                'required' => array('webshop-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'webshop-title-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'required' => array('webshop-title-mode', '!=', 'none'),
            ),
            array(
                'id' => 'webshop-title-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Tartalmi elemek - Termékoldal
    Redux::setSection($opt_name, array(
        'title'         => 'Termékoldal',
        'id'            => 'content-webshopproduct-page',
        'subsection'    => true,
        'fields'        => array(

            array(
                'id' => 'product-page-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'product-page-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Terméklista oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'product-page-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('product-page-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'product-page-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('product-page-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'product-page-layout-end',
                'type' => 'section',
                'indent' => false
            ),

            array(
                'id' => 'product-page-cart-start',
                'type' => 'section',
                'title' => 'Kosárba helyezés',
                'indent' => true
            ),
            array(
                'id' => 'product-page-cart-qtybtns',
                'type' => 'switch',
                'title' => 'Plusz Minusz gombok',
                'subtitle' => 'A mennyiség mező két oldalán jelennek meg',
                'default' => true,
            ),
            array(
                'id' => 'product-page-cart-qty-btn-gap',
                'type' => 'slider',
                'title' => 'Mennyiség és Gomb távolsága',
                'max' => 200,
                'step' => 1,
                'default' => 10,
            ),
            array(
                'id' => 'product-page-cart-qty-btn-align',
                'type' => 'button_set',
                'title' => 'Elemek igazítása',
                'options' => array(
                    'left' => 'Balra',
                    'center' => 'Középre',
                    'right' => 'Jobbra',
                ),
                'default' => 'left',
            ),
            array(
                'id' => 'product-page-cart-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Tartalmi elemek - Termékdoboz
    Redux::setSection($opt_name, array(
        'title'         => 'Termékdoboz',
        'id'            => 'content-webshopproduct',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'       => 'webshop-item-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'webshop-item-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'webshop-item-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'webshop-item-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'webshop-item-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'webshop-item-margin',
                'type'     => 'spacing',
                'mode'     => 'margin',
                'title'    => 'Termékek közötti térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'top' => false,
                'left' => false,
                'right' => false,
                'default' => array( 'margin-bottom' => '30px' ),
            ),
            array(
                'id'       => 'webshop-item-rows',
                'type'     => 'sortable',
                'title'    => 'Termék doboz sorai',
                'subtitle' => 'Itt rendezhetjük és rejthetjük el a sorokat',
                'mode'     => 'checkbox',
                'options' => array(
                    'photos' => 'Termékfotók',
                    'title-price' => 'Név és Ár',
                    'buttons' => 'Gombok',
                    'details' => 'Egyéb adatok',
                    'description' => 'Rövid leírás',
                ),
                'default' => array(
                    'photos' => true,
                    'title-price' => true,
                    'buttons' => true,
                    'details' => false,
                    'description' => false,
                ),
            ),

            // Termék Neve és Ára
            array(
                'id' => 'webshop-item-title-price-start',
                'type' => 'section',
                'title' => 'Név és Ár sora',
                'indent' => true
            ),
            array(
                'id' => 'webshop-item-title-price-mode',
                'type' => 'button_set',
                'title' => 'Megjelenítés',
                'options' => array(
                    'block' => 'Egymás alatt',
                    'inline-block' => 'Egymás mellett',
                ),
                'default' => 'inline-block',
            ),
            array(
                'id' => 'webshop-item-title-price-vertical',
                'type' => 'button_set',
                'title' => 'Függőleges igazítás',
                'options' => array(
                    'baseline' => 'Alapvonal',
                    'top' => 'Fent',
                    'middle' => 'Középen',
                    'bottom' => 'Lent',
                ),
                'default' => 'baseline',
                'required' => array('webshop-item-title-price-mode', '=', 'inline-block'),
            ),
            array(
                'id' => 'webshop-item-title-price-width-ratio',
                'type' => 'slider',
                'title' => 'Név szélessége',
                'subtitle' => '<small>pl:</small> <strong>75%</strong> név szélesség - <strong>25%</strong> ár szélesség',
                'default' => 70,
                'min' => 10,
                'max' => 90,
                'step' => 5,
                'display_value' => 'text',
                'required' => array('webshop-item-title-price-mode', '=', 'inline-block'),
            ),
            array(
                'id'       => 'webshop-item-title-price-distance-vertical',
                'type'     => 'dimensions',
                'title'    => 'Függőleges térköz',
                'subtitle' => '<small>(pixel)</small>',
                'width'    => false,
                'required' => array('webshop-item-title-price-mode', '=', 'block'),
            ),
            array(
                'id'       => 'webshop-item-title-price-distance-horizontal',
                'type'     => 'dimensions',
                'title'    => 'Vízszintes térköz',
                'subtitle' => '<small>(pixel)</small>',
                'height'    => false,
                'required' => array('webshop-item-title-price-mode', '=', 'inline-block'),
            ),
            array(
                'id' => 'webshop-item-title-price-padding',
                'type' => 'spacing',
                'mode' => 'padding',
                'units' => array('px'),
                'units_extended' => false,
                'title' => 'Belső térközök',
                'subtitle' => '<small>(pixel)</small>',
            ),
            array(
                'id' => 'webshop-item-title-price-end',
                'type' => 'section',
                'indent' => false
            ),

            // Termék neve
            array(
                'id' => 'webshop-item-title-start',
                'type' => 'section',
                'title' => 'Név',
                'indent' => true
            ),
            array(
                'id' => 'webshop-item-title-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
            ),
            array(
                'id' => 'webshop-item-title-backcolor-type',
                'type' => 'button_set',
                'options' => array(
                    'label' => 'Szöveg',
                    'box' => 'Teljes doboz',
                ),
                'default' => 'box',
            ),
            array(
                'id'       => 'webshop-item-title-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'webshop-item-title-border',
                'type'     => 'border',
                'title' => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'webshop-item-title-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'webshop-item-title-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id' => 'webshop-item-title-end',
                'type' => 'section',
                'indent' => false
            ),

            // Termék ára
            array(
                'id' => 'webshop-item-price-start',
                'type' => 'section',
                'title' => 'Ár',
                'indent' => true
            ),
            array(
                'id' => 'webshop-item-price-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default' => array(
                    'text-align' => 'right',
                ),
            ),
            array(
                'id' => 'webshop-item-price-backcolor-type',
                'type' => 'button_set',
                'options' => array(
                    'label' => 'Szöveg',
                    'box' => 'Teljes doboz',
                ),
                'default' => 'box',
            ),
            array(
                'id'       => 'webshop-item-price-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'webshop-item-price-border',
                'type'     => 'border',
                'title' => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'webshop-item-price-border-radius',
                'type' => 'slider',
                'desc' => '<small>Lekerekítés (pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'webshop-item-price-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id' => 'webshop-item-price-end',
                'type' => 'section',
                'indent' => false
            ),

            // Termékfotók
            array(
                'id' => 'webshop-item-images-start',
                'type' => 'section',
                'title' => 'Fotók',
                'indent' => true
            ),
            array(
                'id'       => 'webshop-item-images-mode',
                'type'     => 'button_set',
                'title'    => 'Megjelenítési mód',
                'options' => array(
                    'single' => 'Egyetlen kép',
                    'gallery' => 'Képgaléria',
                 ),
                'default' => 'single'
            ),
            array(
                'id' => 'webshop-item-images-height',
                'type' => 'slider',
                'title' => 'Képek magassága',
                'subtitle' => 'A szélességhez viszonyítva <small>(%)</small>',
                "default" => 75,
                "min" => 50,
                "step" => 5,
                "max" => 200,
                'display_value' => 'text'
            ),
            array(
                'id' => 'webshop-item-images-size',
                'type' => 'slider',
                'title' => 'Képek mérete',
                'subtitle' => 'A nagyobb oldal mérete <small>(pixel)</small>',
                "default" => 300,
                "min" => 100,
                "step" => 5,
                "max" => 1200,
                'display_value' => 'text'
            ),
            array(
                'id' => 'webshop-item-images-padding',
                'type' => 'spacing',
                'mode' => 'padding',
                'units' => array('px'),
                'units_extended' => false,
                'title' => 'Belső térközök',
                'subtitle' => '<small>(pixel)</small>',
            ),
            array(
                'id'       => 'webshop-item-images-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'webshop-item-images-border',
                'type'     => 'border',
                'title' => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'webshop-item-images-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'   => 'webshop-item-images-gallery-info',
                'type' => 'info',
                'title' => 'Hamarosan...',
                'style' => 'warning',
                'desc' => 'A galéria mód fejlesztés alatt álló funckió',
                'required' => array('webshop-item-images-mode', '=', 'gallery'),
            ),
            array(
                'id' => 'webshop-item-images-end',
                'type' => 'section',
                'indent' => false
            ),

            // Gombok
            array(
                'id' => 'webshop-item-buttons-start',
                'type' => 'section',
                'title' => 'Gombok',
                'indent' => true
            ),
            array(
                'id' => 'webshop-item-buttons-ajax',
                'type' => 'switch',
                'title' => 'Ajax gombok',
                'subtitle' => 'Oldalbetöltés nélküli kosárba helyezés',
                'default' => false,
            ),
            array(
                'id'       => 'webshop-item-cart-button-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Kosár gomb',
                'desc'     => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id' => 'webshop-item-cart-button-textcolor',
                'type' => 'color',
                'desc' => 'Betűszín',
                'transparent' => false,
            ),
            array(
                'id' => 'webshop-item-cart-button-text',
                'type' => 'text',
                'desc' => 'Szöveg',
                'default' => 'Kosárba',
            ),
            array(
                'id' => 'webshop-item-cart-button-icon',
                'type' => 'text',
                'desc' => 'Ikon',
                'default' => 'cart-plus',
            ),
            array(
                'id' => 'webshop-item-favorite-button-shortcode',
                'type' => 'text',
                'title' => 'Kedvencek gomb',
                'subtitle' => '<small>(Shortcode)</small>',
            ),
            array(
                'id' => 'webshop-item-buttons-padding',
                'type' => 'spacing',
                'mode' => 'padding',
                'units' => array('px'),
                'units_extended' => false,
                'title' => 'Belső térközök',
                'subtitle' => '<small>(pixel)</small>',
            ),
            array(
                'id' => 'webshop-item-buttons-align',
                'type' => 'button_set',
                'title' => 'Gombok igazítása',
                'options' => array(
                    'left' => 'Bal',
                    'center' => 'Közép',
                    'right' => 'Jobb',
                ),
                'default' => 'center',
            ),
            array(
                'id' => 'webshop-item-buttons-end',
                'type' => 'section',
                'indent' => false
            ),

            // Egyéb adatok
            array(
                'id' => 'webshop-item-details-start',
                'type' => 'section',
                'title' => 'Egyéb adatok',
                'indent' => true
            ),
            array(
                'id'   => 'webshop-item-details-info',
                'type' => 'info',
                'title' => 'Hamarosan...',
                'style' => 'warning',
                'desc' => 'Fejlesztés alatt álló funckió',
            ),
            array(
                'id' => 'webshop-item-details-end',
                'type' => 'section',
                'indent' => false
            ),

            // Rövid leírás
            array(
                'id' => 'webshop-item-description-start',
                'type' => 'section',
                'title' => 'Rövid leírás',
                'indent' => true
            ),
            array(
                'id'   => 'webshop-item-description-info',
                'type' => 'info',
                'title' => 'Hamarosan...',
                'style' => 'warning',
                'desc' => 'Fejlesztés alatt álló funckió',
            ),
            array(
                'id' => 'webshop-item-description-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Tartalmi elemek - Kosár oldal
    Redux::setSection($opt_name, array(
        'title'         => 'Kosár és Fizetés',
        'id'            => 'content-cart-checkout-page',
        'subsection'    => true,
        'fields'        => array(

            array(
                'id' => 'cart-page-layout-start',
                'type' => 'section',
                'title' => 'Kosár elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'cart-page-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Terméklista oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'cart-page-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('cart-page-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'cart-page-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('cart-page-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'cart-page-layout-end',
                'type' => 'section',
                'indent' => false
            ),

            array(
                'id' => 'checkout-page-layout-start',
                'type' => 'section',
                'title' => 'Fizetés elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'checkout-page-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Terméklista oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'checkout-page-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('checkout-page-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'checkout-page-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('checkout-page-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'checkout-page-layout-end',
                'type' => 'section',
                'indent' => false
            ),

            // Címsor
            array(
                'id' => 'checkout-title-start',
                'type' => 'section',
                'title' => 'Címsor',
                'subtitle' => 'Kategória vagy Címke neve',
                'indent' => true
            ),
            array(
                'id' => 'checkout-title-mode',
                'type' => 'button_set',
                'title' => 'Elhelyezés',
                'options' => array(
                    'none' => 'Kikapcsolva',
                    'normal' => 'Normál',
                    'jumbo' => 'Nagy',
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'checkout-title-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container-fluid',
                'required' => array('checkout-title-mode', '=', 'jumbo'),
            ),
            array(
                'id' => 'checkout-title-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-size'   => '20px',
                    'line-height' => '22px',
                ),
                'required' => array('checkout-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'checkout-title-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'required' => array('checkout-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'checkout-title-background',
                'type'     => 'background',
                'background-color'    => false,
                'required' => array('checkout-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'checkout-title-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'required' => array('checkout-title-mode', '!=', 'none'),
            ),
            array(
                'id' => 'checkout-title-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Tartalmi elemek - Blog oldal
    Redux::setSection($opt_name, array(
        'title'         => 'Blog oldal',
        'id'            => 'content-blogpage',
        'desc'          => 'Blog oldal beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'blogpost-archive-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'blogpost-archive-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Blog archívum oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'blogpost-archive-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('blogpost-archive-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'blogpost-archive-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('blogpost-archive-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'blogpost-archive-layout-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Tartalmi elemek - Blog bejegyzések
    Redux::setSection($opt_name, array(
        'title'         => 'Blog bejegyzések',
        'id'            => 'content-blogposts',
        'desc'          => 'Blog bejegyzések beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'blogpost-single-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'blogpost-single-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Blogbejegyzés oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'blogpost-single-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('blogpost-single-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'blogpost-single-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('blogpost-single-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'blogpost-single-layout-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Tartalmi elemek - Egyszerű oldal
    Redux::setSection($opt_name, array(
        'title'         => 'Egyszerű oldal',
        'id'            => 'content-simple-page',
        'subsection'    => true,
        'fields'        => array(

            array(
                'id' => 'simple-page-layout-start',
                'type' => 'section',
                'title' => 'Elrendezés',
                'indent' => true
            ),
            array(
                'id' => 'simple-page-sidebar-mode',
                'type' => 'image_select',
                'title' => 'Oldalsávok',
                'subtitle' => 'Terméklista oldalsávok helye és száma',
                'options'  => array(
                    'none'      => array(
                        'alt'   => 'Nincs oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/1c.png'
                    ),
                    'left'      => array(
                        'alt'   => 'Bal oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cl.png'
                    ),
                    'right'      => array(
                        'alt'   => 'Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/2cr.png'
                    ),
                    'left-right'      => array(
                        'alt'   => 'Bal és Jobb oldalsáv',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'simple-page-sidebar-left',
                'type'     => 'button_set',
                'title'    => 'Bal oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-1',
                'required' => array('simple-page-sidebar-mode', 'contains', 'left'),
            ),
            array(
                'id'       => 'simple-page-sidebar-right',
                'type'     => 'button_set',
                'title'    => 'Jobb oldalsáv',
                'options'     => array(
                    'sidebar-widgets-1' => 'Oldalsáv 1',
                    'sidebar-widgets-2' => 'Oldalsáv 2',
                    'sidebar-widgets-3' => 'Oldalsáv 3',
                    'sidebar-widgets-4' => 'Oldalsáv 4',
                    'sidebar-widgets-5' => 'Oldalsáv 5',
                    'sidebar-widgets-6' => 'Oldalsáv 6',
                ),
                'default' => 'sidebar-widgets-2',
                'required' => array('simple-page-sidebar-mode', 'contains', 'right'),
            ),
            array(
                'id' => 'simple-page-layout-end',
                'type' => 'section',
                'indent' => false
            ),



            // Címsor
            array(
                'id' => 'pages-title-start',
                'type' => 'section',
                'title' => 'Címsor',
                'subtitle' => 'Kategória vagy Címke neve',
                'indent' => true
            ),
            array(
                'id' => 'pages-title-mode',
                'type' => 'button_set',
                'title' => 'Elhelyezés',
                'options' => array(
                    'none' => 'Kikapcsolva',
                    'normal' => 'Normál',
                    'jumbo' => 'Nagy',
                ),
                'default' => 'none',
            ),
            array(
                'id'       => 'pages-title-width-class',
                'type'     => 'image_select',
                'title'    => 'Szélesség',
                'subtitle' => 'Teljes vagy középre zárt elrendezés',
                'options'  => array(
                    'container-fluid'       => array(
                        'alt'   => 'Teljes szélességű',
                        'img'   => ReduxFramework::$_url.'assets/img/1col.png'
                    ),
                    'container' => array(
                        'alt'   => 'Középre zárt',
                        'img'   => ReduxFramework::$_url.'assets/img/3cm.png'
                    ),
                ),
                'default' => 'container-fluid',
                'required' => array('pages-title-mode', '=', 'jumbo'),
            ),
            array(
                'id' => 'pages-title-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-size'   => '20px',
                    'line-height' => '22px',
                ),
                'required' => array('pages-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'pages-title-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'required' => array('pages-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'pages-title-background',
                'type'     => 'background',
                'background-color'    => false,
                'required' => array('pages-title-mode', '!=', 'none'),
            ),
            array(
                'id'       => 'pages-title-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'required' => array('pages-title-mode', '!=', 'none'),
            ),
            array(
                'id' => 'pages-title-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));

    // Szöveg elemek
    Redux::setSection($opt_name, array(
        'title'         => 'Szöveg elemek',
        'id'            => 'text',
        'desc'          => 'Szöveg elemek beállításai',
        'icon'          => 'el el-align-justify'
    ));
    // Szöveg elemek - Címsorok
    Redux::setSection($opt_name, array(
        'title'         => 'Címsorok',
        'id'            => 'text-titles',
        'desc'          => 'Címsorok beállításai',
        'subsection'    => true,
        'fields'        => array(

            // Oldal címe
            array(
                'id' => 'text-title-start',
                'type' => 'section',
                'title' => 'Oldalcím',
                'subtitle' => 'Az oldalak tetején megjelenő címsor',
                'indent' => true
            ),
            array(
                'id'       => 'text-title-enabled',
                'type'     => 'switch',
                'title'    => 'Bekapcsolva',
                'default'  => true,
            ),
            array(
                'id'       => 'text-title-display',
                'type'     => 'button_set',
                'title'    => 'Szélesség',
                'options' => array(
                        'inline-block' => 'Automatikus',
                        'block' => 'Teljes szélességű'
                     ),
                'default' => 'inline-block',
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id' => 'text-title-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-size'   => '26px',
                    'line-height' => '28px',
                ),
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id'       => 'text-title-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id'       => 'text-title-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id' => 'text-title-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id'       => 'text-title-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'label.entry-title' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id'       => 'text-title-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'label.entry-title' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'required' => array('text-title-enabled', '=', true),
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id'       => 'text-title-underline',
                'type'     => 'switch',
                'title'    => 'Aláhúzott',
                'default'  => false,
                'required' => array('text-title-enabled', '=', true),
            ),
            array(
                'id'     => 'text-title-end',
                'type'   => 'section',
                'indent' => false,
            ),

            // Címsor 1
            array(
                'id' => 'text-h1-start',
                'type' => 'section',
                'title' => 'Címsor 1',
                'indent' => true
            ),
            array(
                'id'       => 'text-h1-display',
                'type'     => 'button_set',
                'title'    => 'Szélesség',
                'options' => array(
                        'inline-block' => 'Automatikus',
                        'block' => 'Teljes szélességű'
                     ),
                'default' => 'block'
            ),
            array(
                'id' => 'text-h1-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-style'  => '700',
                    'font-size'   => '40px',
                    'line-height' => '42px',
                )
            ),
            array(
                'id'       => 'text-h1-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'text-h1-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                )
            ),
            array(
                'id' => 'text-h1-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'text-h1-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h1' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'text-h1-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h1' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id'       => 'text-h1-underline',
                'type'     => 'switch',
                'title'    => 'Aláhúzott',
                'default'  => false
            ),
            array(
                'id'     => 'text-h1-end',
                'type'   => 'section',
                'indent' => false,
            ),

            // Címsor 2
            array(
                'id' => 'text-h2-start',
                'type' => 'section',
                'title' => 'Címsor 2',
                'indent' => true
            ),
            array(
                'id'       => 'text-h2-display',
                'type'     => 'button_set',
                'title'    => 'Szélesség',
                'options' => array(
                        'inline-block' => 'Automatikus',
                        'block' => 'Teljes szélességű'
                     ),
                'default' => 'block'
            ),
            array(
                'id' => 'text-h2-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-style'  => '700',
                    'font-size'   => '32px',
                    'line-height' => '34px',
                )
            ),
            array(
                'id'       => 'text-h2-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'text-h2-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                )
            ),
            array(
                'id' => 'text-h2-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'text-h2-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h2' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'text-h2-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h2' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id'       => 'text-h2-underline',
                'type'     => 'switch',
                'title'    => 'Aláhúzott',
                'default'  => false
            ),
            array(
                'id'     => 'text-h2-end',
                'type'   => 'section',
                'indent' => false,
            ),

            // Címsor 3
            array(
                'id' => 'text-h3-start',
                'type' => 'section',
                'title' => 'Címsor 3',
                'indent' => true
            ),
            array(
                'id'       => 'text-h3-display',
                'type'     => 'button_set',
                'title'    => 'Szélesség',
                'options' => array(
                        'inline-block' => 'Automatikus',
                        'block' => 'Teljes szélességű'
                     ),
                'default' => 'block'
            ),
            array(
                'id' => 'text-h3-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-size'   => '26px',
                    'line-height' => '28px',
                )
            ),
            array(
                'id'       => 'text-h3-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'text-h3-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'font-style'  => '700',
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                )
            ),
            array(
                'id' => 'text-h3-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'text-h3-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h3' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'text-h3-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h3' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id'       => 'text-h3-underline',
                'type'     => 'switch',
                'title'    => 'Aláhúzott',
                'default'  => false
            ),
            array(
                'id'     => 'text-h3-end',
                'type'   => 'section',
                'indent' => false,
            ),

            // Címsor 4
            array(
                'id' => 'text-h4-start',
                'type' => 'section',
                'title' => 'Címsor 4',
                'indent' => true
            ),
            array(
                'id'       => 'text-h4-display',
                'type'     => 'button_set',
                'title'    => 'Szélesség',
                'options' => array(
                        'inline-block' => 'Automatikus',
                        'block' => 'Teljes szélességű'
                     ),
                'default' => 'block'
            ),
            array(
                'id' => 'text-h4-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Igazítás, betű és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => true,
                'subsets'   => false,
                'default'     => array(
                    'font-style'  => '700',
                    'font-size'   => '20px',
                    'line-height' => '22px',
                )
            ),
            array(
                'id'       => 'text-h4-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttérszín',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'text-h4-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                )
            ),
            array(
                'id' => 'text-h4-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'text-h4-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h4, h5, h6' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'text-h4-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( 'h4, h5, h6' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id'       => 'text-h4-underline',
                'type'     => 'switch',
                'title'    => 'Aláhúzott',
                'default'  => false
            ),
            array(
                'id'     => 'text-h4-end',
                'type'   => 'section',
                'indent' => false,
            ),
        )
    ));
    // Szöveg elemek - Bekezdések
    Redux::setSection($opt_name, array(
        'title'         => 'Bekezdések',
        'id'            => 'text-paragraphs',
        'desc'          => 'Bekezdések beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'paragraph-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'betű, sor és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'subsets'   => false,
            ),
            array(
                'id'       => 'paragraph-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'paragraph-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'paragraph-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'paragraph-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'paragraph-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content > p' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'paragraph-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content > p' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
        )
    ));
    // Szöveg elemek - Idézetek
    Redux::setSection($opt_name, array(
        'title'         => 'Idézetek',
        'id'            => 'text-quotes',
        'desc'          => 'Idézetek beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'blockquote-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'betű, sor és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'subsets'   => false,
            ),
            array(
                'id' => 'blockquote-italic',
                'type' => 'switch',
                'desc' => 'Dőlt betűk',
                'default' => false,
            ),
            array(
                'id'       => 'blockquote-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'blockquote-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'blockquote-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'blockquote-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'blockquote-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content > blockquote, .entry-content p > blockquote' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'blockquote-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content > blockquote, .entry-content p > blockquote' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
        )
    ));
    // Szöveg elemek - Linkek
    Redux::setSection($opt_name, array(
        'title'         => 'Linkek',
        'id'            => 'text-links',
        'desc'          => 'Linkek beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'       => 'color-links',
                'type'     => 'link_color',
                'title'    => 'Linkek',
                'subtitle' => 'Alap, egér alatti, látogatott és kattintott szín',
                //'regular'   => false, // Disable Regular Color
                //'hover'     => false, // Disable Hover Color
                //'active'    => false, // Disable Active Color
                'visited'   => true,  // Enable Visited Color
                'default'  => array(
                    'regular' => '#0073aa',
                    'hover'   => '#6bb2d4',
                    'active'  => '#295e77',
					'visited' => '#9800aa'
                )
            ),
        )
    ));
    // Szöveg elemek - Gombok
    Redux::setSection($opt_name, array(
        'title'         => 'Gombok',
        'id'            => 'text-buttons',
        'desc'          => 'Gombok beállításai',
        'subsection'    => true,
        'fields'        => array(

            // Általános beállítások
            array(
                'id' => 'text-buttons-basic-start',
                'type' => 'section',
                'title' => 'Általános beállítások',
                'indent' => true
            ),
            array(
                'id' => 'text-buttons-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'Betűk stílusa',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => false,
                'word-spacing' => false,
                'letter-spacing' => false,
                'color' => false,
                'text-align' => false,
                'subsets'   => false,
            ),
            array(
                'id'       => 'text-buttons-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'color'    => false,
            ),
            array(
                'id' => 'text-buttons-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
                'default' => 6,
            ),
            array(
                'id'       => 'text-buttons-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'default' => array(
                    'padding-left' => '20px',
                    'padding-right' => '20px',
                    'padding-top' => '10px',
                    'padding-bottom' => '10px',
                ),
            ),
            array(
                'id' => 'text-buttons-icon-type',
                'type' => 'image_select',
                'title' => 'Ikon pozíciója',
                'subtitle' => 'Azokra a gombokra vonatkozik amiknek van ikonja',
                'options'  => array(
                    'left-1'      => array(
                        'alt'   => 'Ikon a bal oldalon a gomb szélén',
                        'img'   => ReduxFramework::$_url.'assets/img/alig_left_1.png'
                    ),
                    'left-2'      => array(
                        'alt'   => 'Ikon a bal oldalon a szöveg mellett',
                        'img'   => ReduxFramework::$_url.'assets/img/alig_left_2.png'
                    ),
                    'right-2'      => array(
                        'alt'   => 'Ikon a jobb oldalon a szöveg mellett',
                        'img'   => ReduxFramework::$_url.'assets/img/alig_right_2.png'
                    ),
                    'right-1'      => array(
                        'alt'   => 'Ikon a jobb oldalon a gomb szélén',
                        'img'   => ReduxFramework::$_url.'assets/img/alig_right_1.png'
                    ),
                ),
                'default' => 'right-2',
            ),
            array(
                'id' => 'text-buttons-icon-gap-1',
                'type' => 'slider',
                'title' => 'Ikon távolsága',
                'subtitle' => 'Távolság az <strong>ikon</strong> és a <strong>gomb</strong> között',
                'max' => 64,
                'step' => 1,
                'default' => 10,
                'required' => array('text-buttons-icon-type', 'contains', '1'),
            ),
            array(
                'id' => 'text-buttons-icon-gap-2',
                'type' => 'slider',
                'title' => 'Ikon távolsága',
                'subtitle' => 'Távolság az <strong>ikon</strong> és a <strong>gomb szövege</strong> között',
                'max' => 64,
                'step' => 1,
                'default' => 10,
                'required' => array('text-buttons-icon-type', 'contains', '2'),
            ),
            array(
                'id' => 'text-buttons-basic-end',
                'type' => 'section',
                'indent' => false
            ),

            // Normál állapot
            array(
                'id' => 'text-buttons-normal-start',
                'type' => 'section',
                'title' => 'Normál állapot',
                'subtitle' => 'A gomb érintetlen',
                'indent' => true
            ),
            array(
                'id'       => 'text-buttons-normal-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'default' => array(
                    'color' => '#007cba',
                    'alpha' => 1,
                ),
            ),
            array(
                'id'       => 'text-buttons-normal-background',
                'type'     => 'background',
                'background-color'    => false,
                'background-attachment' => false,
            ),
            array(
                'id'       => 'text-buttons-normal-bordercolor',
                'type'     => 'color_rgba',
                'title'    => 'Szegély színe',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'default' => array(
                    'color' => '#007cba',
                    'alpha' => 1,
                ),
            ),
            array(
                'id'       => 'text-buttons-normal-textcolor',
                'type'     => 'color',
                'title'    => 'Szöveg színe',
                'default' => '#ffffff',
                'transparent' => false,
            ),
            array(
                'id'       => 'text-buttons-normal-iconcolor',
                'type'     => 'color',
                'title'    => 'Ikon színe',
                'transparent' => false,
            ),
            array(
                'id' => 'text-buttons-normal-end',
                'type' => 'section',
                'indent' => false
            ),

            // Hover állapot
            array(
                'id' => 'text-buttons-hover-start',
                'type' => 'section',
                'title' => 'Hover állapot',
                'subtitle' => 'Az egér a gomb fölött van',
                'indent' => true
            ),
            array(
                'id'       => 'text-buttons-hover-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'default' => array(
                    'color' => '#0071a1',
                    'alpha' => 1,
                ),
            ),
            array(
                'id'       => 'text-buttons-hover-background',
                'type'     => 'background',
                'background-color'    => false,
                'background-attachment' => false,
            ),
            array(
                'id'       => 'text-buttons-hover-bordercolor',
                'type'     => 'color_rgba',
                'title'    => 'Szegély színe',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'default' => array(
                    'color' => '#0071a1',
                    'alpha' => 1,
                ),
            ),
            array(
                'id'       => 'text-buttons-hover-textcolor',
                'type'     => 'color',
                'title'    => 'Szöveg színe',
                'default' => '#ffffff',
                'transparent' => false,
            ),
            array(
                'id'       => 'text-buttons-hover-iconcolor',
                'type'     => 'color',
                'title'    => 'Ikon színe',
                'transparent' => false,
            ),
            array(
                'id' => 'text-buttons-hover-end',
                'type' => 'section',
                'indent' => false
            ),

            // Active állapot
            array(
                'id' => 'text-buttons-active-start',
                'type' => 'section',
                'title' => 'Active állapot',
                'subtitle' => 'A gomb le van nyomva',
                'indent' => true
            ),
            array(
                'id'       => 'text-buttons-active-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'default' => array(
                    'color' => '#00669b',
                    'alpha' => 1,
                ),
            ),
            array(
                'id'       => 'text-buttons-active-background',
                'type'     => 'background',
                'background-color'    => false,
                'background-attachment' => false,
            ),
            array(
                'id'       => 'text-buttons-active-bordercolor',
                'type'     => 'color_rgba',
                'title'    => 'Szegély színe',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'default' => array(
                    'color' => '#00669b',
                    'alpha' => 1,
                ),
            ),
            array(
                'id'       => 'text-buttons-active-textcolor',
                'type'     => 'color',
                'title'    => 'Szöveg színe',
                'default' => '#ffffff',
                'transparent' => false,
            ),
            array(
                'id'       => 'text-buttons-active-iconcolor',
                'type'     => 'color',
                'title'    => 'Ikon színe',
                'transparent' => false,
            ),
            array(
                'id' => 'text-buttons-active-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Szöveg elemek - Számozatlan listák
    Redux::setSection($opt_name, array(
        'title'         => 'Számozatlan listák',
        'id'            => 'text-ulists',
        'desc'          => 'Számozatlan listák beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'ul-container-start',
                'type' => 'section',
                'title' => 'Doboz',
                'subtitle' => 'A listát magába foglaló doboz',
                'indent' => true
            ),
            array(
                'id'       => 'ul-container-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'ul-container-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'ul-container-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'ul-container-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'ul-container-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ul' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'ul-container-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ul' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id' => 'ul-container-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'ul-items-start',
                'type' => 'section',
                'title' => 'Elemek',
                'subtitle' => 'A lista elemei',
                'indent' => true
            ),
            array(
                'id' => 'ul-items-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'betű, sor és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'subsets'   => false,
            ),
            array(
                'id'       => 'ul-items-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ul li' ),
                'top'      => false,
                'bottom'   => false,
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'ul-items-padding',
                'type'     => 'spacing',
                'title'    => 'Doboztól való távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ul li' ),
                'top'      => false,
                'bottom'   => false,
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array(
                    'margin-left' => '10px',
                ),
            ),
            array(
                'id'       => 'ul-items-distance',
                'type'     => 'dimensions',
                'title'    => 'Elemek közti távolság',
                'subtitle' => '<small>(pixel)</small>',
                'width'    => false,
                'units'    => array('px'),
                'units_extended' => false,
            ),
            array(
                'id'       => 'ul-items-type',
                'type'     => 'radio',
                'title'    => 'Elemek jelölése',
                'options'     => array(
                    '1' => '● Pötty',
                    '2' => '■ Kocka',
                    '3' => '─ Vonal',
                    '4' => '☺ Kép'
                ),
                'default' => 1,
            ),
            array(
                'id'       => 'ul-items-image',
                'type'     => 'media',
                'url'      => true,
                'library_filter' => array( 'png' ),
                'required' => array('ul-items-type', '=', 4 ),
            ),
            array(
                'id'       => 'ul-items-color',
                'type'     => 'color_rgba',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'required' => array('ul-items-type', '!=', 4 ),
            ),
            array(
                'id'       => 'ul-items-bullet-distance',
                'type'     => 'dimensions',
                'title'    => 'Jelölő és elem közti távolság',
                'subtitle' => '<small>(pixel)</small>',
                'height'    => false,
                'units'    => array('px'),
                'units_extended' => false,
                'default' => array(
                    'width' => '5px',
                ),
            ),
            array(
                'id' => 'ul-items-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Szöveg elemek - Számozott listák
    Redux::setSection($opt_name, array(
        'title'         => 'Számozott listák',
        'desc'          => 'Számozott listák beállításai',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'ol-container-start',
                'type' => 'section',
                'title' => 'Doboz',
                'subtitle' => 'A listát magába foglaló doboz',
                'indent' => true
            ),
            array(
                'id'       => 'ol-container-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id'       => 'ol-container-background',
                'type'     => 'background',
                'background-color'    => false,
            ),
            array(
                'id'       => 'ol-container-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
            ),
            array(
                'id' => 'ol-container-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 64,
                'step' => 2,
            ),
            array(
                'id'       => 'ol-container-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ol' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'ol-container-margin',
                'type'     => 'spacing',
                'title'    => 'Távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ol' ),
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array( 'margin-bottom' => '10px' ),
            ),
            array(
                'id' => 'ol-container-end',
                'type' => 'section',
                'indent' => false
            ),
            array(
                'id' => 'ol-items-start',
                'type' => 'section',
                'title' => 'Elemek',
                'subtitle' => 'A lista elemei',
                'indent' => true
            ),
            array(
                'id' => 'ol-items-typo',
                'type' => 'typography',
                'title' => 'Betűtípus',
                'subtitle' => 'betű, sor és szóköz, stílus és szín',
                'units' => 'px',
                'font-style' => true,
                'font-weight' => true,
                'font-size' => true,
                'font-family' => true,
                'line-height' => true,
                'word-spacing' => true,
                'letter-spacing' => true,
                'text-align' => false,
                'subsets'   => false,
            ),
            array(
                'id'       => 'ol-items-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ol li' ),
                'top'      => false,
                'bottom'   => false,
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
            ),
            array(
                'id'       => 'ol-items-padding',
                'type'     => 'spacing',
                'title'    => 'Doboztól való távolság',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content ol li' ),
                'top'      => false,
                'bottom'   => false,
                'units'    => array(
                    'px'
                ),
                'mode'     => 'margin',
                'display_units' => false,
                'default'  => array(
                    'margin-left' => '10px',
                ),
            ),
            array(
                'id'       => 'ol-items-distance',
                'type'     => 'dimensions',
                'title'    => 'Elemek közti távolság',
                'subtitle' => '<small>(pixel)</small>',
                'width'    => false,
                'units'    => array('px'),
                'units_extended' => false,
            ),
            array(
                'id'       => 'ol-items-type',
                'type'     => 'radio',
                'title'    => 'Elemek jelölése',
                'options'     => array(
                    '1' => '1 Arab számok',
                    '2' => 'I Római számok',
                    '3' => 'A Betűk',
                ),
                'default' => 1,
            ),
            array(
                'id'       => 'ol-items-color',
                'type'     => 'color_rgba',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
            ),
            array(
                'id' => 'ol-items-end',
                'type' => 'section',
                'indent' => false
            ),
        )
    ));
    // Szöveg elemek - Ikonok
    Redux::setSection($opt_name, array(
        'title'         => 'Ikonok',
        'id'            => 'text-icons',
        'desc'          => 'FontAwesome beállítások',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'text-icons-fontawesome-type',
                'type' => 'image_select',
                'title' => 'Stílus',
                'subtitle' => 'Körvonalas vagy kitöltött ikonokat szeretnénk?',
                'desc' => 'Ez a beállítás a menükben és gombokban található ikonokra vonatkozik, illetve az egyszerű <small><strong>[fa]ikon[/fa]</strong></small> shortcode-ra',
                'options'  => array(
                    'regular'      => array(
                        'alt'   => '"Regular" stílus',
                        'img'   => ReduxFramework::$_url.'assets/img/fa_regular.jpg'
                    ),
                    'solid'      => array(
                        'alt'   => '"Solid" stílus',
                        'img'   => ReduxFramework::$_url.'assets/img/fa_solid.jpg'
                    ),
                ),
                'default' => 'solid',
            ),
            array(
                'id' => 'text-icons-fontawesome-shortcode',
                'type' => 'raw',
                'title' => 'Shortcode',
                'subtitle' => 'Példa kódok ikon beágyazására',
                'full_width' => false,
                'content' => fa_example_shortcodes(),
            ),
        )
    ));

    // Extrák
    Redux::setSection($opt_name, array(
        'title'         => 'Extrák',
        'id'            => 'extras',
        'desc'          => 'Top gomb, oldal betöltés animáció stb.',
        'icon'          => 'el el-magic'
    ));
    // Extrák - Analitika
    Redux::setSection($opt_name, array(
        'title'         => 'Analitika',
        'id'            => 'extras-analytics',
        'desc'          => 'Google és Facebook követőkódok',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'ga-api-id',
                'title' => 'Google Analytics',
                'subtitle' => 'Követőkód <small>(pl: UA-XXXXX-Y)</small>',
                'type' => 'text',
            ),
            array(
                'id' => 'ga-api-mode',
                'type' => 'button_set',
                'desc' => 'Betöltés módja',
                'options' => array(
                    'normal' => 'Normál',
                    'async' => 'Aszinkron',
                ),
                'default' => 'normal',
                'required' => array('ga-api-id', '!=', ''),
            ),
                array(
                    'id' => 'fb-api-id',
                    'title' => 'Facebook Analytics',
                    'subtitle' => 'Pixel Azonosító kód',
                    'type' => 'text',
                ),
        )
    ));
    // Szállítási limitációk
    Redux::setSection($opt_name, array(
        'title'         => 'Szállítási limitációk',
        'id'            => 'extras-shipping-limits',
        'desc'          => 'Deaktiválhatunk szállítási módokat bizonyos szállítási osztályokhoz',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'shipping-limits',
                'type' => 'textarea',
                'title' => 'Szabályok',
                'subtitle' => '<strong>Szállítási osztály</strong> (slug)|Szállítási mód<span style="opacity:0.4;display:inline">,Szállítási mód 2,stb..</span></br><strong>fagyasztott-termekek</strong>|Házhozszállítás<span style="opacity:0.4;display:inline">,GLS Futár,FoxPost</span>',
                'desc' => 'Az itt megadott szállítási módok <strong>TILTVA</strong> lesznek azoknál a termékeknél amelyek a megadott szállítási osztályba tartoznak!',
                'rows' => 6,
            ),
            array(
                'id' => 'shipping-limits-debug',
                'type' => 'switch',
                'title' => 'Hibakeresés',
                'default' => false,
            ),
        )
    ));
    // Extrák - Top gomb
    Redux::setSection($opt_name, array(
        'title'         => 'Top gomb',
        'id'            => 'extras-topbutton',
        'desc'          => 'Vissza az oldal tetejére gomb',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id' => 'topbutton-enabled',
                'type' => 'switch',
                'title' => 'Bekapcsolva',
                'default' => false,
            ),

            // POZÍCIÓ
            array(
                'id' => 'topbutton-position-start',
                'type' => 'section',
                'title' => 'Pozíció',
                'subtitle' => 'A top gomb helye',
                'indent' => true,
                'required' => array('topbutton-enabled', '=', true),
            ),
            array(
                'id' => 'topbutton-position',
                'type' => 'image_select',
                'title' => 'Sarok',
                'options' => array(
                    'top-left'      => array(
                        'alt'   => 'Bal felső',
                        'img'   => ReduxFramework::$_url.'assets/img/top-left.png'
                    ),
                    'top-right'      => array(
                        'alt'   => 'Jobb felső',
                        'img'   => ReduxFramework::$_url.'assets/img/top-right.png'
                    ),
                    'bottom-left'      => array(
                        'alt'   => 'Bal alsó',
                        'img'   => ReduxFramework::$_url.'assets/img/bottom-left.png'
                    ),
                    'bottom-right'      => array(
                        'alt'   => 'Jobb alsó',
                        'img'   => ReduxFramework::$_url.'assets/img/bottom-right.png'
                    ),
                ),
                'default' => 'bottom-right',
            ),
            array(
                'id'       => 'topbutton-spacing',
                'type'     => 'dimensions',
                'title'    => 'Távolság',
                'subtitle' => 'A saroktól <small>(pixel)</small>',
                'units'    => array('px'),
                'units_extended' => false,
                'default' => array(
                    'width' => '40px',
                    'height' => '40px',
                )
            ),
            array(
                'id' => 'topbutton-position-end',
                'type' => 'section',
                'indent' => false,
                'required' => array('topbutton-enabled', '=', true),
            ),

            // HÁTTÉR
            array(
                'id' => 'topbutton-background-start',
                'type' => 'section',
                'title' => 'Háttér',
                'subtitle' => 'A top gomb mögötti rész beágyazásai',
                'indent' => true,
                'required' => array('topbutton-enabled', '=', true),
            ),
            array(
                'id' => 'topbutton-background-enabled',
                'type' => 'switch',
                'title' => 'Bekapcsolva',
                'default' => true,
            ),
            array(
                'id'       => 'topbutton-backcolor',
                'type'     => 'color_rgba',
                'title'    => 'Háttér',
                'options' => array(
                    'clickout_fires_change' => true,
                    'choose_text' => 'Kiválaszt',
                    'cancel_text' => 'Mégsem'
                ),
                'required' => array('topbutton-background-enabled', '=', true),
            ),
            array(
                'id'       => 'topbutton-background',
                'type'     => 'background',
                'background-color'    => false,
                'required' => array('topbutton-background-enabled', '=', true),
            ),
            array(
                'id'       => 'topbutton-border',
                'type'     => 'border',
                'title'    => 'Szegélyek',
                'subtitle' => '<small>(pixel)</small>',
                'all'      => false,
                'default'  => array(
                    'border-color'  => '#333',
                    'border-style'  => 'solid',
                ),
                'required' => array('topbutton-background-enabled', '=', true),
            ),
            array(
                'id' => 'topbutton-border-radius',
                'type' => 'slider',
                'desc' => 'Lekerekítés <small>(pixel)</small>',
                'max' => 500,
                'step' => 1,
                'required' => array('topbutton-background-enabled', '=', true),
            ),
            array(
                'id'       => 'topbutton-padding',
                'type'     => 'spacing',
                'title'    => 'Belső térköz',
                'subtitle' => '<small>(pixel)</small>',
                'output'   => array( '.entry-content > blockquote, .entry-content p > blockquote' ),
                'units'    => array(
                    'px'
                ),
                'display_units' => false,
                'default' => array(
                    'padding-left' => '20px',
                    'padding-right' => '20px',
                    'padding-bottom' => '20px',
                    'padding-top' => '20px',
                ),
                'required' => array('topbutton-background-enabled', '=', true),
            ),
            array(
                'id' => 'topbutton-background-end',
                'type' => 'section',
                'indent' => false,
                'required' => array('topbutton-enabled', '=', true),
            ),

            // IKON
            array(
                'id' => 'topbutton-icon-start',
                'type' => 'section',
                'title' => 'Ikon',
                'subtitle' => 'A top gomb ikonja',
                'indent' => true,
                'required' => array('topbutton-enabled', '=', true),
            ),
            array(
                'id' => 'topbutton-icon-type',
                'type' => 'button_set',
                'title' => 'Típus',
                'options' => array(
                    'icon' => 'Ikon',
                    'image' => 'Kép',
                ),
                'default' => 'icon',
            ),
            array(
                'id' => 'topbutton-icon-fa',
                'type' => 'text',
                'title' => 'Ikon neve',
                'subtitle' => '<a href="https://fontawesome.com/icons" target="_blank">Fontawesome</a> név. <small>pl: <strong>chevron-up</strong></small>',
                'default' => 'chevron-up',
                'required' => array('topbutton-icon-type', '=', 'icon'),
            ),
            array(
                'id' => 'topbutton-icon-typo',
                'type' => 'typography',
                'title' => 'Ikon stílusa',
                'subtitle' => 'betű stílus és szín',
                'units' => 'px',
                'font-style' => false,
                'font-weight' => false,
                'font-size' => true,
                'font-family' => false,
                'line-height' => false,
                'word-spacing' => false,
                'letter-spacing' => false,
                'text-align' => false,
                'subsets'   => false,
                'default' => array(
                    'font-size' => '30px',
                    'color' => '#000',
                ),
            ),
            array(
                'id' => 'topbutton-icon-image',
                'type' => 'media',
                'title' => 'Kép',
                'subtitle' => 'a <strong>PNG</strong> és <strong>jpg</strong> formátumok használhatók',
                'library_filter' => array(
                    'jpg',
                    'jpeg',
                    'png',
                ),
                'required' => array('topbutton-icon-type', '=', 'image'),
            ),
            array(
                'id' => 'topbutton-icon-image-size',
                'type' => 'slider',
                'title' => 'Képméret',
                'subtitle' => 'Szélesség. A magasság képarány alapján automatikus. <small>(pixel)</small>',
                'min' => 16,
                'max' => 300,
                'default' => 80,
                'required' => array('topbutton-icon-type', '=', 'image'),
            ),
            array(
                'id' => 'topbutton-icon-end',
                'type' => 'section',
                'indent' => false,
                'required' => array('topbutton-enabled', '=', true),
            ),
        )
    ));
    // Extrák - Betöltés animáció
    Redux::setSection($opt_name, array(
        'title'         => 'Betöltés animáció',
        'id'            => 'extras-loadinganimation',
        'desc'          => 'Oldal betöltése közben megjelenített animáció',
        'subsection'    => true,
        'fields'        => array(
            // TODO
        )
    ));
    // Extrák - Süti figyelmeztetés
    Redux::setSection($opt_name, array(
        'title'         => 'Süti figyelmeztetés',
        'id'            => 'extras-cookies',
        'desc'          => 'Süti figyelmeztetés beállításai',
        'subsection'    => true,
        'fields'        => array(
            // TODO
        )
    ));

    // Fejlesztő
    Redux::setSection($opt_name, array(
        'title'         => 'Fejlesztő',
        'id'            => 'developer',
        'desc'          => 'Fejlesztőknek szánt lehetőségek',
        'icon'          => 'el el-laptop'
    ));
    // Fejlesztő - Karbantartás mód
    Redux::setSection($opt_name, array(
        'title'         => 'Karbantartás mód',
        'id'            => 'developer-maintenance',
        'desc'          => 'Meggátolhatjuk hogy a nem bejelentkezett felhasználók látogathassák oldalunkat',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'       => 'maintenance-mode',
                'type'     => 'switch',
                'title'    => 'Karbantartás mód',
                'default'  => false,
            ),
            array(
                'id'       => 'maintenance-title',
                'type'     => 'text',
                'title'    => 'Cím',
                'subtitle'     => 'Nagyban jelenik meg felül',
                'required' => array(
                    array('maintenance-mode','=',true)
                ),
                'validate' => array('not_empty'),
                'default'  => 'Under Maintenance.'
            ),
            array(
                'id'       => 'maintenance-content',
                'type'     => 'textarea',
                'title'    => 'Szöveg',
                'subtitle'     => 'Magyarázat a cím alatt',
                'required' => array(
                    array('maintenance-mode','=',true)
                ),
                'validate' => array('not_empty'),
                'default'  => 'Website under planned maintenance. Please check back later.'
            ),
        )
    ));
    // Fejlesztő - Egyedi kód
    Redux::setSection($opt_name, array(
        'title'         => 'Egyedi kód',
        'id'            => 'developer-customcode',
        'desc'          => 'Egyedi CSS és Javascript kódok',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'       => 'custom-css',
                'type'     => 'ace_editor',
                'title'    => 'Egyedi CSS',
                'mode'     => 'css',
                'theme'    => 'monokai'
            ),
            array(
                'id'       => 'custom-js',
                'type'     => 'ace_editor',
                'title'    => 'Egyedi JS',
                'mode'     => 'javascript',
                'theme'    => 'monokai'
            ),
        )
    ));
    // Fejlesztő - Hibakeresés
    Redux::setSection($opt_name, array(
        'title'         => 'Hibakeresés',
        'id'            => 'developer-debug',
        'desc'          => 'Hibakeresési lehetőségek',
        'subsection'    => true,
        'fields'        => array(
            array(
                'id'       => 'debug-pre',
                'type'     => 'switch',
                'title'    => 'Mentett adatok',
                'subtitle'     => 'Megjeleníti a főoldalon a változókat',
                'default'  => false,
            ),
        )
    ));
