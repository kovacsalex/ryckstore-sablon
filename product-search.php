<?php /* Template Name: Termek kereso */

get_header();
page_normal_header();

 ?>
	<main id="content">
		<?php
            $search_string = isset($_GET['q']) ? $_GET['q'] : false;
            $search_term_type = isset($_GET['term-type']) ? $_GET['term-type'] : false;
            $search_term_type_name = $search_term_type == 'product_tag' ? 'címke' : 'kategória';
            $search_term_id = isset($_GET['term-id']) ? intval($_GET['term-id']) : false;
            $search_term_name = $search_term_id !== false ? get_term($search_term_id)->name : false;

            $all_term_link = '';
            if ($search_term_id !== false)
                $all_term_link = $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST'] . explode('?', $_SERVER['REQUEST_URI'], 2)[0] . '?q='.$search_string.'&amp;term-id=-1&amp;term-type=' . ($search_term_type !== false ? $search_term_type : 'product_cat');

            ?>
			<h3 class="keyword"><strong>Keresett termék:</strong> <span class="keyword"><?php if ($search_string !== false): print "\"$search_string\""; endif; ?></a></h3>
			<?php if ($search_term_id !== false && $search_term_id !== -1 && $search_term_name !== false): ?>
				<h3 class="selected-term"><strong>Kiválasztott <?php echo $search_term_type_name; ?>:</strong> <a class="remove-term" href="<?php echo $all_term_link; ?>"><?php print $search_term_name; build_icon('times'); ?></a></h3>
			<?php endif;

            $all_products = productsSearch($search_string, $search_term_id, $search_term_type, -1, true);

            //print_r($all_products);

            if (sizeof($all_products) > 0):
                ?><h3 class="result-count"><strong>Találatok:</strong> <?php echo sizeof($all_products); ?> termék</h3><?php
            elseif($search_term_id !== -1 && $search_term_name !== false):
                ?><h3 class="result-count"><strong>Nincs találat</strong> a(z) "<?php echo $search_term_name; ?>" <?php echo $search_term_type_name; ?> termékei között:</h3><?php
            else:
                ?><h3 class="result-count"><strong>Nincs találat</h3> a termékek között:</h3><?php
            endif;

            ?><div class="search-results"><?php
            foreach($all_products as $product_id)
            {
                $product = new RyckProduct($product_id);
                $product->display();
            }
            ?></div><?php
        ?>
	</main>

	<?php get_sidebar(); ?>
<?php get_footer(); ?>
