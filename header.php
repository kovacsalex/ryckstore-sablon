<?php

global $wrapper_width_override;

$width_class = pref('wrapper-width-class');

if (isset($wrapper_width_override) && $wrapper_width_override != null && is_string($wrapper_width_override) && $wrapper_width_override != "")
	$width_class = $wrapper_width_override;

dynamic_category_header_check();
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<div id="wrapper" class="hfeed" style="display: none;">
			<?php build_sticky_header(); ?>
			<?php build_header(); ?>
			<?php complex_main_menu(); ?>
			<?php woocommerce_jumbo_header(); ?>
			<?php cart_checkout_jumbo_header(); ?>
			<?php page_jumbo_header(); ?>
			<div id="container" class="<?php print $width_class; ?>">
				<?php
					if (pref('debug-pre') == true) { print_r_pref(); }

					sidebar_switches();

					sidebar_left();

				?><div id="center">
